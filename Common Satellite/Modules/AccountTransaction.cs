﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Linq;

namespace Common_Satellite.Modules
{
    public class AccountTransaction
    {
        public TaskList Task
        {
            get; set;
        }

        public string ConnString
        {
            get; set;
        }

        public AccountTransaction(TaskList task)
        {
            Task = task;
        }

        public TransReference TransActionReference
        { get; set; }

        public void AddTransaction(TaskList task, vw_CustomerProfile profile)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                var trans = uow.Transactions.Find(x => x.T_FILENAME == task.TL_OrginalFileName && x.T_MSGTYPE == "Original").FirstOrDefault();
                if (trans != null)
                {
                    trans.T_DATETIME = DateTime.Now;
                    trans.T_TRIAL = "N";
                    trans.T_DIRECTION = profile.P_DIRECTION;
                    trans.T_REF1 = TransActionReference == null ? null : TransActionReference.Reference1;
                    trans.T_REF2 = TransActionReference == null ? null : TransActionReference.Reference2;
                    trans.T_REF3 = TransActionReference == null ? null : TransActionReference.Reference3;
                    trans.T_REF1TYPE = TransActionReference == null ? null : TransActionReference.Ref1Type.ToString();
                    trans.T_REF2TYPE = TransActionReference == null ? null : TransActionReference.Ref2Type.ToString();
                    trans.T_REF3TYPE = TransActionReference == null ? null : TransActionReference.Ref3Type.ToString();
                    trans.T_MSGTYPE = "Update";
                    trans.T_INVOICED = "N";
                    trans.T_CHARGEABLE = profile.P_CHARGEABLE;
                }
                else
                {
                    Transaction newTrans = new Transaction
                    {
                        T_C = profile.P_C,
                        T_P = task.TL_P,
                        T_FILENAME = task.TL_OrginalFileName,
                        T_DATETIME = DateTime.Now,
                        T_TRIAL = "N",
                        T_DIRECTION = profile.P_DIRECTION,
                        T_REF1 = TransActionReference == null ? null : TransActionReference.Reference1,
                        T_REF2 = TransActionReference == null ? null : TransActionReference.Reference2,
                        T_REF3 = TransActionReference == null ? null : TransActionReference.Reference3,
                        T_REF1TYPE = TransActionReference == null ? null : TransActionReference.Ref1Type.ToString(),
                        T_REF2TYPE = TransActionReference == null ? null : TransActionReference.Ref2Type.ToString(),
                        T_REF3TYPE = TransActionReference == null ? null : TransActionReference.Ref3Type.ToString(),
                        T_MSGTYPE = "Original",
                        T_INVOICED = "N",
                        T_CHARGEABLE = profile.P_CHARGEABLE,
                        T_BILLTO = profile.P_BILLTO
                    };
                }
                uow.Complete();

            }

        }


    }



}
