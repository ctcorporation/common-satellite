﻿using NodeResources;
using System;

namespace Common_Satellite
{
    class Profile : Ivw_CustomerProfile
    {
        public string C_CODE { get; set; }
        public string C_FTP_CLIENT { get; set; }
        public Guid C_ID { get; set; }
        public string C_IS_ACTIVE { get; set; }
        public string C_NAME { get; set; }
        public string C_ON_HOLD { get; set; }
        public string C_PATH { get; set; }
        public string C_SHORTNAME { get; set; }
        public string P_ACTIVE { get; set; }
        public string P_BILLTO { get; set; }
        public Guid? P_C { get; set; }
        public string P_CHARGEABLE { get; set; }
        public string P_CUSTOMERCOMPANYNAME { get; set; }
        public string P_CWAPPCODE { get; set; }
        public string P_CWFILENAME { get; set; }
        public string P_CWSUBJECT { get; set; }
        public string P_DELIVERY { get; set; }
        public string P_DESCRIPTION { get; set; }
        public string P_DIRECTION { get; set; }
        public string P_DTS { get; set; }
        public string P_EMAILADDRESS { get; set; }
        public string P_EVENTCODE { get; set; }
        public string P_FILETYPE { get; set; }
        public string P_FORWARDWITHFLAGS { get; set; }
        public Guid P_ID { get; set; }
        public string P_LIBNAME { get; set; }
        public string P_MESSAGEDESCR { get; set; }
        public string P_MESSAGETYPE { get; set; }
        public string P_METHOD { get; set; }
        public string P_MSGTYPE { get; set; }
        public string P_NOTIFY { get; set; }
        public string P_PARAMLIST { get; set; }
        public string P_PASSWORD { get; set; }
        public string P_PATH { get; set; }
        public string P_PORT { get; set; }
        public string P_REASONCODE { get; set; }
        public string P_RECIPIENTID { get; set; }
        public string P_SENDEREMAIL { get; set; }
        public string P_SENDERID { get; set; }
        public string P_SERVER { get; set; }
        public string P_SSL { get; set; }
        public string P_SUBJECT { get; set; }
        public string P_USERNAME { get; set; }
        public string P_XSD { get; set; }
    }
}