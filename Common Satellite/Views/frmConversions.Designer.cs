﻿namespace Common_Satellite.Views
{
    partial class frmConversions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.grdConversionList = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MethodName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MethodDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbDirection = new System.Windows.Forms.GroupBox();
            this.rbTo = new System.Windows.Forms.RadioButton();
            this.rbFrom = new System.Windows.Forms.RadioButton();
            this.lblMethodName = new System.Windows.Forms.Label();
            this.lblMothodDescription = new System.Windows.Forms.Label();
            this.edMethodName = new System.Windows.Forms.TextBox();
            this.edMethodDesc = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.lblConversionsHeader = new System.Windows.Forms.Label();
            this.lblHeader2 = new System.Windows.Forms.Label();
            this.lblHeader1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdConversionList)).BeginInit();
            this.gbDirection.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(712, 382);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grdConversionList
            // 
            this.grdConversionList.AllowUserToAddRows = false;
            this.grdConversionList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdConversionList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdConversionList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Direction,
            this.MethodName,
            this.MethodDescription});
            this.grdConversionList.Location = new System.Drawing.Point(417, 69);
            this.grdConversionList.Name = "grdConversionList";
            this.grdConversionList.ReadOnly = true;
            this.grdConversionList.Size = new System.Drawing.Size(370, 274);
            this.grdConversionList.TabIndex = 1;
            this.grdConversionList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdConversionList_CellDoubleClick);
            this.grdConversionList.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdConversionList_UserDeletingRow);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "CP_ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // Direction
            // 
            this.Direction.DataPropertyName = "CP_Direction";
            this.Direction.HeaderText = "Direction";
            this.Direction.Name = "Direction";
            this.Direction.ReadOnly = true;
            this.Direction.Width = 30;
            // 
            // MethodName
            // 
            this.MethodName.DataPropertyName = "CP_MethodName";
            this.MethodName.HeaderText = "Method Name";
            this.MethodName.Name = "MethodName";
            this.MethodName.ReadOnly = true;
            // 
            // MethodDescription
            // 
            this.MethodDescription.DataPropertyName = "CP_MethodDescription";
            this.MethodDescription.HeaderText = "Details";
            this.MethodDescription.Name = "MethodDescription";
            this.MethodDescription.ReadOnly = true;
            this.MethodDescription.Width = 300;
            // 
            // gbDirection
            // 
            this.gbDirection.Controls.Add(this.rbTo);
            this.gbDirection.Controls.Add(this.rbFrom);
            this.gbDirection.Cursor = System.Windows.Forms.Cursors.Default;
            this.gbDirection.Location = new System.Drawing.Point(39, 112);
            this.gbDirection.Name = "gbDirection";
            this.gbDirection.Size = new System.Drawing.Size(200, 57);
            this.gbDirection.TabIndex = 2;
            this.gbDirection.TabStop = false;
            this.gbDirection.Text = "Conversion Direction";
            // 
            // rbTo
            // 
            this.rbTo.AutoSize = true;
            this.rbTo.Location = new System.Drawing.Point(103, 19);
            this.rbTo.Name = "rbTo";
            this.rbTo.Size = new System.Drawing.Size(38, 17);
            this.rbTo.TabIndex = 1;
            this.rbTo.Text = "To";
            this.rbTo.UseVisualStyleBackColor = true;
            // 
            // rbFrom
            // 
            this.rbFrom.AutoSize = true;
            this.rbFrom.Checked = true;
            this.rbFrom.Location = new System.Drawing.Point(24, 19);
            this.rbFrom.Name = "rbFrom";
            this.rbFrom.Size = new System.Drawing.Size(48, 17);
            this.rbFrom.TabIndex = 0;
            this.rbFrom.TabStop = true;
            this.rbFrom.Text = "From";
            this.rbFrom.UseVisualStyleBackColor = true;
            // 
            // lblMethodName
            // 
            this.lblMethodName.AutoSize = true;
            this.lblMethodName.Location = new System.Drawing.Point(36, 174);
            this.lblMethodName.Name = "lblMethodName";
            this.lblMethodName.Size = new System.Drawing.Size(130, 13);
            this.lblMethodName.TabIndex = 3;
            this.lblMethodName.Text = "Conversion Method Name";
            // 
            // lblMothodDescription
            // 
            this.lblMothodDescription.AutoSize = true;
            this.lblMothodDescription.Location = new System.Drawing.Point(36, 224);
            this.lblMothodDescription.Name = "lblMothodDescription";
            this.lblMothodDescription.Size = new System.Drawing.Size(99, 13);
            this.lblMothodDescription.TabIndex = 4;
            this.lblMothodDescription.Text = "Method Description";
            // 
            // edMethodName
            // 
            this.edMethodName.Location = new System.Drawing.Point(39, 190);
            this.edMethodName.Name = "edMethodName";
            this.edMethodName.Size = new System.Drawing.Size(173, 20);
            this.edMethodName.TabIndex = 5;
            // 
            // edMethodDesc
            // 
            this.edMethodDesc.AcceptsReturn = true;
            this.edMethodDesc.AcceptsTab = true;
            this.edMethodDesc.Location = new System.Drawing.Point(39, 240);
            this.edMethodDesc.Multiline = true;
            this.edMethodDesc.Name = "edMethodDesc";
            this.edMethodDesc.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edMethodDesc.Size = new System.Drawing.Size(335, 103);
            this.edMethodDesc.TabIndex = 6;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(390, 382);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(309, 382);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 8;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // lblConversionsHeader
            // 
            this.lblConversionsHeader.AutoSize = true;
            this.lblConversionsHeader.Location = new System.Drawing.Point(38, 69);
            this.lblConversionsHeader.Name = "lblConversionsHeader";
            this.lblConversionsHeader.Size = new System.Drawing.Size(254, 13);
            this.lblConversionsHeader.TabIndex = 9;
            this.lblConversionsHeader.Text = "Custom Conversion Method names to pick in Profiles";
            // 
            // lblHeader2
            // 
            this.lblHeader2.AutoSize = true;
            this.lblHeader2.Location = new System.Drawing.Point(38, 86);
            this.lblHeader2.Name = "lblHeader2";
            this.lblHeader2.Size = new System.Drawing.Size(291, 13);
            this.lblHeader2.TabIndex = 10;
            this.lblHeader2.Text = "Conversion Method must be created in the Common Satellite";
            // 
            // lblHeader1
            // 
            this.lblHeader1.AutoSize = true;
            this.lblHeader1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lblHeader1.Location = new System.Drawing.Point(39, 13);
            this.lblHeader1.Name = "lblHeader1";
            this.lblHeader1.Size = new System.Drawing.Size(297, 20);
            this.lblHeader1.TabIndex = 0;
            this.lblHeader1.Text = "CUSTOM CONVERSION METHODS";
            // 
            // frmConversions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 417);
            this.Controls.Add(this.lblHeader1);
            this.Controls.Add(this.lblHeader2);
            this.Controls.Add(this.lblConversionsHeader);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.edMethodDesc);
            this.Controls.Add(this.edMethodName);
            this.Controls.Add(this.lblMothodDescription);
            this.Controls.Add(this.lblMethodName);
            this.Controls.Add(this.gbDirection);
            this.Controls.Add(this.grdConversionList);
            this.Controls.Add(this.btnClose);
            this.Name = "frmConversions";
            this.Text = "Custom Conversion Processes";
            this.Load += new System.EventHandler(this.frmConversions_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdConversionList)).EndInit();
            this.gbDirection.ResumeLayout(false);
            this.gbDirection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView grdConversionList;
        private System.Windows.Forms.GroupBox gbDirection;
        private System.Windows.Forms.RadioButton rbTo;
        private System.Windows.Forms.RadioButton rbFrom;
        private System.Windows.Forms.Label lblMethodName;
        private System.Windows.Forms.Label lblMothodDescription;
        private System.Windows.Forms.TextBox edMethodName;
        private System.Windows.Forms.TextBox edMethodDesc;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direction;
        private System.Windows.Forms.DataGridViewTextBoxColumn MethodName;
        private System.Windows.Forms.DataGridViewTextBoxColumn MethodDescription;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label lblConversionsHeader;
        private System.Windows.Forms.Label lblHeader2;
        private System.Windows.Forms.Label lblHeader1;
    }
}