﻿using NodeData.Models;
using System;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmAppLog : Form
    {
        public frmAppLog()
        {
            InitializeComponent();
        }

        public AppLog AppLog { get; set; }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAppLog_Load(object sender, EventArgs e)
        {
            if (AppLog != null)
            {
                lblDate.Text = AppLog.GL_Date.ToString("dd/MM/yyyy hh:mm:ss tt");
                lblServer.Text = AppLog.GL_ServerName;
                lblProcess.Text = AppLog.GL_ProcName;
                lblMesageType.Text = AppLog.GL_MessageType;
                lblSeverity.Text = AppLog.GL_MessageLevel.ToString();
                txtDetail.Text = AppLog.GL_MessageDetail;
            }

        }
    }
}
