﻿using Common_Satellite.Classes;
using Common_Satellite.Modules;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using SendModule;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using XML_Locker.Airco;
using XMLLocker.CargoOffice;
using XMLLocker.Cargowise;
using XMLLocker.Cargowise.XUS;
using XMLLocker.ContainerChain;
using XMLLocker.CTC;
using XMLLocker.SAP;
using XMLocker.Cargowise;
using XMLLocker.TMC;
using XML_Locker.Borg;
using System.Collections.Generic;

namespace Common_Satellite.Views
{


    public partial class frmMain : Form
    {
        private string path = string.Empty;
        //  public SqlConnection sqlConn, sqlCTCConn;

        System.Timers.Timer _maintimer;
        System.Timers.Timer _highTideTimer;
        Stopwatch eventStopWatch;
        private IMailServerSettings mailSettings;
        string lo = Globals.AppLogPath;
        string cp;
        string de;
        string fu;
        DateTime ti;
        string er;
        string di;
        bool CheckStopped;
        private int iCw;
        private int iArc;
        private int iCC;
        private int iSc;
        private string appVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public frmMain()
        {
            InitializeComponent();
            _maintimer = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds
            };
            _maintimer.Elapsed += new ElapsedEventHandler(timerMain_Elapsed);


            _highTideTimer = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(15).TotalMilliseconds
            };
            _highTideTimer.Elapsed += new ElapsedEventHandler(HighTideTimer_Elapsed);

            //this.Text = string.Format("CTC Satellite - ", Assembly.GetExecutingAssembly().GetName().Version.ToString());
            //this.Text = this.Text + " - CTC";
            lblVersion.Text = "version " + appVersion;
        }

        private void HighTideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (eventStopWatch.IsRunning)
            {
                if (eventStopWatch.Elapsed.TotalMinutes >= 5)
                {

                    string cp = "High Tide Processing";
                    string de = "Event timer found to be running for " + eventStopWatch.Elapsed.TotalMinutes + " Minutes";
                    string fu = "Lockup Found";

                    if (GetLogLevel() == 2)
                    {
                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {
                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = cp,
                                GL_MessageType = "Information",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = fu + Environment.NewLine + de
                            });

                        }
                    }

                    btnTimer_Click(btnTimer, new EventArgs());
                    System.Threading.Thread.Sleep(1000);
                    btnTimer_Click(btnTimer, new EventArgs());
                }
            }
        }

        private void DoWork()
        {
            string primaryPath = string.Empty;
            string archivePath = string.Empty;
            string pickupPath = string.Empty;
            string tempPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                primaryPath = Globals.PrimaryLocation;
                archivePath = Globals.ArchiveLocation;
                pickupPath = Globals.PickupLocation;
                tempPath = Globals.TempLocation;
            }
            else
            {
                primaryPath = Globals.TestPrimaryLocation;
                archivePath = Globals.TestArchiveLocation;
                pickupPath = Globals.TestPickupLocation;
                tempPath = Globals.TestTempLocation;
            }


            if (tslMode.Text == "Production")
            {
                path = Globals.ProdOutputLocation;
            }
            else
            {
                path = Globals.TestOutputLocation;
            }
            eventStopWatch.Reset();
            eventStopWatch.Start();
             ProcessQueues();
            tslMain.Text = "Processed Queue ...";
        }


        private void timerMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            _maintimer.Stop();
            DoWork();
            _maintimer.Start();
        }

        private TaskList ConvertToCommon(TaskList task)
         {
            bool converted = false;


            var profile = GetProfile(task.TL_P);
            if (profile != null)
            {

                switch (profile.P_METHOD.ToUpper())
                {
                    case "ARCTOCW":
                        converted = ArcToCW(profile, task);
                        iArc++;
                        break;
                    case "CWTOARC":
                        converted = CwToArc(profile, task);
                        iCw++;
                        break;
                    case "CWTOCC":
                        converted = CwToCC(profile, task);
                        iCw++;
                        break;
                    case "CWTOCARGOOFFICE":
                        converted = CwToCargoOffice(profile, task);
                        iCw++;
                        break;
                    case "CARGOOFFICETOCW":
                        converted = CargoOfficeToCW(profile, task);
                        iCw++;
                        break;
                    case "CCTOCW":
                        converted = ContainerChainToCW(profile, task);
                        iCC++;
                        break;
                    case "SAPTOCOMMON":
                        converted = SapToCommon(profile, task);
                        iSc++;
                        break;
                    case "COMMONTOSAP":
                        converted = CommonToSAP(profile, task);
                        iSc++;
                        break;
                    case "CARGOWISETOMOVEIT":
                        converted = CargowiseToMoveit(profile, task);
                        iCw++;
                        break;
                    case "AIRCOTOCOMMONORDER":
                        converted = AircoToCWOrder(profile, task);
                        iCw++;
                        break;
                    case "CARGOWISETOAIRCOASN":
                        converted = CargowiseToAircoASN(profile, task);
                        iSc++;
                        break;
                    case "TMCTOCOMMON":
                        converted = TMCToCommon(profile, task);
                        iSc++;
                        break;
                    case "CARGOWISETOBORG":
                        converted = CargowiseToBorg(profile, task);
                        iSc++;
                        break;
                    case "SAVEREFERENCE":
                        converted = CargowiseSaveReference(profile, task);
                        iSc++;
                        break;
                    case "REMOVECARGOWISEIDENTITY":
                        converted = RemoveCargowiseIdentity(profile, task);
                        iSc++;
                        break;
                    case "AIRCOTOCWBOOKING":
                        converted = AircotoCWBooking(profile, task);
                        iSc++;
                        break;

                    case "CSVTOCWWAREHOUSEORDER":
                        converted = CSVtoCWWarehouseOrder(profile, task);
                        iSc++;
                        break;

                }
                if (converted)
                {
                    task.TL_Processed = true;
                    task.TL_ProcessDate = DateTime.Now;

                }
            }

            return task;
        }

        private bool CargoOfficeToCW(NodeData.Models.Ivw_CustomerProfile profile, TaskList task)
        {
            return false;
        }

        private bool CSVtoCWWarehouseOrder(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;

            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }

            //Convert to Common
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);

            FileInfo fi = new FileInfo(currentFile);
            DataTable tb = new DataTable();
            try
            {

                Classes.ExcelToTable excelToTable = new Classes.ExcelToTable(fi.FullName);
                tb = CsvHelper.ConvertCSVtoDataTable(fi.FullName);
                        
                ConvertCSV(tb, fi.FullName, profile, task);
                
                //result = true;

            }
            catch (IOException ex)
            {
                MessageBox.Show("File is in use by another process.", "Unable to open File", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }

            return result;
        }
        private void ConvertCSV(DataTable tb, string filename, vw_CustomerProfile profile, TaskList task)
        {

            // TODO : Fix the Dynamic method. 
            // MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            //var method = mo.GetOperationMethod((Guid)cmbOperationList.SelectedValue);
            //MethodInfo custMethodToRun = this.GetType().GetMethod(method);
            //var varResult = custMethodToRun.Invoke(this, new object[] { tb, txtOwner.Text, txtCustomer.Text });
            //if ( varResult==null)
            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var mapHeaderID = mo.GetMapHeaderByName("Reynard Warehouse Order");
            var mapList = mo.GetMap(mapHeaderID.MH_ID);
            CommonToCw commonToCW = new CommonToCw(Globals.AppLogPath);
            ToCargowise toCargowise = new ToCargowise(Globals.AppLogPath);
            ConvertOrder order;
            order = new ConvertOrder(tb, Globals.ConnString.ConnString);
            var common = order.CreateWarehouseOrders(mapList, mapHeaderID.MH_ID, profile.P_SENDERID);
            if (common != null)
            {
                common.IdendtityMatrix = new NodeFileIdendtityMatrix
                {
                    CustomerId = profile.P_RECIPIENTID,
                    SenderId = profile.P_SENDERID,
                    EventCode = "DIM",
                    PurposeCode = "CTC",
                    DocumentType = "WORD",
                    FileDateTime = DateTime.Now.ToString("yyyyMMddThh:mm:ss")

                };

                //     commonToCW.CreateWarehouseOrder(common, txtOutputFolder.Text);
                foreach (var item in common.WarehouseOrders)
                {
                    XMLLocker.Cargowise.XUS.UniversalInterchange cwFile = toCargowise.CWFromCommonWarehouse(item);
                    cwFile.Header.SenderID = common.IdendtityMatrix.SenderId;
                    cwFile.Header.RecipientID = common.IdendtityMatrix.CustomerId;
                    var file = toCargowise.ToFile(cwFile, common.IdendtityMatrix.CustomerId + "-" + item.OrderNumber, path);
                    CreateOutboundTasklist(profile, task, common.IdendtityMatrix.CustomerId + "-" + item.OrderNumber + ".XML");
                }

            }


        }



            private bool CommonToSAP(NodeData.Models.Ivw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            CWToCommon cw2Common = new CWToCommon(Globals.AppLogPath, profile);

            var common = cw2Common.ConvertData(Path.Combine(task.TL_Path, task.TL_FileName), profile);
            fu = m.Name;
            lo = Globals.AppLogPath;
            cp = string.Empty;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;

            if (cw2Common != null)
            {
                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = m.Name + " started"
                        });

                    }
                }
                var sap = new CommonToSAP(profile);
                sap.ConnString = Globals.ConnString.ConnString;

                //sap.OutputPath = Globals.TempLocation;
                sap.OutputPath = Globals.ProdTempLocation;
                if (common.TrackingOrders != null)
                {
                    foreach (var order in common.TrackingOrders)
                    {
                        de = "Order file " + order.OrderNo + " processed";
                        if (!string.IsNullOrEmpty(order.ShipmentNo))
                        {
                            var shipmentMilestones = (from sh in common.Shipments
                                                      where sh.ShipmentNo == order.ShipmentNo
                                                      select sh.Milestones).ToList();
                            if (shipmentMilestones.Count > 0)
                            {

                                var oMileStones = order.Milestones.ToList();
                                foreach (var ms in shipmentMilestones)
                                {
                                    oMileStones.AddRange(ms.AsEnumerable());
                                }
                                order.Milestones = oMileStones.ToArray();

                            }
                        }

                        var sapFile = sap.CreateSAP(order, common.IdendtityMatrix.DocumentType);
                        if (sapFile != null)
                        {

                            var fileToSend = sap.BuildFile(sapFile, sapFile.GetType());
                            result = DeliverFile(fileToSend, profile, task);
                            de += ". Ok";
                            er = string.Empty;
                            if (GetLogLevel() == 2)
                            {
                                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                                {

                                    log.AddLog(new AppLog
                                    {
                                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                        GL_ServerName = Environment.MachineName.ToString(),
                                        GL_Date = DateTime.Now,
                                        GL_ProcName = m.Name,
                                        GL_MessageType = "Information",
                                        GL_MessageLevel = 1,
                                        GL_MessageDetail = de
                                    });

                                }
                            }
                        }
                        else
                        {
                            de += " with errors";
                            er = sap.ErrorString;

                            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                            {

                                log.AddLog(new AppLog
                                {
                                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                    GL_ServerName = Environment.MachineName.ToString(),
                                    GL_Date = DateTime.Now,
                                    GL_ProcName = m.Name,
                                    GL_MessageType = "Warning",
                                    GL_MessageLevel = 1,
                                    GL_MessageDetail = de + Environment.NewLine + er
                                });

                            }

                        }


                    }


                }
            }
            else
            {
                de = "File " + task.TL_OrginalFileName + " did not process.";
                er = "Failed to Convert to Common";
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = de + Environment.NewLine + er
                    });

                }

            }



            return true;
        }

        private bool DateFound(NodeFileTrackingOrder trackingOrders, DateElementDateType arrival)
        {

            var date = (from x in trackingOrders.Dates
                        where x.DateType == arrival
                        select x).FirstOrDefault();
            if (date == null)
            {
                return false;
            }
            DateTime d;
            if (!string.IsNullOrEmpty(date.ActualDate))
            {
                return DateTime.TryParse(date.ActualDate, out d);
            }

            if (!string.IsNullOrEmpty(date.EstimateDate))
            {
                return DateTime.TryParse(date.EstimateDate, out d);
            }

            return false;
        }


        public bool SendFile(string fileToSend, NodeData.Models.Ivw_CustomerProfile profile, TaskList task)
        {
            using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
            {
                mailModule.SendMsg(fileToSend, profile.P_EMAILADDRESS, profile.P_DESCRIPTION, "File attached");

            }

            return true;
        }

        private bool DeliverFile(string fileToSend, NodeData.Models.Ivw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            switch (profile.P_DELIVERY)
            {
                case "D":  //FTP Send
                    FtpHelper ftp = new FtpHelper(fileToSend, Globals.WinSCPLocation);

                    string sendResult = string.Empty;
                    if (profile.P_SSL == "Y" ? true : false)
                    {

                    }
                    else
                    {
                        int port = int.Parse(profile.P_PORT);
                        //sendResult = ftp.FtpSend(fileToSend, profile.P_SERVER, port, profile.P_PATH, profile.P_USERNAME, profile.P_PASSWORD);
                    }

                    if (sendResult.Contains("Failed"))
                    {
                        using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                        {
                            mailModule.SendMsg(string.Empty, Globals.AlertsTo, "FTP Send Direct error - Profile = " + profile.P_DESCRIPTION, "Common Satellite FTP Process failed to send the file : " + fileToSend + Environment.NewLine + "Error Message was :" + sendResult);
                        }

                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {
                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name + "(FTP)",
                                GL_MessageType = "Error",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "Error Sending File : " + task.TL_OrginalFileName + Environment.NewLine + "Error was: " + sendResult
                            });

                        }

                        // Move file to failed
                        string failedLocation = Globals.ProdOutputLocation + "\\Failed";
                        CommonResources.MoveFile(fileToSend, failedLocation);
                        return false;
                    }
                    if (!sendResult.Contains("Failed"))
                    {
                        // if not failed then break out of loop and continue
                        break;
                    }

                    break;
                case "R":  //Drop in Folder
                    try
                    {
                        CommonResources.MoveFile(fileToSend, profile.P_SERVER);
                    }
                    catch (IOException ex)
                    {
                        using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                        {
                            mailModule.SendMsg(string.Empty, Globals.AlertsTo, "File Moving Error:" + profile.P_DESCRIPTION, ex.Message);
                        }

                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {
                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name + "(MOVE)",
                                GL_MessageType = "Error",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "File Moving error " + profile.P_DESCRIPTION + Environment.NewLine + "Error: " + fileToSend + Environment.NewLine + ex.Message
                            });

                        }

                        return false;
                    }
                    break;
                case "E": // Email
                    using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                    {
                        mailModule.SendMsg(fileToSend, profile.P_EMAILADDRESS, profile.P_DESCRIPTION, "File attached");

                    }
                    break;

            }
            return true;
        }

        private bool SapToCommon(NodeData.Models.Ivw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }

            SAPToCommon sapToCommon = new SAPToCommon(Path.Combine(task.TL_Path, task.TL_FileName), Globals.ConnString.ConnString, profile);
            sapToCommon.OutputPath = path;
            NodeFile common = sapToCommon.ConvertSAP();
            CommonToCw cwFile = new CommonToCw(Globals.AppLogPath);
            var cwORder = cwFile.CreateOrderFile(common, path);


            return true;
        }

        private vw_CustomerProfile GetProfile(Guid? pid)
        {
            if (pid != Guid.Empty)
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
                {
                    Guid id = (Guid)pid;
                    
                    
                    var profile = uow.CustProfiles.Find(x => x.P_ID == id).FirstOrDefault();
                    if (profile != null)
                    {
                        return profile;
                    }
                }
            }
            return null;
        }

        private bool ContainerChainToCW(vw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            ContainerChainToCW ccTocw = new ContainerChainToCW(path, Globals.ConnString.ConnString, Path.Combine(task.TL_Path, task.TL_FileName), profile);
            if (ccTocw.CreateEventFromActivity())
            {
                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = m.Name + " complete"
                        });

                    }
                }
                return true;
            }
            return false;

        }

        private NodeFile ReturnCommonFromCargowiseFile(vw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            CWToCommon cWToCommon = new CWToCommon(Globals.AppLogPath, profile);
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            if (CWFunctions.IsCargowiseFile(currentFile))
            {
                NodeFile commonXML = new NodeFile();
                commonXML = cWToCommon.ConvertData(currentFile, profile);
                if (commonXML == null)
                {

                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Error",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Cargowise Order."
                        });

                    }


                    return null;
                }
                return commonXML;

            }
            return null;
        }


        private NodeFile ReturnCommonFromTMC(vw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            TMCToCommon tmcToCommon = new TMCToCommon(Globals.AppLogPath);
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            
                NodeFile commonXML = new NodeFile();
                commonXML = tmcToCommon.ConvertTMC(currentFile, profile);
                if (commonXML == null)
                {

                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Error",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Cargowise Order."
                        });

                    }


                    return null;
                }
                return commonXML;

        }
        private NodeFile ReturnCommonFromCargowiseFileShipment(vw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            CWToCommon cWToCommon = new CWToCommon(Globals.AppLogPath, profile);
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            if (CWFunctions.IsCargowiseFile(currentFile))
            {
                NodeFile commonXML = new NodeFile();
                commonXML = cWToCommon.ConvertDataShipment(currentFile, profile);
                if (commonXML == null)
                {

                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Error",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Cargowise Order."
                        });

                    }


                    return null;
                }
                return commonXML;

            }
            return null;
        }

        private bool CargowiseToBorg(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            fu = m.Name;
            lo = Globals.AppLogPath;
            cp = string.Empty;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            var commonXML = ReturnCommonFromCargowiseFileShipment(profile, task);

            if (commonXML == null)
            {

                er = "Failed to create Common file";
                //using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                //{
                //    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                //}
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }
            }


            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            var commonToBorg = new CommonToBorg();
            var borgXML = commonToBorg.ConvertData(commonXML);
            var Sender = commonXML.IdendtityMatrix.CustomerId;
            var RefNo = "Borg-" + borgXML.JobNumber + "-" + DateTime.Now.ToString("yyyyMMddHHmmss");
            NodeData.Models.TransReference tr = new NodeData.Models.TransReference
            {
                Ref1Type = NodeData.Models.RefType.Order,
                Reference1 = RefNo

            };
            string xmlFile = RefNo;
            int i = 1;
            while (System.IO.File.Exists(Path.Combine(path, xmlFile + ".XML")))
            {
                xmlFile = RefNo + i.ToString();
                i++;
            }
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            de = "Processing Order " + tr.Reference1 + ".";
            try
            {
                using (Stream outputCW = System.IO.File.Open(Path.Combine(path, xmlFile + ".XML"), FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(XML_Locker.Borg.File));
                    ns.Add("", "");
                    xSer.Serialize(outputCW, borgXML, ns);
                    outputCW.Flush();
                    outputCW.Close();

                    CreateOutboundTasklist(profile, task, xmlFile + ".XML");
                    result = true;
                }
            }
            catch (IOException ex)
            {
                er = ex.Message;

                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + "Error " + Environment.NewLine + ex.Message
                    });

                }

            }
            catch (Exception ex)
            {
                er = ex.Message;
                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + " Error " + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace
                    });

                }

            }
            return result;
        }

        public void CreateOutboundTasklist(vw_CustomerProfile profile, TaskList task, string outputFile)
        {
            using (NodeDataContext _context = new NodeDataContext(Globals.ConnString.ConnString))
            {
                if (profile.P_OUTBOUND != Guid.Empty && profile.P_OUTBOUND != null)
                {
                    OutboundTaskList tl = new OutboundTaskList();

                    tl.OTL_ID = Guid.NewGuid();
                    tl.OTL_OrginalFileName = task.TL_FileName;
                    tl.OTL_FileName = outputFile;
                    tl.OTL_ReceiveDate = DateTime.Now;
                    tl.OTL_Path = path;
                    tl.OTL_SenderID = profile.P_SENDERID;
                    tl.OTL_RecipientID = profile.P_RECIPIENTID;
                
                    tl.OTL_P = profile.P_OUTBOUND;
                
                
                    tl.OTL_Processed = false;

                    _context.OutboundTaskLists.Add(tl);
                    _context.SaveChanges();
                    //result
                }
            }
        }

        private bool CwToCargoOffice(vw_CustomerProfile profile, TaskList task)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            var commonXML = ReturnCommonFromCargowiseFile(profile, task);
            if (commonXML != null)
            {

                CommonToCargoOffice cargoOffice = new CommonToCargoOffice(Globals.AppLogPath, profile);
                cargoOffice.OutPutLocation = path;
                cargoOffice.CommonXml = commonXML;

                NodeData.Models.TransReference tr = new NodeData.Models.TransReference
                {
                    Ref1Type = NodeData.Models.RefType.Shipment,
                    Reference1 = commonXML.TimeSlotRequests[0].ShipmentNo,
                    Ref2Type = NodeData.Models.RefType.Housebill,
                    Reference2 = commonXML.TimeSlotRequests[0].HouseBill
                };
                var fileToSend = cargoOffice.BuildFile();
                if (string.IsNullOrEmpty(fileToSend))
                {
                    return false;
                }
                DeliverFile(fileToSend, profile, task);
                if (GetLogLevel() == 2)
                {
                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Information",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = m.Name + " Finished " + profile.P_DESCRIPTION + "(" + fileToSend + ")"
                        });

                    }
                }
                AddTransaction(task, GetProfile(task.TL_P), tr);
            }
            return true;
        }

        private bool CwToCC(vw_CustomerProfile profile, TaskList task)
        {
            lo = Globals.AppLogPath;
            cp = string.Empty;
            de = "Processing Files";
            fu = "Convert from Arc";
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            bool result = false;
            var commonXML = ReturnCommonFromCargowiseFile(profile, task);
            CommonToContainerChain ccFile = new CommonToContainerChain(path);
            ccFile.Profile = profile;
            ccFile.CommonXML = commonXML;
            foreach (var t in commonXML.TimeSlotRequests)
            {

                // var ccRoot= ccFile.ConvertFromCommon(t);
                var fileToSend = ccFile.BuildCCFile();
                if (fileToSend.Contains("ERROR"))
                {
                    using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                    {
                        mailModule.SendMsg(string.Empty, Globals.AlertsTo, "File Conversion error " + profile.P_DESCRIPTION, "Error: " + fileToSend);
                    }

                    using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                    {

                        log.AddLog(new AppLog
                        {
                            GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                            GL_ServerName = Environment.MachineName.ToString(),
                            GL_Date = DateTime.Now,
                            GL_ProcName = m.Name,
                            GL_MessageType = "Error",
                            GL_MessageLevel = 1,
                            GL_MessageDetail = "Error Converting File" + task.TL_OrginalFileName + Environment.NewLine + fileToSend
                        });

                    }

                    try
                    {
                        if (System.IO.File.Exists(fileToSend))
                        {
                            System.IO.File.Delete(fileToSend);
                        }

                    }
                    catch (IOException ex)
                    {

                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {
                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name,
                                GL_MessageType = "Information",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "Error Deleting File" + fileToSend + Environment.NewLine + ex.Message
                            });

                        }
                    }
                    return false;
                }
                DeliverFile(fileToSend, profile, task);

            }
            NodeData.Models.TransReference tr = new NodeData.Models.TransReference
            {
                Ref1Type = NodeData.Models.RefType.Shipment,
                Reference1 = commonXML.TimeSlotRequests[0].ShipmentNo,
                Ref2Type = NodeData.Models.RefType.Housebill,
                Reference2 = commonXML.TimeSlotRequests[0].HouseBill
            };
            AddTransaction(task, profile, tr);
            result = true;
            return result;
        }

        private string BuildCwFile(UniversalInterchange cw, string path)
        {
            var Sender = cw.Header.SenderID;
            var Recipient = cw.Header.RecipientID;
            var RefNo = cw.Body.BodyField.Shipment.DataContext.DataTargetCollection[0].Key;


            string xmlFile = Sender + "-" + Recipient + RefNo + DateTime.Now.ToString("yyyyMMddHHmmss");
            int i = 1;
            while (System.IO.File.Exists(Path.Combine(path, xmlFile + ".XML")))
            {
                xmlFile = Sender + RefNo + i.ToString();
                i++;
            }
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            var cwNSNative = new XmlSerializerNamespaces();
            cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
            using (Stream outputCW = System.IO.File.Open(Path.Combine(Globals.CNodePath, xmlFile + ".XML"), FileMode.Create))
            {
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                xSer.Serialize(outputCW, cw, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();

                
            }
            return xmlFile;
        }

        private string BuildCwFileBooking(UniversalInterchange cw, string path)
        {
            var Sender = cw.Header.SenderID;
            var Recipient = cw.Header.RecipientID;
            var RefNo = cw.Body.BodyField.Shipment.DataContext.DataSourceCollection[0].Key;


            string xmlFile = "ForwardingBooking_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            int i = 1;
            while (System.IO.File.Exists(Path.Combine(path, xmlFile + ".XML")))
            {
                xmlFile = Sender + RefNo + i.ToString();
                i++;
            }
            var cwNSUniversal = new XmlSerializerNamespaces();
            cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
            var cwNSNative = new XmlSerializerNamespaces();
            cwNSNative.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
            using (Stream outputCW = System.IO.File.Open(Path.Combine(Globals.CNodePath, xmlFile + ".XML"), FileMode.Create))
            {
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                xSer.Serialize(outputCW, cw, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();


            }
            return xmlFile;
        }

        private bool CargowiseToAircoASN(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            fu = m.Name;
            lo = Globals.AppLogPath;
            cp = string.Empty;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            var commonXML = ReturnCommonFromCargowiseFile(profile, task);

            if (commonXML == null)
            {

                er = "Failed to create Common file";
                //using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                //{
                //    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                //}
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }

                // Move file to failed
                string failedLocation = Globals.ProdOutputLocation + "\\Failed";
                CommonResources.MoveFile(task.TL_FileName, failedLocation);
                return false;
            }

            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            var commonToAirco = new CommonToAirco();
            var aircoXML = commonToAirco.ConvertData(commonXML);
            var Sender = commonXML.IdendtityMatrix.CustomerId;
            var RefNo = "AIRCO" + commonXML.IdendtityMatrix.FileId;
            NodeData.Models.TransReference tr = new NodeData.Models.TransReference
            {
                Ref1Type = NodeData.Models.RefType.Order,
                Reference1 = RefNo

            };
            string xmlFile = RefNo;
            int i = 1;
            while (System.IO.File.Exists(Path.Combine(path, xmlFile + ".XML")))
            {
                xmlFile = RefNo + i.ToString();
                i++;
            }
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            de = "Processing Order " + tr.Reference1 + ".";
            try
            {
                using (Stream outputCW = System.IO.File.Open(Path.Combine(path, xmlFile + ".XML"), FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(Shipments));
                    xSer.Serialize(outputCW, aircoXML, ns);
                    outputCW.Flush();
                    outputCW.Close();

                    CreateOutboundTasklist(profile, task, xmlFile + ".XML");
                    result = true;
                }
            }
            catch (IOException ex)
            {
                er = ex.Message;

                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + "Error " + Environment.NewLine + ex.Message
                    });

                }

            }
            catch (Exception ex)
            {
                er = ex.Message;
                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + " Error " + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace
                    });

                }

            }
            return result;
        }


        private bool AircotoCWBooking(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }

            //Convert to Common
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            AircoToCommon aircoToCommon = new AircoToCommon(Globals.AppLogPath);
            NodeFile commonXML = new NodeFile();
            commonXML = aircoToCommon.ConvertAircoOrder(currentFile, profile);

            if (commonXML == null)
            {

                er = "Failed to create Common file";
                using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                {
                    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                }
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }

                result = false;
            }
            else
            {
                ToCargowise convertToCargowise = new ToCargowise(Globals.AppLogPath);


                var cws = convertToCargowise.CWFromCommonBooking(commonXML);

                foreach (var cw in cws)
                {
                    if (cw != null)
                    {
                        var xmlFile = BuildCwFileBooking(cw, path);
                        er = "Order " + commonXML.Bookings[0].OrderNumber + " created CW File";

                        //AddTransaction(task, GetProfile(task.TL_P), tr);
                        CreateOutboundTasklist(profile, task, xmlFile + ".XML");
                        //result = true;
                    }
                    else
                    {
                        er = "Order " + commonXML.TrackingOrders[0].OrderNo + " Did not create CW File";
                        result = false;
                    }
                }




            }


            //now build CW file
            //CommonToCw cwFile = new CommonToCw(Globals.AppLogPath);
            //var cwORder = cwFile.CreateOrderFile(commonXML);


            return result;
        }


        private bool CargowiseSaveReference(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            fu = m.Name;
            lo = Globals.AppLogPath;
            cp = string.Empty;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            var commonXML = ReturnCommonFromCargowiseFile(profile, task);

            if (commonXML == null)
            {

                er = "Failed to create Common file";
                //using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                //{
                //    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                //}
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }

                // Move file to failed
                string failedLocation = Globals.ProdOutputLocation + "\\Failed";
                CommonResources.MoveFile(task.TL_FileName, failedLocation);
                return false;
            }

            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }


            ReferenceNumber refs = new ReferenceNumber()
            {
                REF_MASTERBILL = commonXML.LocalTransport.WayBillNumber,
                REF_JOBNUMBER = commonXML.LocalTransport.JobNumber

            };


            if (CheckReferenceNumber(refs))
            {
                ReferenceNumber job = new ReferenceNumber();
                try
                {
                    using (NodeDataContext _context = new NodeDataContext(Globals.ConnString.ConnString))
                    {


                        if (CheckReferenceNumber(refs))
                        {
                            job.REF_ID = Guid.NewGuid();
                            job.REF_HOUSEBILL = refs.REF_HOUSEBILL;
                            job.REF_MASTERBILL = refs.REF_MASTERBILL;
                            job.REF_JOBNUMBER = refs.REF_JOBNUMBER;
                            
                            _context.ReferenceNumbers.Add(job);
                            _context.SaveChanges();

                        }

                    }
                }
                catch (Exception ex)
                {
                    var x = ex.InnerException;
                }
            }

            //if(res == -1)
            //{
            //    result = true;
            //}

            return result;

        }

        public bool CheckReferenceNumber(ReferenceNumber refNum)
        {
            bool result = true;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                ReferenceNumber data = uow.ReferenceNumbers.Find(x => x.REF_MASTERBILL == refNum.REF_MASTERBILL).FirstOrDefault();

                if (data != null)
                {
                    result = false;
                }
            }

            return result;
        }


        private bool TMCToCommon(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            fu = m.Name;
            lo = Globals.AppLogPath;
            cp = string.Empty;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            var commonXML = ReturnCommonFromTMC(profile, task);

            if (commonXML == null)
            {
                er = "Failed to create Common file";
                //using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                //{
                //    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                //}
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }

                // Move file to failed
                string failedLocation = Globals.ProdOutputLocation + "\\Failed";
                CommonResources.MoveFile(task.TL_FileName, failedLocation);
                return false;
            }

            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            
            de = "Processing Order " + commonXML.TransportJob[0].ShipmentNo + ".";
            try
            {
                string refNum = commonXML.TransportJob[0].CustomerReferences.Where(x => x.RefType == "MBOL").Select(x => x.RefValue).FirstOrDefault();

                if (!string.IsNullOrEmpty(refNum))
                {
                    References bill = new References(Globals.ConnString.ConnString);

                    var jobNum = bill.GetJobNumber(refNum);

                    if (!string.IsNullOrEmpty(jobNum))
                    {
                        string file = "TMC-" + jobNum + ".txt";
                        string content = CommonToTxt(commonXML, jobNum);
                        jobNum = jobNum.Replace("/", "_");
                        TextWriter txt = new StreamWriter(Path.Combine(path, "TMC-" + jobNum + ".txt"));
                        
                        txt.Write(content);
                        txt.Close();

                        CreateOutboundTasklist(profile, task, file);
                        result = true;
                    }
                    else
                    { 
                        //Reference not found in db
                    }
                }
                else
                {
                    //if Reference MBOL not found
                }
            }
            catch (IOException ex)
            {
                er = ex.Message;

                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + "Error " + Environment.NewLine + ex.Message
                    });

                }

            }
            catch (Exception ex)
            {
                er = ex.Message;
                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + " Error " + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace
                    });

                }

            }
            return result;
        }

        private string CommonToTxt(NodeFile node, string jobNum)
        {
            NodeFileTransportJob job = node.TransportJob[0];
            List<string> writer = new List<string>();
            string content = "";

            writer.Add("Job Number : " + jobNum);
            writer.Add("Shipment ID : " + job.ShipmentNo);
            writer.Add("Shipment Mode : " + job.TransportMode);
            writer.Add("Customer Name : " + job.CustomerName);

            writer.Add("\n");
            writer.Add("CARRIER DETAILS");
            foreach (var car in job.Carriers)
            {
                writer.Add("Carrier Code : " + car.CarrierCode);
                writer.Add("Equipment Type : " + car.EquipmentType);
                writer.Add("Trailer Length : " + car.TrailerLength);
            }

            foreach(var com in job.Companies)
            {
                if(com.CompanyType == CompanyElementCompanyType.PickupAddress)
                {
                    writer.Add("\n");
                    writer.Add("PICKUP DETAILS");
                    

                }
                else if (com.CompanyType == CompanyElementCompanyType.DeliveryAddress)
                {
                    writer.Add("\n");
                    writer.Add("DELIVERY DETAILS");
                    
                }

                writer.Add("Location ID : " + com.CompanyCode);
                writer.Add("Name : " + com.CompanyName);
                writer.Add("Contact Name : " + com.ContactName);
                writer.Add("Contact Number : " + com.PhoneNo);
                writer.Add("Address : " + com.Address1 + ", " + com.City + ", " + com.State + ", " + com.CountryCode + " " + com.PostCode);
                writer.Add("Notes : " + com.Note);
                writer.Add("Requirements : ");
                for(var i = 0; i<com.Requirements.Length; i++)
                {
                    writer.Add(com.Requirements[i]);
                }
            }
            
            writer.Add("\nITEMS");
            foreach (var line in job.OrderLines)
            {
                writer.Add("Item ID : " + line.Product.ItemId);
                writer.Add("Item Description : " + line.Product.Description);
                writer.Add("SKU Number : " + line.Product.Code);
                writer.Add("PO Number : " + line.Product.Barcode);
            }

            foreach (var s in writer)
            {
                content += s + "\n";
            }
            return content;
        }

        private bool CwToArc(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            fu = m.Name;
            lo = Globals.AppLogPath;
            cp = string.Empty;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            var commonXML = ReturnCommonFromCargowiseFile(profile, task);
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            XMLLocker.ARC.CommonToArc commonToArc = new XMLLocker.ARC.CommonToArc();
            // TODO look at how to add Event Tracking date to CommonXML. 

            XMLLocker.ARC.ToSend.BOM arcXML = commonToArc.ConvertData(commonXML);
            var Sender = commonXML.IdendtityMatrix.CustomerId;
            var RefNo = commonXML.IdendtityMatrix.DocumentType + "-" + commonXML.IdendtityMatrix.FileId;
            NodeData.Models.TransReference tr = new NodeData.Models.TransReference
            {
                Ref1Type = NodeData.Models.RefType.Order,
                Reference1 = RefNo

            };
            string xmlFile = Sender + RefNo;
            int i = 1;
            while (System.IO.File.Exists(Path.Combine(profile.P_SERVER, xmlFile + ".XML")))
            {
                xmlFile = Sender + RefNo + i.ToString();
                i++;
            }
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            de = "Processing Order " + tr.Reference1 + ".";
            try
            {
                using (Stream outputCW = System.IO.File.Open(Path.Combine(profile.P_SERVER, xmlFile + ".XML"), FileMode.Create))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(XMLLocker.ARC.ToSend.BOM));
                    xSer.Serialize(outputCW, arcXML, ns);
                    outputCW.Flush();
                    outputCW.Close();

                    CreateOutboundTasklist(profile, task, xmlFile + ".XML");
                    result = true;
                }
            }
            catch (IOException ex)
            {
                er = ex.Message;

                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + "Error " + Environment.NewLine + ex.Message
                    });

                }

            }
            catch (Exception ex)
            {
                er = ex.Message;
                result = false;

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = ex.GetType().Name + " Error " + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace
                    });

                }

            }



            return result;
        }

        private bool ArcToCW(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            if (GetLogLevel() == 2)
            {
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = m.Name + " started"
                    });

                }
            }
            lo = Globals.AppLogPath;
            cp = string.Empty;

            fu = m.Name;
            ti = DateTime.Now;
            er = string.Empty;
            di = string.Empty;
            de = "Processing File " + task.TL_OrginalFileName + ".";

            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            XMLLocker.ARC.ArcToCommon arcToCommon = new XMLLocker.ARC.ArcToCommon(Globals.AppLogPath);
            NodeFile commonXML = new NodeFile();
            commonXML = arcToCommon.ConvertData(currentFile, profile);
            if (commonXML == null)
            {

                er = "Failed to create Common file";
                using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                {
                    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                }
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }

                result = false;
            }
            else
            {
                XMLLocker.CTC.ToCargowise convertToCargowise = new XMLLocker.CTC.ToCargowise(Globals.AppLogPath);
                var cw = convertToCargowise.CWFromCommon(commonXML);
                if (cw != null)
                {
                    string xmlFile = BuildCwFile(cw, path);
                    er = "Order " + commonXML.TrackingOrders[0].OrderNo + " created CW File";

                    //AddTransaction(task, GetProfile(task.TL_P), tr);
                    CreateOutboundTasklist(profile, task, xmlFile + ".XML");
                    result = true;
                }
                else
                {
                    er = "Order " + commonXML.TrackingOrders[0].OrderNo + " Did not create CW File";
                    result = false;
                }

            }

            return result;
        }

        private bool RemoveCargowiseIdentity(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            string xmlFile = Path.Combine(task.TL_Path, task.TL_FileName);

            if (System.IO.File.Exists(xmlFile))
            {
                XDocument doc = XDocument.Load(xmlFile);
                List<XElement> elements = new List<XElement>();

                doc.Descendants().Where(x => x.Name.LocalName == "Header").Remove();

                foreach (var loElement in doc.Descendants().ToList())
                {

                    if (loElement.Name.LocalName == "Body")
                    {
                        loElement.AddAfterSelf(loElement.Descendants());
                        loElement.Remove();
                    }
                }
                XElement first = doc.Root.Elements().First();

                first.Save(Path.Combine(path, "EXP_" + task.TL_OrginalFileName));

                CreateOutboundTasklist(profile, task, "EXP_" + task.TL_OrginalFileName + ".XML");
                result = true;
            }

            return result;
        }

        

        private bool CargowiseToMoveit(vw_CustomerProfile profile, TaskList task)
        {


            return false;
        }

        private bool AircoToCWOrder(vw_CustomerProfile profile, TaskList task)
        {
            bool result = false;
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }

            //Convert to Common
            string currentFile = Path.Combine(task.TL_Path, task.TL_FileName);
            AircoToCommon aircoToCommon = new AircoToCommon(Globals.AppLogPath);
            NodeFile commonXML = new NodeFile();
            commonXML = aircoToCommon.ConvertAirco(currentFile, profile);

            if (commonXML == null)
            {

                er = "Failed to create Common file";
                using (MailModule mailModule = new MailModule(Globals.MailServerSettings))
                {
                    mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Failed to Convert File " + task.TL_FileName, "Failed in Converting Order Lines \r\nSee previous log entry for details.");
                }
                //TODO Mail Error Message

                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {

                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = m.Name,
                        GL_MessageType = "Error",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Failed to Convert File " + task.TL_FileName + Environment.NewLine + "Failed in Converting Order Lines" + Environment.NewLine + er
                    });

                }

                result = false;
            }
            else
            {
                ToCargowise convertToCargowise = new ToCargowise(Globals.AppLogPath);


                var cws = convertToCargowise.CWFromCommonOrders(commonXML);

                foreach (var cw in cws)
                {
                    if (cw != null)
                    {
                        var xmlFile = BuildCwFile(cw, path);
                        er = "Order " + commonXML.TrackingOrders[0].OrderNo + " created CW File";

                        //AddTransaction(task, GetProfile(task.TL_P), tr);
                        CreateOutboundTasklist(profile, task, xmlFile + ".XML");
                        result = true;
                    }
                    else
                    {
                        er = "Order " + commonXML.TrackingOrders[0].OrderNo + " Did not create CW File";
                        result = false;
                    }
                }




            }


            //now build CW file
            //CommonToCw cwFile = new CommonToCw(Globals.AppLogPath);
            //var cwORder = cwFile.CreateOrderFile(commonXML);


            return result;
        }

        private void ProcessQueues()
        {
            tslMain.Text = "Processing Queues ...";
            MethodBase m = MethodBase.GetCurrentMethod();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);

                heartBeat.RegisterHeartBeat("CTC", m.Name, m.GetParameters());
            }
            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            {
                log.AddLog(new AppLog
                {
                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = m.Name,
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_MessageDetail = m.Name + " started"
                });
            }

            using (NodeDataContext _context = new NodeDataContext(Globals.ConnString.ConnString))
            {
                var taskList = (from t in _context.TaskLists
                                join p in _context.vw_CustomerProfile on t.TL_P equals p.P_ID
                                where p.P_LIBNAME.ToUpper() == Globals.LibraryName
                                && (!t.TL_Processed)
                                select t).ToList();

                var pcount = 1;
                lblProcCount.Text = taskList.Count.ToString();
                if (taskList.Count > 0)
                {
                    foreach (var t in taskList)
                    {
                        tslMain.Text = "Processing Task Queue : " + pcount.ToString() + " of " + taskList.Count.ToString() + " task";
                        if (System.IO.File.Exists(Path.Combine(t.TL_Path, t.TL_FileName)))
                        {
                            if (eventStopWatch == null)
                            {
                                eventStopWatch = new Stopwatch();
                            }

                            TaskList updated = ConvertToCommon(t);
                            t.TL_ProcessDate = updated.TL_ProcessDate;
                            t.TL_Processed = updated.TL_Processed;
                            _context.SaveChanges();
                            if (updated.TL_Processed && System.IO.File.Exists(Path.Combine(t.TL_Path, t.TL_FileName)))
                            {
                                System.IO.File.Delete(Path.Combine(t.TL_Path, t.TL_FileName));
                            }
                        }
                        pcount = pcount + 1;
                    }
                    eventStopWatch.Stop();
                    if (GetLogLevel() == 2)
                    {
                        using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                        {

                            log.AddLog(new AppLog
                            {
                                GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                                GL_ServerName = Environment.MachineName.ToString(),
                                GL_Date = DateTime.Now,
                                GL_ProcName = m.Name,
                                GL_MessageType = "Information",
                                GL_MessageLevel = 1,
                                GL_MessageDetail = "Processing Complete. " + taskList.Count() + " files processed. "

                            });
                        }

                    }
                }
            }
        }

        private void AddTransaction(TaskList t, vw_CustomerProfile profile, NodeData.Models.TransReference tr)
        {

            AccountTransaction accountTransaction = new AccountTransaction(t);
            accountTransaction.ConnString = Globals.ConnString.ConnString;
            accountTransaction.TransActionReference = tr;
            accountTransaction.AddTransaction(t, profile);



        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout AboutBox = new frmAbout();
            AboutBox.ShowDialog();
            LoadSettings();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            tslMain.Text = "Loading....";
            LoadSettings();
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                heartBeat.RegisterHeartBeat("CTC", "Starting", null);
            }
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            //This creates the Log file Initially if it does not exist. 
            string appLog = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-Log.XML";
            Globals.AppLogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, appLog);
            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            {
                log.AddLog(new AppLog
                {
                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = "Starting",
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_MessageDetail = "Application started"
                });
            }
            btnTimer_Click(btnTimer, null);
            tslMain.Text = "Loaded";
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private int GetLogLevel()
        {
            foreach (RadioButton rb in gbLogging.Controls)
            {
                if (rb.Checked)
                {
                    return int.Parse(rb.Tag.ToString());
                }
            }
            return -1;
        }

        private void LoadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            var path = Path.Combine(appDataPath, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            Globals.AppPath = path;
            Globals.AppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.AppConfig = Path.Combine(path, Globals.AppConfig);
            frmSettings settings = new frmSettings();


            if (!System.IO.File.Exists(Globals.AppConfig))
            {
                XDocument xmlConfig = new XDocument(
                            new XDeclaration("1.0", "UTF-8", "Yes"),
                            new XElement("CommonSatellite",
                            new XElement("Database"),
                            new XElement("Communications"),
                            new XElement("Config")));
                xmlConfig.Save(Globals.AppConfig);
                settings.ShowDialog();
                LoadSettings();
            }
            else
            {
                try
                {
                    XDocument doc = XDocument.Load(Globals.AppConfig);
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.AppConfig);

                    var node = doc.Root.Element("Database");

                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    //NodeData.ConnectionManager connstring =

                    Globals.ConnString = new NodeData.ConnectionManager(node.Element("ServerName").Value,
                    node.Element("DatabaseName").Value,
                    node.Element("UserName").Value,
                    node.Element("Password").Value);
                    node = doc.Root.Element("Communications");
                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    Globals.MailServerSettings = new MailServerSettings
                    {
                        Server = node.Element("SMTPServer").Value,
                        UserName = node.Element("SMTPUser").Value,
                        Password = node.Element("SMTPPassword").Value,
                        IsSecure = bool.Parse(node.Element("SMTPSsl").Value),
                        Port = int.Parse(node.Element("SMTPPort").Value),
                        Email = node.Element("SMTPEmailFrom").Value
                    };
                    node = doc.Root.Element("Config");
                    if (node == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    Globals.PrimaryLocation = node.Element("PrimaryLocation") == null ? "" : node.Element("PrimaryLocation").Value;
                    Globals.TempLocation = node.Element("TemporaryLocation") == null ? "" : node.Element("TemporaryLocation").Value;
                    Globals.ArchiveLocation = node.Element("ArchiveLocation") == null ? "" : node.Element("ArchiveLocation").Value;
                    Globals.WinSCPLocation = node.Element("WinSCPLocation") == null ? "" : node.Element("WinSCPLocation").Value;
                    Globals.TestPrimaryLocation = node.Element("TestPrimaryLocation") == null ? "" : node.Element("TestPrimaryLocation").Value;
                    Globals.TestTempLocation = node.Element("TestTemporaryLocation") == null ? "" : node.Element("TestTemporaryLocation").Value;
                    Globals.TestOutputLocation = node.Element("TestOutputLocation") == null ? "" : node.Element("TestOutputLocation").Value;
                    Globals.ProdTempLocation = node.Element("ProdTemporaryLocation") == null ? "" : node.Element("ProdTemporaryLocation").Value;
                    Globals.ProdOutputLocation = node.Element("ProdOutputLocation") == null ? "" : node.Element("ProdOutputLocation").Value;
                    Globals.TestArchiveLocation = node.Element("TestArchiveLocation") == null ? "" : node.Element("TestArchiveLocation").Value;
                    Globals.PollTime = node.Element("PollTime") == null ? 0 : int.Parse(node.Element("PollTime").Value);
                    Globals.RefreshTime = node.Element("RefreshTime") == null ? 0 : int.Parse(node.Element("RefreshTime").Value);
                    Globals.AlertsTo = node.Element("AlertsTo") == null ? "" : node.Element("AlertsTo").Value;
                    Globals.PickupLocation = node.Element("PickupLocation") == null ? "" : node.Element("PickupLocation").Value;
                    Globals.TestPickupLocation = node.Element("TestPickupLocation") == null ? "" : node.Element("TestPickupLocation").Value;
                    if (node.Element("LibraryName") == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    else
                    {
                        Globals.LibraryName = node.Element("LibraryName").Value;
                    }
                    if (node.Element("CNodeLocation") == null)
                    {
                        MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        settings.ShowDialog();
                        LoadSettings();
                    }
                    else
                    {
                        Globals.CNodePath = node.Element("CNodeLocation").Value;
                    }

                }
                catch (Exception ex)
                {
                    string err = ex.GetType().Name;
                    MessageBox.Show("Error Loading Some of the System settings.", "System Settings missing", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    settings.ShowDialog();
                    LoadSettings();

                }
                _maintimer.Interval = (int)TimeSpan.FromMinutes(Globals.PollTime).TotalMilliseconds;
            }

        }

        private void profilesToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settingsform();
        }

        private void Settingsform()
        {
            using (frmSettings FormSettings = new frmSettings())
            {
                FormSettings.ShowDialog();
                DialogResult dr = FormSettings.DialogResult;
                if (dr == DialogResult.OK)
                {
                    LoadSettings();
                }
            }
        }

        private void customMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void bbClose_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            this.tslMain.Width = this.Width / 2;
            this.tslSpacer.Width = (this.Width / 2) - (productionToolStripMenuItem.Width + tslCmbMode.Width);
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void eventTypesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnTimer_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                tslMain.Text = "Starting....";
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = System.Drawing.Color.LightCoral;
                _highTideTimer.Enabled = true;
                _highTideTimer.Start();
                eventStopWatch = new Stopwatch();
                if (tslMode.Text == "Testing")
                {
                    DialogResult dr = MessageBox.Show("In Test Mode do you wish ", "Test Mode detected", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {

                    }

                }
                else
                {

                }
                DoWork();
                _maintimer.Start();
                tslMain.Text = "Timed process running";
                this.tslSpacer.Padding = new Padding(this.Size.Width - (195), 0, 0, 0);
            }
            else
            {
                tslMain.Text = "Stopping....";
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = System.Drawing.Color.LightGreen;
                _highTideTimer.Stop();
                _highTideTimer.Enabled = false;
                _maintimer.Stop();
                tslMain.Text = "Timed process stopped";
                using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
                {
                    log.AddLog(new AppLog
                    {
                        GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                        GL_ServerName = Environment.MachineName.ToString(),
                        GL_Date = DateTime.Now,
                        GL_ProcName = "Timed Processes",
                        GL_MessageType = "Information",
                        GL_MessageLevel = 1,
                        GL_MessageDetail = "Timed Processes stopped"
                    });
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnClose_Click(this, null);
        }

        private void convertProductDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmConvertData convertProduct = new frmConvertData())
            {

                convertProduct.ShowDialog();
            }
        }


        private void searchArchivesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmArchive archive = new frmArchive())
            {
                archive.ShowDialog();

            }
        }


        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            using (IHeartBeatManager heartBeat = new HeartBeatManager(Globals.ConnString.ConnString))
            {
                heartBeat.ExeName = Assembly.GetExecutingAssembly().GetName().Name;
                heartBeat.ExePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                MethodBase m = MethodBase.GetCurrentMethod();
                heartBeat.RegisterHeartBeat("CTC", "Stopping", null);
            }
            using (AppLogging log = new AppLogging(Globals.ConnString.ConnString))
            {
                log.AddLog(new AppLog
                {
                    GL_AppName = Assembly.GetExecutingAssembly().GetName().Name,
                    GL_ServerName = Environment.MachineName.ToString(),
                    GL_Date = DateTime.Now,
                    GL_ProcName = "Shutting down",
                    GL_MessageType = "Information",
                    GL_MessageLevel = 1,
                    GL_MessageDetail = "Application Closed"
                });
            }

        }

        private void importDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmConvertData convertProduct = new frmConvertData())
            {
                convertProduct.ShowDialog();
            }



        }

        private void codeMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmCargowiseEnums mapping = new frmCargowiseEnums(Globals.ConnString.ConnString))
            {
                mapping.ShowDialog();
            }

        }



        private void mappingOperationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmMapOperations mapOps = new frmMapOperations())
            {
                mapOps.ShowDialog();
            }

        }

        private void setMappingFieldsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmMapFields mapFields = new frmMapFields())
            {
                mapFields.ShowDialog();
            }

        }


        private void sendToAPIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (frmWebApi webApi = new frmWebApi())
            {
                webApi.ShowDialog();
            }
        }

        private void conversionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var conversions = new frmConversions();
            conversions.Show();
        }

        public static OrganizationAddress GetAddress(OrganizationAddress[] organizationAddressCollection, string AddressType)
        {

            OrganizationAddress Address = organizationAddressCollection.ToList().Find(x => x.AddressType == AddressType);
            return Address;
        }

        //private NodeFile ConvertAIRCO(string ordrLine)
        //{

        //NodeFile common = new NodeFile();
        //foreach (var sapOrder in ordrLine.IDOC.E1EDL20)
        //{
        //    common.IdendtityMatrix = ReturnIdentityMatrix();
        //    List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
        //    NodeFileTrackingOrder order = GetOrderFromAirco(sapOrder);
        //    orders.Add(order);
        //    common.TrackingOrders = orders.ToArray();


        //}
        //return common;
        //}

        //private NodeFileTrackingOrder GetOrderFromAirco()
        //{
        //NodeFileTrackingOrder order = new NodeFileTrackingOrder();
        //order.Companies = GetCompaniesFromOrder(ordrLine).ToArray();
        //order.Dates = GetOrderDates(ordrLine).ToArray();
        //order.OrderLines = GetLinesfromOrder(ordrLine).ToArray();
        //order.CustomerReferences = GetCustomFields(ordrLine).ToArray();
        //order.SupplierOrderNo = ordrLine.INCO2_L;
        //order.OrderNo = ordrLine.E1EDL24[0].VGBEL;
        //order.Containers = GetContainersFromOrder(ordrLine).ToArray();
        //if (order.OrderLines.Length > 0)
        //{
        //    order.GoodsDescription = order.OrderLines[0].Product.Description;
        //}


        //return order;
        //}

    }
}

