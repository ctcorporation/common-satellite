﻿using Common_Satellite.Classes;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmMapFields : Form
    {
        private Guid fieldId;
        public frmMapFields()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMapFields_Load(object sender, EventArgs e)
        {
            dgFieldList.AutoGenerateColumns = false;
            FillOperations();
            fieldId = Guid.Empty;
            btnSave.Enabled = false;
        }

        private void FillOperations()
        {
            var mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var opsList = mo.GetOperations();
            opsList.Insert(0, new KeyValuePair<Guid, string>(Guid.Empty, "(none selected)"));
            cmbOperationList.DataSource = opsList;
            cmbOperationList.DisplayMember = "Value";
            cmbOperationList.ValueMember = "Key";
        }

        private void cmbOperationList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbOperationList.SelectedItem != null)
            {
                if ((Guid)cmbOperationList.SelectedValue != Guid.Empty)
                {
                    FillFields((Guid)cmbOperationList.SelectedValue);
                    btnSave.Enabled = true;
                }
                else
                {
                    btnSave.Enabled = false;

                }

            }
        }

        private void FillFields(Guid selectedItem)
        {
            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var fieldList = mo.GetFields(selectedItem);
            dgFieldList.DataSource = fieldList;

        }

        private void cmbOperationList_DropDown(object sender, EventArgs e)
        {
            cmbOperationList.SelectedItem = null;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if ((Guid)cmbOperationList.SelectedValue != Guid.Empty)
            {
                MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
                var mf = mo.SaveFields(new MappingMasterField
                {
                    MM_FieldName = txtFieldName.Text,
                    MM_MO = (Guid)cmbOperationList.SelectedValue
                });
                if (mf < 1)
                {
                    MessageBox.Show("Error saving Record:" + Environment.NewLine + mo.ErrorLog, "Error saving record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (fieldId != Guid.Empty)
                    mo.RemoveField(fieldId);
                fieldId = Guid.Empty;
                FillFields((Guid)cmbOperationList.SelectedValue);
            }

        }

        private void dgFieldList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                txtFieldName.Text = (string)dgFieldList.Rows[e.RowIndex].Cells[1].Value;
                fieldId = (Guid)dgFieldList.Rows[e.RowIndex].Cells[0].Value;
            }
        }
    }
}
