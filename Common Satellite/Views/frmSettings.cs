﻿using NodeData;
using NodeData.Models;
using NodeResources;
using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Common_Satellite.Views
{
    public partial class frmSettings : Form
    {
        SqlConnection sqlConn, sqlCTCConn;
        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void LoadSettings()
        {

            try
            {
                if (Globals.MailServerSettings != null)
                {
                    txtSmtpServer.Text = Globals.MailServerSettings.Server;
                    txtSmtpPort.Text = Globals.MailServerSettings.Port.ToString();
                    txtSmtpUserName.Text = Globals.MailServerSettings.UserName;
                    txtSmtpPassword.Text = Globals.MailServerSettings.Password;
                    txtSmtpEmailAddress.Text = Globals.MailServerSettings.Email;
                }

                if (Globals.ConnString != null)
                {
                    txtDatabaseServer.Text = Globals.ConnString.ServerName;
                    txtDatabaseName.Text = Globals.ConnString.DatabaseName;
                    txtUserName.Text = Globals.ConnString.User;
                    txtPassword.Text = Globals.ConnString.Password;
                }

                txtAlertsTo.Text = Globals.AlertsTo;
                txtPollTime.Text = Globals.PollTime.ToString();
                txtRefresh.Text = Globals.RefreshTime.ToString();
                txtLibraryName.Text = Globals.LibraryName;
                txtCNodeLocation.Text = Globals.CNodePath;
                edWinSCPLocation.Text = Globals.WinSCPLocation;
                edProdOutputLocation.Text = Globals.ProdOutputLocation;
                edProdTempLocation.Text = Globals.ProdTempLocation;
                edTestOutputLocation.Text = Globals.TestOutputLocation;
                edTestTempLocation.Text = Globals.TestTempLocation;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading Settings. Error was: \n" + ex.Message, "Error Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            XDocument doc = XDocument.Load(Globals.AppConfig);
            string section = "Database";
            var node = doc.Root.Element(section)
                        .Element("ServerName");
            if (node == null)
            {
                XElement el = new XElement("ServerName");
                el.Value = txtDatabaseServer.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtDatabaseServer.Text;
            }
            node = doc.Root.Element(section)
                        .Element("DatabaseName");
            if (node == null)
            {
                XElement el = new XElement("DatabaseName");
                el.Value = txtDatabaseName.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtDatabaseName.Text;
            }
            node = doc.Root.Element(section)
                        .Element("UserName");
            if (node == null)
            {
                XElement el = new XElement("UserName");
                el.Value = txtUserName.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtUserName.Text;
            }

            node = doc.Root.Element(section)
                        .Element("Password");
            if (node == null)
            {
                XElement el = new XElement("Password");
                el.Value = txtPassword.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtPassword.Text;
            }
            section = "Communications";
            node = doc.Root.Element(section)
                         .Element("SMTPServer");
            if (node == null)
            {
                XElement el = new XElement("SMTPServer");
                el.Value = txtSmtpServer.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpServer.Text;
            }

            node = doc.Root.Element(section)
             .Element("SMTPUser");
            if (node == null)
            {
                XElement el = new XElement("SMTPUser");
                el.Value = txtSmtpUserName.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpUserName.Text;
            }

            node = doc.Root.Element(section)
             .Element("SMTPEmailFrom");
            if (node == null)
            {
                XElement el = new XElement("SMTPEmailFrom");
                el.Value = txtSmtpEmailAddress.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpEmailAddress.Text;
            }
            node = doc.Root.Element(section)
                         .Element("SMTPPassword");
            if (node == null)
            {
                XElement el = new XElement("SMTPPassword");
                el.Value = txtSmtpPassword.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpPassword.Text;
            }

            node = doc.Root.Element(section)
                         .Element("SMTPPort");
            if (node == null)
            {
                XElement el = new XElement("SMTPPort");
                el.Value = txtSmtpPort.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtSmtpPort.Text;
            }

            node = doc.Root.Element(section)
                         .Element("SMTPSsl");
            if (node == null)
            {
                XElement el = new XElement("SMTPSsl");
                el.Value = cbSMTPSecure.Checked.ToString();
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = cbSMTPSecure.Checked.ToString();
            }
            section = "Config";
            node = doc.Root.Element(section)
                         .Element("PrimaryLocation");
            if (node == null)
            {
                XElement el = new XElement("PrimaryLocation");
                //   el.Value = txtTestPrimaryLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                //       node.Value = txtTestPrimaryLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("WinSCPLocation");
            if (node == null)
            {
                XElement el = new XElement("WinSCPLocation");
                //   el.Value = txtTestPrimaryLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = edWinSCPLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("CNodeLocation");
            if (node == null)
            {
                XElement el = new XElement("CNodeLocation");
                el.Value = txtCNodeLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtCNodeLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("ArchiveLocation");
            if (node == null)
            {
                XElement el = new XElement("ArchiveLocation");
                //       el.Value = txtTestArchiveLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                //        node.Value = txtTestArchiveLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TemporaryLocation");
            if (node == null)
            {
                XElement el = new XElement("TemporaryLocation");
                //        el.Value = txtTempLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                //       node.Value = txtTempLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestPrimaryLocation");
            if (node == null)
            {
                XElement el = new XElement("TestPrimaryLocation");
                //       el.Value = txtTestPrimaryLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                //         node.Value = txtTestPrimaryLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestArchiveLocation");
            if (node == null)
            {
                XElement el = new XElement("TestArchiveLocation");
                //         el.Value = txtArchiveLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                //          node.Value = txtArchiveLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("TestTemporaryLocation");
            if (node == null)
            {
                XElement el = new XElement("TestTemporaryLocation");
                el.Value = edTestTempLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = edTestTempLocation.Text;
            }
            node = doc.Root.Element(section)
                         .Element("TestOutputLocation");
            if (node == null)
            {
                XElement el = new XElement("TestOutputLocation");
                el.Value = edTestOutputLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = edTestOutputLocation.Text;
            }
            node = doc.Root.Element(section)
                         .Element("ProdTemporaryLocation");
            if (node == null)
            {
                XElement el = new XElement("ProdTemporaryLocation");
                el.Value = edProdTempLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = edProdTempLocation.Text;
            }
            node = doc.Root.Element(section)
                         .Element("ProdOutputLocation");
            if (node == null)
            {
                XElement el = new XElement("ProdOutputLocation");
                el.Value = edProdOutputLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = edProdOutputLocation.Text;
            }
            node = doc.Root.Element(section)
                         .Element("TestPickupLocation");
            if (node == null)
            {
                XElement el = new XElement("TestPickupLocation");
                //         el.Value = txtTestPickupLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                ///         node.Value = txtTestPickupLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("PickupLocation");
            if (node == null)
            {
                XElement el = new XElement("PickupLocation");
                //         el.Value = txtPickupLocation.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                //         node.Value = txtPickupLocation.Text;
            }

            node = doc.Root.Element(section)
                         .Element("PollTime");
            if (node == null)
            {
                XElement el = new XElement("PollTime");
                el.Value = txtPollTime.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtPollTime.Text;
            }

            node = doc.Root.Element(section)
                         .Element("RefreshTime");
            if (node == null)
            {
                XElement el = new XElement("RefreshTime");
                el.Value = txtRefresh.Text;
                XElement xNewnode = doc.Root.Element(section);
                xNewnode.Add(el);
            }
            else
            {
                node.Value = txtRefresh.Text;
            }
            node = doc.Root.Element(section)
                .Element("AlertsTo");
            if (node == null)
            {
                XElement el = new XElement("AlertsTo");
                el.Value = txtAlertsTo.Text;
                XElement xNewNode = doc.Root.Element(section);
                xNewNode.Add(el);
            }
            else
            {
                node.Value = txtAlertsTo.Text;
            }

            node = doc.Root.Element(section)
                .Element("LibraryName");
            if (node == null)
            {
                XElement el = new XElement("LibraryName");
                el.Value = txtLibraryName.Text;
                XElement xNewNode = doc.Root.Element(section);
                xNewNode.Add(el);
            }
            else
            {
                node.Value = txtLibraryName.Text;
            }
            doc.Save(Globals.AppConfig);
            MessageBox.Show("Application Settings saved.");
        }

        private void btnDirXml_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtCNodeLocation.Text;
            fd.ShowDialog();
            txtCNodeLocation.Text = fd.SelectedPath;


        }

        private void bbProdTempLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edProdTempLocation.Text;
            fd.ShowDialog();
            edProdTempLocation.Text = fd.SelectedPath;
        }

        private void bbProdOutputLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edProdOutputLocation.Text;
            fd.ShowDialog();
            edProdOutputLocation.Text = fd.SelectedPath;
        }

        private void bbTestTempLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edTestTempLocation.Text;
            fd.ShowDialog();
            edTestTempLocation.Text = fd.SelectedPath;
        }

        private void bbTestOutputLocation_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edTestOutputLocation.Text;
            fd.ShowDialog();
            edTestOutputLocation.Text = fd.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.FileName = edWinSCPLocation.Text;
            fd.ShowDialog();
            edWinSCPLocation.Text = fd.FileName;
        }

        private void btnCTCTest_Click(object sender, EventArgs e)
        {
            txtConnectionResults.Clear();
            txtConnectionResults.Text = "";
            NodeData.IConnectionManager cm = new ConnectionManager(txtDatabaseServer.Text, txtDatabaseName.Text, txtUserName.Text, txtPassword.Text);
            string connstring = cm.ConnString;
            using (NodeDataContext _context = new NodeDataContext(connstring))
            {
                try
                {
                    if (_context.Database.Exists())
                    {
                        txtConnectionResults.Text += "Database Connection Ok.";
                    }
                }
                catch (Exception ex)
                {
                    txtConnectionResults.Text += "Error Connecting to the Database. Error was :" + ex.GetType().Name + Environment.NewLine + "Error details: " + ex.Message;
                }

            }
        }
    }
}
