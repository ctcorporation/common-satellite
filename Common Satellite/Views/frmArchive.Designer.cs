﻿namespace Common_Satellite.Views
{
    partial class frmArchive
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bbClose = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbProcessSelection = new System.Windows.Forms.ComboBox();
            this.dtReceived = new System.Windows.Forms.DateTimePicker();
            this.edProcessedFileName = new System.Windows.Forms.TextBox();
            this.edOriginalFileName = new System.Windows.Forms.TextBox();
            this.edRecipientId = new System.Windows.Forms.TextBox();
            this.edSenderID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.cmbSelectProfile = new System.Windows.Forms.ComboBox();
            this.dgTaskList = new System.Windows.Forms.DataGridView();
            this.cmTaskList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsReProcess = new System.Windows.Forms.ToolStripMenuItem();
            this.tsRetrieveFromArchive = new System.Windows.Forms.ToolStripMenuItem();
            this.bbFind = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgTaskList)).BeginInit();
            this.cmTaskList.SuspendLayout();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(702, 415);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(398, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 15);
            this.label4.TabIndex = 28;
            this.label4.Text = "Select Profile";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 15);
            this.label3.TabIndex = 27;
            this.label3.Text = "Select Customer";
            // 
            // cmbProcessSelection
            // 
            this.cmbProcessSelection.FormattingEnabled = true;
            this.cmbProcessSelection.Items.AddRange(new object[] {
            "(none selected)",
            "FTPIn",
            "Pickup",
            "Email",
            "CTCAdaptor"});
            this.cmbProcessSelection.Location = new System.Drawing.Point(375, 51);
            this.cmbProcessSelection.Name = "cmbProcessSelection";
            this.cmbProcessSelection.Size = new System.Drawing.Size(121, 21);
            this.cmbProcessSelection.TabIndex = 26;
            // 
            // dtReceived
            // 
            this.dtReceived.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dtReceived.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtReceived.Location = new System.Drawing.Point(101, 53);
            this.dtReceived.Name = "dtReceived";
            this.dtReceived.Size = new System.Drawing.Size(132, 20);
            this.dtReceived.TabIndex = 25;
            // 
            // edProcessedFileName
            // 
            this.edProcessedFileName.Location = new System.Drawing.Point(495, 86);
            this.edProcessedFileName.Name = "edProcessedFileName";
            this.edProcessedFileName.Size = new System.Drawing.Size(280, 20);
            this.edProcessedFileName.TabIndex = 24;
            // 
            // edOriginalFileName
            // 
            this.edOriginalFileName.Location = new System.Drawing.Point(131, 83);
            this.edOriginalFileName.Name = "edOriginalFileName";
            this.edOriginalFileName.Size = new System.Drawing.Size(221, 20);
            this.edOriginalFileName.TabIndex = 23;
            // 
            // edRecipientId
            // 
            this.edRecipientId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edRecipientId.Location = new System.Drawing.Point(367, 19);
            this.edRecipientId.Name = "edRecipientId";
            this.edRecipientId.Size = new System.Drawing.Size(100, 20);
            this.edRecipientId.TabIndex = 22;
            // 
            // edSenderID
            // 
            this.edSenderID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edSenderID.Location = new System.Drawing.Point(93, 22);
            this.edSenderID.Name = "edSenderID";
            this.edSenderID.Size = new System.Drawing.Size(100, 20);
            this.edSenderID.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(364, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(125, 15);
            this.label9.TabIndex = 20;
            this.label9.Text = "Processed File Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 15);
            this.label8.TabIndex = 19;
            this.label8.Text = "Original File Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(265, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "Receive Process";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 15);
            this.label6.TabIndex = 17;
            this.label6.Text = "Receive Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 15);
            this.label1.TabIndex = 15;
            this.label1.Text = "Sender ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(263, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "Recipient ID";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(118, 113);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(274, 21);
            this.cmbCustomer.TabIndex = 29;
            this.cmbCustomer.SelectedValueChanged += new System.EventHandler(this.cmbCustomer_SelectedValueChanged);
            // 
            // cmbSelectProfile
            // 
            this.cmbSelectProfile.FormattingEnabled = true;
            this.cmbSelectProfile.Location = new System.Drawing.Point(495, 112);
            this.cmbSelectProfile.Name = "cmbSelectProfile";
            this.cmbSelectProfile.Size = new System.Drawing.Size(280, 21);
            this.cmbSelectProfile.TabIndex = 30;
            // 
            // dgTaskList
            // 
            this.dgTaskList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTaskList.ContextMenuStrip = this.cmTaskList;
            this.dgTaskList.Location = new System.Drawing.Point(18, 189);
            this.dgTaskList.Name = "dgTaskList";
            this.dgTaskList.Size = new System.Drawing.Size(757, 220);
            this.dgTaskList.TabIndex = 31;
            // 
            // cmTaskList
            // 
            this.cmTaskList.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cmTaskList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsReProcess,
            this.tsRetrieveFromArchive});
            this.cmTaskList.Name = "cmTaskList";
            this.cmTaskList.Size = new System.Drawing.Size(226, 52);
            // 
            // tsReProcess
            // 
            this.tsReProcess.Name = "tsReProcess";
            this.tsReProcess.Size = new System.Drawing.Size(225, 24);
            this.tsReProcess.Text = "Re-Process File";
            // 
            // tsRetrieveFromArchive
            // 
            this.tsRetrieveFromArchive.Name = "tsRetrieveFromArchive";
            this.tsRetrieveFromArchive.Size = new System.Drawing.Size(225, 24);
            this.tsRetrieveFromArchive.Text = "Extract File from Archive";
            // 
            // bbFind
            // 
            this.bbFind.Location = new System.Drawing.Point(700, 148);
            this.bbFind.Name = "bbFind";
            this.bbFind.Size = new System.Drawing.Size(75, 23);
            this.bbFind.TabIndex = 33;
            this.bbFind.Text = "&Find";
            this.bbFind.UseVisualStyleBackColor = true;
            this.bbFind.Click += new System.EventHandler(this.bbFind_Click);
            // 
            // frmArchive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 450);
            this.Controls.Add(this.bbFind);
            this.Controls.Add(this.dgTaskList);
            this.Controls.Add(this.cmbSelectProfile);
            this.Controls.Add(this.cmbCustomer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbProcessSelection);
            this.Controls.Add(this.dtReceived);
            this.Controls.Add(this.edProcessedFileName);
            this.Controls.Add(this.edOriginalFileName);
            this.Controls.Add(this.edRecipientId);
            this.Controls.Add(this.edSenderID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bbClose);
            this.Name = "frmArchive";
            this.Text = "Search Archive";
            this.Load += new System.EventHandler(this.frmArchive_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgTaskList)).EndInit();
            this.cmTaskList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbProcessSelection;
        private System.Windows.Forms.DateTimePicker dtReceived;
        private System.Windows.Forms.TextBox edProcessedFileName;
        private System.Windows.Forms.TextBox edOriginalFileName;
        private System.Windows.Forms.TextBox edRecipientId;
        private System.Windows.Forms.TextBox edSenderID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.ComboBox cmbSelectProfile;
        private System.Windows.Forms.DataGridView dgTaskList;
        private System.Windows.Forms.ContextMenuStrip cmTaskList;
        private System.Windows.Forms.ToolStripMenuItem tsReProcess;
        private System.Windows.Forms.ToolStripMenuItem tsRetrieveFromArchive;
        private System.Windows.Forms.Button bbFind;
    }
}