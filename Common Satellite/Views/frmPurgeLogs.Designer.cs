﻿namespace Common_Satellite.Views
{
    partial class frmPurgeLogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgeLogs));
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbInformation = new System.Windows.Forms.CheckBox();
            this.cbWarning = new System.Windows.Forms.CheckBox();
            this.cbError = new System.Windows.Forms.CheckBox();
            this.dtPurge = new System.Windows.Forms.DateTimePicker();
            this.cbAll = new System.Windows.Forms.CheckBox();
            this.btnPurge = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(268, 144);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(59, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Before Date:";
            // 
            // cbInformation
            // 
            this.cbInformation.AutoSize = true;
            this.cbInformation.Checked = true;
            this.cbInformation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbInformation.Location = new System.Drawing.Point(15, 47);
            this.cbInformation.Name = "cbInformation";
            this.cbInformation.Size = new System.Drawing.Size(116, 17);
            this.cbInformation.TabIndex = 2;
            this.cbInformation.Text = "Include Information";
            this.cbInformation.UseVisualStyleBackColor = true;
            // 
            // cbWarning
            // 
            this.cbWarning.AutoSize = true;
            this.cbWarning.Checked = true;
            this.cbWarning.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWarning.Location = new System.Drawing.Point(15, 70);
            this.cbWarning.Name = "cbWarning";
            this.cbWarning.Size = new System.Drawing.Size(104, 17);
            this.cbWarning.TabIndex = 3;
            this.cbWarning.Text = "Include Warning";
            this.cbWarning.UseVisualStyleBackColor = true;
            // 
            // cbError
            // 
            this.cbError.AutoSize = true;
            this.cbError.Checked = true;
            this.cbError.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbError.Location = new System.Drawing.Point(15, 93);
            this.cbError.Name = "cbError";
            this.cbError.Size = new System.Drawing.Size(86, 17);
            this.cbError.TabIndex = 4;
            this.cbError.Text = "Include Error";
            this.cbError.UseVisualStyleBackColor = true;
            // 
            // dtPurge
            // 
            this.dtPurge.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPurge.Location = new System.Drawing.Point(85, 21);
            this.dtPurge.Name = "dtPurge";
            this.dtPurge.Size = new System.Drawing.Size(95, 20);
            this.dtPurge.TabIndex = 5;
            // 
            // cbAll
            // 
            this.cbAll.AutoSize = true;
            this.cbAll.Location = new System.Drawing.Point(12, 116);
            this.cbAll.Name = "cbAll";
            this.cbAll.Size = new System.Drawing.Size(107, 17);
            this.cbAll.TabIndex = 6;
            this.cbAll.Text = "Purge Everything";
            this.cbAll.UseVisualStyleBackColor = true;
            // 
            // btnPurge
            // 
            this.btnPurge.Image = ((System.Drawing.Image)(resources.GetObject("btnPurge.Image")));
            this.btnPurge.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPurge.Location = new System.Drawing.Point(268, 115);
            this.btnPurge.Name = "btnPurge";
            this.btnPurge.Size = new System.Drawing.Size(59, 23);
            this.btnPurge.TabIndex = 7;
            this.btnPurge.Text = "&Purge";
            this.btnPurge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPurge.UseVisualStyleBackColor = true;
            this.btnPurge.Click += new System.EventHandler(this.btnPurge_Click);
            // 
            // frmPurgeLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 179);
            this.Controls.Add(this.btnPurge);
            this.Controls.Add(this.cbAll);
            this.Controls.Add(this.dtPurge);
            this.Controls.Add(this.cbError);
            this.Controls.Add(this.cbWarning);
            this.Controls.Add(this.cbInformation);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPurgeLogs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Purge Logs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbInformation;
        private System.Windows.Forms.CheckBox cbWarning;
        private System.Windows.Forms.CheckBox cbError;
        private System.Windows.Forms.DateTimePicker dtPurge;
        private System.Windows.Forms.CheckBox cbAll;
        private System.Windows.Forms.Button btnPurge;
    }
}