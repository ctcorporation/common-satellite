﻿using Common_Satellite.Classes;
using NodeData;
using NodeData.DTO;
using NodeData.Models;

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using XMLLocker.CTC;
using XMLLocker.TMC;


namespace Common_Satellite.Views
{
    public partial class frmConvertData : Form
    {
        DataTable tableMapping;
        public frmConvertData()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {

            if ((Guid)cmbMapList.SelectedValue != Guid.Empty)
            {
                MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
                var mapHeaderID = mo.GetMapHeader((Guid)cmbMapList.SelectedValue);
                string lo = Globals.AppLogPath;
                string cp = string.Empty;
                DateTime ti = DateTime.Now;
                string er = string.Empty;
                string di = string.Empty;
                if (string.IsNullOrEmpty(txtFileToConvert.Text))
                {
                    MessageBox.Show("Please select a file to convert", "Import Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                FileInfo fi = new FileInfo(txtFileToConvert.Text);
                DataTable tb = new DataTable();
                try
                {

                    ExcelToTable excelToTable = new ExcelToTable(fi.FullName);
                    switch (fi.Extension.ToUpper())
                    {
                        case ".XLS":
                            if (cmbMapList.Text == "Normans Australia Import")
                            {
                                tb = excelToTable.XlsToTable(fi.FullName, "# of boxes");

                            }
                            else
                            {
                                tb = excelToTable.XlsToTable(fi.FullName);
                            }

                            break;
                        case ".XLSX":
                            if (cmbMapList.Text == "Normans Australia Import")
                            {

                                tb = excelToTable.ToTable(fi.FullName, "# of boxes");
                            }
                            else
                            {
                                tb = excelToTable.ToTable(fi.FullName);
                            }
                            break;
                        case ".CSV":
                            tb = CsvHelper.ConvertCSVtoDataTable(fi.FullName);
                            break;
                    }
                    Convert(tb, fi.FullName);


                }
                catch (IOException ex)
                {
                    MessageBox.Show("File is in use by another process.", "Unable to open File", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
            }

        }

        private void Convert(DataTable tb, string filename)
        {

            // TODO : Fix the Dynamic method. 
            // MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            //var method = mo.GetOperationMethod((Guid)cmbOperationList.SelectedValue);
            //MethodInfo custMethodToRun = this.GetType().GetMethod(method);
            //var varResult = custMethodToRun.Invoke(this, new object[] { tb, txtOwner.Text, txtCustomer.Text });
            //if ( varResult==null)
            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var mapHeaderID = mo.GetMapHeader((Guid)cmbMapList.SelectedValue);
            var mapProc = mo.GetOperationMethod((Guid)cmbOperationList.SelectedValue);
            var mapList = mo.GetMap((Guid)cmbMapList.SelectedValue);
            CommonToCw commonToCW = new CommonToCw(Globals.AppLogPath);
            ToCargowise toCargowise = new ToCargowise(Globals.AppLogPath);
            ConvertOrder order;
            switch (cmbOperationList.Text)
            {
                case "Organisation":
                    ConvertData cd = new ConvertData();
                    var commonCompanies = cd.CreateCommonOrgs(tb, txtOwner.Text, txtCustomer.Text);
                    
                    var native = toCargowise.CreateOrgFromCompany(commonCompanies);
                    break;
                case "Receipt":
                    var recptmap = mo.GetMap((Guid)cmbMapList.SelectedValue);
                    OrderReceipt or = new OrderReceipt(Globals.ConnString.ConnString, tb, recptmap);
                    or.OutputPath = txtOutputFolder.Text;
                    or.RecipientId = txtCustomer.Text;
                    or.SenderId = txtOwner.Text;

                    or.MailSettings = (NodeResources.MailServerSettings)Globals.MailServerSettings;
                    if (!or.ConvertToCommonReceipt("andy.duggan@ctcorp.com.au", (Guid)cmbMapList.SelectedValue))
                    {
                        using (NodeResources.MailModule mailModule = new NodeResources.MailModule(Globals.MailServerSettings))
                        {
                            mailModule.SendMsg(string.Empty, Globals.AlertsTo, "Error Converting file: " + txtFileToConvert.Text, "Error: " + or.ErrorMsg);
                        }

                    }

                    //ToCargowise commonFunc = new ToCargowise("andy.duggan@ctcorp.com.au");
                    //var cwFile = commonFunc.CWFromCommon(rcptFile);


                    break;
                case "Tracking Order":

                    ConvertPo po = new ConvertPo(tb, Globals.ConnString.ConnString);
                    if ((Guid)cmbMapList.SelectedValue != Guid.Empty)
                    {
                        var objType = Type.GetType(mapProc);

                        var common = po.CreateCommon(mapList, mapHeaderID.MH_ID);
                        if (common != null)
                        {
                            common.IdendtityMatrix = new NodeFileIdendtityMatrix
                            {
                                CustomerId = txtCustomer.Text,
                                SenderId = txtOwner.Text,
                                EventCode = "DIM",
                                PurposeCode = "CTC",
                                DocumentType = "PORD",
                                FileDateTime = DateTime.Now.ToString("yyyyMMddThh:mm:ss")

                            };

                            commonToCW.CreateOrderFile(common, txtOutputFolder.Text);

                        }
                    }

                    break;

                case "Transport Job":

                    switch (mapHeaderID.MH_Name)
                    {
                        case "Normans Australia Import":
                            ConvertTransport tj = new ConvertTransport(tb, Globals.ConnString.ConnString);
                            var common = tj.CreateCommon(mapList, mapHeaderID.MH_ID);
                            if ((Guid)cmbMapList.SelectedValue != Guid.Empty)
                            {
                                var objType = Type.GetType(mapProc);
                                if (common != null)
                                {
                                    common.IdendtityMatrix = new NodeFileIdendtityMatrix
                                    {
                                        CustomerId = txtCustomer.Text,
                                        SenderId = txtOwner.Text,
                                        EventCode = "DIM",
                                        PurposeCode = "CTC",
                                        DocumentType = "PORD",
                                        FileDateTime = DateTime.Now.ToString("yyyyMMddThh:mm:ss")

                                    };
                                    common.TransportJob[0].OrderNo = poNo.Text;
                                    string job = (string)jobType.SelectedValue;
                                    common.TransportJob[0].JobTypeCode = job.Replace(" ", String.Empty);
                                    //common.TransportJob[0].JobType = GetJobTypeDescription(jobType.Text);
                                    common.TransportJob[0].JobType = (string)jobType.GetItemText(jobType.SelectedItem);
                                    if (legNo.Text != "")
                                    {
                                        common.TransportJob[0].StartLeg = int.Parse(legNo.Text);
                                    }
                                    else
                                    {
                                        common.TransportJob[0].StartLeg = 1;
                                    }

                                    commonToCW.CreateTransportFile(common, txtOutputFolder.Text);

                                }
                            }
                            break;
                        case "TMC Carrier":
                            TMCToCommon tmc = new TMCToCommon();
                            vw_CustomerProfile profile = new vw_CustomerProfile();
                            profile.P_SENDERID = mapHeaderID.MH_Customer;
                            profile.C_CODE = mapHeaderID.MH_Recipient;
                            common = tmc.ConvertTMC(filename, profile);
                            commonToCW.CreateTMCFile(common, txtOutputFolder.Text);

                            break;
                    }
                    break;


                case "Commercial Invoice": break;
                case "Native Product":
                    var map = (from x in mo.GetMap((Guid)cmbMapList.SelectedValue)
                               select new XMLLocker.MapOperation
                               {
                                   MD_DataType = x.MD_DataType,
                                   MD_FromField = x.MD_FromField,
                                   MD_MapDescription = x.MD_MapDescription,
                                   MD_ToField = x.MD_ToField

                               }).ToList();
                    NodeFile nodeFile = NodeFunctions.ImportProducts(tb, txtOwner.Text, txtCustomer.Text, map, Globals.AppLogPath);
                    if (nodeFile == null)
                    {
                        MessageBox.Show("Error Converting. Check Log Files", "Error converting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    commonToCW.CreateProductFile(nodeFile, XMLLocker.Cargowise.NDM.Action.MERGE, txtOutputFolder.Text);
                    break;
                case "Order": break;
                case "WH Out":
                    
                    order = new ConvertOrder(tb, Globals.ConnString.ConnString);
                    if ((Guid)cmbMapList.SelectedValue != Guid.Empty)
                    {
                        var objType = Type.GetType(mapProc);

                        var common = order.CreateCommon(mapList, mapHeaderID.MH_ID);
                        if (common != null)
                        {
                            common.IdendtityMatrix = new NodeFileIdendtityMatrix
                            {
                                CustomerId = txtCustomer.Text,
                                SenderId = txtOwner.Text,
                                EventCode = "DIM",
                                PurposeCode = "CTC",
                                DocumentType = "WORD",
                                FileDateTime = DateTime.Now.ToString("yyyyMMddThh:mm:ss")

                            };

                            //     commonToCW.CreateWarehouseOrder(common, txtOutputFolder.Text);
                        }
                    }

                    break;

                case "Warehouse Order":

                    order = new ConvertOrder(tb, Globals.ConnString.ConnString);
                    if ((Guid)cmbMapList.SelectedValue != Guid.Empty)
                    {
                        var objType = Type.GetType(mapProc);

                        var common = order.CreateCommonOrder(mapList, mapHeaderID.MH_ID);
                        if (common != null)
                        {
                            common.IdendtityMatrix = new NodeFileIdendtityMatrix
                            {
                                CustomerId = txtCustomer.Text,
                                SenderId = txtOwner.Text,
                                EventCode = "DIM",
                                PurposeCode = "CTC",
                                DocumentType = "WORD",
                                FileDateTime = DateTime.Now.ToString("yyyyMMddThh:mm:ss")

                            };

                            //     commonToCW.CreateWarehouseOrder(common, txtOutputFolder.Text);
                            foreach(var item in common.WarehouseOrders)
                            {
                               XMLLocker.Cargowise.XUS.UniversalInterchange cwFile = toCargowise.CWFromCommonWarehouse(item);
                                cwFile.Header.SenderID = common.IdendtityMatrix.SenderId;
                                cwFile.Header.RecipientID = common.IdendtityMatrix.CustomerId;
                                var file = toCargowise.ToFile(cwFile, common.IdendtityMatrix.CustomerId + "-" + item.OrderNumber, txtOutputFolder.Text);
                            }
                            
                        }
                    }
                    break;


            }
            // TODO - Fix the MapOperation Parameter. 

            MessageBox.Show("Conversion Complete", "File Conversion", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private object GetMapOperation(string text)
        {
            throw new NotImplementedException();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();

            fd.ShowDialog();
            txtFileToConvert.Text = fd.FileName;
            GetFieldNames(txtFileToConvert.Text);
        }

        private void GetFieldNames(string text)
        {
            FileInfo fi = new FileInfo(txtFileToConvert.Text);
            DataTable tb = new DataTable();
            ExcelToTable excelToTable = new ExcelToTable(fi.FullName);
            switch (fi.Extension.ToUpper())
            {
                case ".XLS":
                    if (cmbMapList.Text == "Normans Australia Import")
                    {
                        tb = excelToTable.XlsToTable(fi.FullName, "# of boxes");

                    }
                    else
                    {
                        tb = excelToTable.XlsToTable(fi.FullName);
                    }

                    break;
                case ".XLSX":
                    if (cmbMapList.Text == "Normans Australia Import")
                    {
                        tb = excelToTable.ToTable(fi.FullName, "# of boxes");
                    }
                    else
                    {
                        tb = excelToTable.ToTable(fi.FullName);
                    }
                    break;
                case ".CSV":
                    tb = CsvHelper.ConvertCSVtoDataTable(fi.FullName);

                    break;

            }
            lvFieldList.Items.Clear();

            foreach (DataColumn dc in tb.Columns)
            {
                lvFieldList.Items.Add(dc.ColumnName);
            }

        }

        public string GetJobTypeDescription(string code)
        {
            string jobtype = "";
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                var enumVal = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == "JOBTYPE" && x.CW_ENUM == code).FirstOrDefault();
                jobtype = enumVal.CW_MAPVALUE; 
            }
            return jobtype;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = txtOutputFolder.Text;
            fd.ShowDialog();
            txtOutputFolder.Text = fd.SelectedPath;

        }

        private void frmConvertProductData_Load(object sender, EventArgs e)
        {
            FillMapOperations();
            FillFieldTypes();
            txtOutputFolder.Text = Globals.CNodePath;
            tableMapping = new DataTable();
            tableMapping.Columns.Add("MapFrom", typeof(string));
            tableMapping.Columns.Add("MapTo", typeof(string));
            tableMapping.Columns.Add("DataType", typeof(string));
            dgMapping.DataSource = tableMapping;
        }

        private void FillFieldTypes()
        {
            var dataItems = new List<KeyValuePair<string, string>>();
            dataItems.Add(new KeyValuePair<string, string>("", "N/A"));
            dataItems.Add(new KeyValuePair<string, string>("BOO", "Boolean"));
            dataItems.Add(new KeyValuePair<string, string>("DAT", "Date/Time"));
            dataItems.Add(new KeyValuePair<string, string>("STR", "String"));
            dataItems.Add(new KeyValuePair<string, string>("NUM", "Number"));
            dataItems.Add(new KeyValuePair<string, string>("INT", "Integer"));
            cmbDataTypes.DataSource = dataItems;
            cmbDataTypes.DisplayMember = "Value";
            cmbDataTypes.ValueMember = "Key";

        }

        private void FillMapOperations()
        {
            var mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var opsList = mo.GetOperations();
            opsList.Insert(0, new KeyValuePair<Guid, string>(Guid.Empty, "(none selected)"));
            cmbOperationList.DataSource = opsList;
            cmbOperationList.DisplayMember = "Value";
            cmbOperationList.ValueMember = "Key";
        }

        private void FillJobTypes()
        {
            var mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var opsList = mo.GetJobTypes();
            opsList.Insert(0, new KeyValuePair<String, string>("", "(none selected)"));
            jobType.DataSource = opsList;
            jobType.DisplayMember = "Value";
            jobType.ValueMember = "Key";
        }

        private void FillRequiredFields(Guid selectedItem)
        {
            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var fieldList = mo.GetFields(selectedItem);
            lvRequiredFields.DataSource = fieldList;
            lvRequiredFields.DisplayMember = "MM_FieldName";
            lvRequiredFields.ValueMember = "MM_FieldName";

        }

        private void FillMapList(Guid selectedValue)
        {
            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var mapList = mo.GetMapHeaders(selectedValue);
            cmbMapList.DisplayMember = "Value";
            cmbMapList.ValueMember = "Key";
            if (mapList != null)
            {
                mapList.Insert(0, new KeyValuePair<Guid, string>(Guid.Empty, "(none selected)"));
                cmbMapList.DataSource = mapList;


            }

        }
        private void btnAddRequiredField_Click(object sender, EventArgs e)
        {

            AddRequiredField();
        }

        private void AddRequiredField()
        {
            if (cmbOperationList.SelectedValue.ToString() != "N/A")
            {
                var currentLabel = btnAddMapping.Text.Split(':');
                if (currentLabel.Length == 2)
                {
                    currentLabel[1] = lvRequiredFields.SelectedValue.ToString();
                    btnAddMapping.Text = currentLabel[0] + " : " + currentLabel[1];
                }

                else
                {
                    btnAddMapping.Text = " : " + lvRequiredFields.SelectedValue.ToString();
                }
            }
        }

        private void btnAddField_Click(object sender, EventArgs e)
        {
            AddField();
        }

        private void AddField()
        {

            if (cmbOperationList.SelectedValue.ToString() != "N/A")
            {

                var currentLabel = btnAddMapping.Text.Split(':');
                if (currentLabel.Length == 2)
                {
                    if (cbOverride.Checked)
                    {
                        currentLabel[0] = txtStaticValue.Text;
                    }
                    else
                    {
                        currentLabel[0] = lvFieldList.SelectedItem.ToString();
                    }
                    btnAddMapping.Text = currentLabel[0] + " : " + currentLabel[1];
                }

                else
                {
                    if (cbOverride.Checked)
                    {
                        btnAddMapping.Text = txtStaticValue.Text + " : ";
                    }
                    else
                    {
                        btnAddMapping.Text = lvFieldList.SelectedItem.ToString() + " : ";
                    }
                }
            }
        }

        private void btnAddMapping_Click(object sender, EventArgs e)
        {
            if (cmbDataTypes.SelectedIndex > 0 || cbOverride.Checked)
            {
                if (btnAddMapping.Text.Split(':').Count() == 2)
                {

                    DataRow dr = tableMapping.NewRow();
                    var currentLabel = btnAddMapping.Text.Split(':');
                    if (currentLabel.Length == 2)
                    {
                        var required = (from DataRow x in tableMapping.Rows
                                        where (string)x["MapFrom"] == currentLabel[1].Trim()
                                        select x).FirstOrDefault();
                        if (required != null)
                        {
                            DialogResult qresult = MessageBox.Show("Field " + currentLabel[1] + " is already mapped." + Environment.NewLine + "Do you want to continue?", "Duplicate Field Mapped", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (qresult == DialogResult.No)
                            {
                                return;
                            }
                        }

                        dr["MapFrom"] = currentLabel[1].Trim();
                        var avail = (from DataRow x in tableMapping.Rows
                                     where (string)x["MapTo"] == currentLabel[0].Trim()
                                     select x).FirstOrDefault();
                        if (avail != null)
                        {
                            DialogResult qresult = MessageBox.Show("Field " + currentLabel[0] + " is already mapped." + Environment.NewLine + "Do you want to continue?", "Duplicate Field Mapped", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (qresult == DialogResult.No)
                            {
                                return;
                            }
                        }

                        dr["MapTo"] = currentLabel[0].Trim();
                        dr["DataType"] = cbOverride.Checked ? "OVR" : cmbDataTypes.SelectedValue.ToString();
                        tableMapping.Rows.Add(dr);



                    }
                }
            }


        }


        private void btnLoadMapping_Click(object sender, EventArgs e)
        {
            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            var mapHeader = mo.GetMapHeader((Guid)cmbMapList.SelectedValue);
            if (mapHeader != null)
            {
                txtCustomer.Text = mapHeader.MH_Recipient;
                txtOwner.Text = mapHeader.MH_Customer;
                txtMapName.Text = mapHeader.MH_Name;
                txtMapDescription.Text = mapHeader.MH_Description;
                var map = mo.GetMap((Guid)cmbMapList.SelectedValue);
                if (map.Count > 0)
                {
                    tableMapping.Rows.Clear();
                    foreach (var m in map)
                    {
                        DataRow row = tableMapping.NewRow();
                        row["MapFrom"] = m.MD_FromField;
                        row["MapTo"] = m.MD_ToField;
                        row["DataType"] = m.MD_DataType;
                        tableMapping.Rows.Add(row);
                    }
                }
                if (mapHeader.MH_Name == "Normans Australia Import")
                {
                    groupBox5.Visible = true;
                    FillJobTypes();
                }
                dgMapping.Refresh();
                MessageBox.Show("Mapping Profile Loaded.", "Custom Mapping Profile", MessageBoxButtons.OK, MessageBoxIcon.Information);


            }


        }

        private void lvFieldList_DoubleClick(object sender, EventArgs e)
        {
            AddField();
        }

        private void lvRequiredFields_DoubleClick(object sender, EventArgs e)
        {
            AddRequiredField();
        }

        private void dgMapping_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {

        }

        private void dgMapping_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var field = e.Row.Cells[2].Value.ToString().Trim();
            var required = e.Row.Cells[1].Value.ToString().Trim();

            //if (!lvRequiredFields.Items.Contains(required))
            //{
            //    lvRequiredFields.Items.Add(required);
            //}

            //if (!lvFieldList.Items.Contains(field))
            //{
            //    lvFieldList.Items.Add(field);
            //}


        }

        private void cmbOperationList_DropDown(object sender, EventArgs e)
        {
            cmbOperationList.SelectedItem = null;
        }

        private void btnSaveMapping_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMapDescription.Text))
            {
                MessageBox.Show("You cannot have a blank Map Description", "Save Mapping Operation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
            MapHeader mapheader;
            if (cmbMapList.SelectedIndex > 0)
            {
                mapheader = mo.GetMapHeader((Guid)cmbMapList.SelectedValue);
                mapheader.MH_Name = txtMapName.Text;
                mapheader.MH_Description = txtMapDescription.Text;
                mapheader.MH_Recipient = txtCustomer.Text;
                mapheader.MH_Customer = txtOwner.Text;
            }
            else
            {
                mapheader = mo.GetMapHeaderByName(txtMapName.Text);
                if (mapheader == null)
                {
                    mapheader = new MapHeader
                    {
                        MH_Name = txtMapName.Text,
                        MH_Description = txtMapDescription.Text,
                        MH_Customer = txtOwner.Text,
                        MH_Recipient = txtCustomer.Text,
                        MH_MO = (Guid)cmbOperationList.SelectedValue
                    };
                    mapheader = mo.AddMapHeader(mapheader);
                }
            }
            List<MapOperation> map = new List<MapOperation>();
            foreach (DataRow row in tableMapping.Rows)
            {
                MapOperation md = new MapOperation
                {
                    MD_MapDescription = txtMapDescription.Text,
                    MD_FromField = row["MapFrom"].ToString(),
                    MD_ToField = row["MapTo"].ToString(),
                    MD_DataType = row["DataType"].ToString()
                };
                map.Add(md);
            }
            mo.AddMapOperation(mapheader, map);
            var item = (Guid)cmbMapList.SelectedValue;
            FillMapList((Guid)cmbOperationList.SelectedValue);
            cmbMapList.SelectedValue = item;
        }

        private void cmbOperationList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbOperationList.SelectedItem != null)
            {
                if ((Guid)cmbOperationList.SelectedValue != Guid.Empty)
                {
                    FillMapList((Guid)cmbOperationList.SelectedValue);
                    FillRequiredFields((Guid)cmbOperationList.SelectedValue);


                }
                else
                {

                }

            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void cbOverride_CheckedChanged(object sender, EventArgs e)
        {
            if (cbOverride.Checked)
            {
                cmbDataTypes.Enabled = false;
            }
            else
            {
                cmbDataTypes.Enabled = true;
            }

        }

        private void dgMapping_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }
    }

}

