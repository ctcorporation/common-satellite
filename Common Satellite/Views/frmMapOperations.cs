﻿using Common_Satellite.Classes;
using Common_Satellite.Resources;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmMapOperations : Form
    {
        private Guid opid = new Guid();
        public frmMapOperations()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmMapOperations_Load(object sender, EventArgs e)
        {
            FillOperations();
            tsMapOperation.Text = "Ready.";
            opid = Guid.Empty;

        }

        private void FillOperations()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                BindingSource bsOpsList = new BindingSource();
                var operations = uow.MappingMasterOperations.GetAll().ToList();
                if (operations.Count > 0)
                {
                    bsOpsList.DataSource = new SortableBindingList<MappingMasterOperation>(operations);
                    grdMappingOperations.DataSource = bsOpsList.DataSource;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MappingMasterOperation mo = new MappingMasterOperation
            {
                MO_Description = txtOperationDescription.Text,
                MO_OperationName = txtMapOperation.Text,
                MO_ID = opid
            };


            MappingMasterFiles mf = new MappingMasterFiles(Globals.ConnString.ConnString);
            var result = mf.SaveOperation(mo);
            switch (result)
            {
                case -1:
                    break;
                default:
                    tsMapOperation.Text = "Operation Added";
                    FillOperations();
                    txtMapOperation.Text = "";
                    txtOperationDescription.Text = "";
                    break;
            }
            opid = Guid.Empty;
        }

        private void grdMappingOperations_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                opid = (Guid)grdMappingOperations.Rows[e.RowIndex].Cells[0].Value;
                txtMapOperation.Text = (string)grdMappingOperations.Rows[e.RowIndex].Cells[1].Value;
                txtOperationDescription.Text = (string)grdMappingOperations.Rows[e.RowIndex].Cells[2].Value;


            }
        }

        private void grdMappingOperations_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Index > -1)
            {
                var opid = (Guid)grdMappingOperations.Rows[e.Row.Index].Cells[0].Value;
                MappingMasterFiles mo = new MappingMasterFiles(Globals.ConnString.ConnString);
                mo.RemoveOperation(opid);
                FillOperations();


            }
        }
    }
}
