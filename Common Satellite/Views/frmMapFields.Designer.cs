﻿namespace Common_Satellite.Views
{
    partial class frmMapFields
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMapFields));
            this.btnSave = new System.Windows.Forms.Button();
            this.bbClose = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbOperationList = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFieldName = new System.Windows.Forms.TextBox();
            this.dgFieldList = new System.Windows.Forms.DataGridView();
            this.idfield = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFieldList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(316, 257);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(58, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Image = ((System.Drawing.Image)(resources.GetObject("bbClose.Image")));
            this.bbClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bbClose.Location = new System.Drawing.Point(316, 286);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(58, 23);
            this.bbClose.TabIndex = 8;
            this.bbClose.Text = "&Close";
            this.bbClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 321);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(386, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(70, 17);
            this.toolStripStatusLabel1.Text = "tsMapFields";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Select Operation";
            // 
            // cmbOperationList
            // 
            this.cmbOperationList.FormattingEnabled = true;
            this.cmbOperationList.Location = new System.Drawing.Point(106, 12);
            this.cmbOperationList.Name = "cmbOperationList";
            this.cmbOperationList.Size = new System.Drawing.Size(274, 21);
            this.cmbOperationList.TabIndex = 12;
            this.cmbOperationList.DropDown += new System.EventHandler(this.cmbOperationList_DropDown);
            this.cmbOperationList.SelectionChangeCommitted += new System.EventHandler(this.cmbOperationList_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Field Name";
            // 
            // txtFieldName
            // 
            this.txtFieldName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtFieldName.Location = new System.Drawing.Point(84, 261);
            this.txtFieldName.Name = "txtFieldName";
            this.txtFieldName.Size = new System.Drawing.Size(209, 20);
            this.txtFieldName.TabIndex = 15;
            // 
            // dgFieldList
            // 
            this.dgFieldList.AllowUserToAddRows = false;
            this.dgFieldList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgFieldList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFieldList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idfield,
            this.fieldName});
            this.dgFieldList.Location = new System.Drawing.Point(17, 50);
            this.dgFieldList.Name = "dgFieldList";
            this.dgFieldList.ReadOnly = true;
            this.dgFieldList.Size = new System.Drawing.Size(357, 192);
            this.dgFieldList.TabIndex = 16;
            this.dgFieldList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFieldList_CellContentDoubleClick);
            this.dgFieldList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFieldList_CellContentDoubleClick);
            // 
            // idfield
            // 
            this.idfield.DataPropertyName = "MM_ID";
            this.idfield.HeaderText = "ID";
            this.idfield.Name = "idfield";
            this.idfield.ReadOnly = true;
            this.idfield.Visible = false;
            // 
            // fieldName
            // 
            this.fieldName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.fieldName.DataPropertyName = "MM_FieldName";
            this.fieldName.HeaderText = "Field Name";
            this.fieldName.Name = "fieldName";
            this.fieldName.ReadOnly = true;
            // 
            // frmMapFields
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 343);
            this.Controls.Add(this.dgFieldList);
            this.Controls.Add(this.txtFieldName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbOperationList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.bbClose);
            this.Name = "frmMapFields";
            this.Text = "Map Operation Fields";
            this.Load += new System.EventHandler(this.frmMapFields_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFieldList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbOperationList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFieldName;
        private System.Windows.Forms.DataGridView dgFieldList;
        private System.Windows.Forms.DataGridViewTextBoxColumn idfield;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldName;
    }
}