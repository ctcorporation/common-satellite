﻿
using Common_Satellite.Classes;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmConversions : Form
    {
        private Guid _pid;

        public frmConversions()
        {
            InitializeComponent();
        }
        private void FillGrid()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                // connect to data to generate sorted list
                var processes = uow.ConversionProcesses.GetAll().OrderBy(x => x.CP_Direction).ThenBy(y => y.CP_MethodName).ToList();
                //bind database to control - form
                BindingSource bs = new BindingSource();
                //Data source is the sortable binding list
                bs.DataSource = new SortableBindingList<ConversionProcess>(processes);
                grdConversionList.DataSource = bs.DataSource;

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            ConversionProcess convProcess;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                if (_pid == Guid.Empty)
                {
                    convProcess = new ConversionProcess();
                }
                else
                {
                    convProcess = uow.ConversionProcesses.Get(_pid);
                }


                convProcess.CP_Direction = rbFrom.Checked ? "F" : "T";
                convProcess.CP_MethodName = edMethodName.Text.Trim();
                convProcess.CP_MethodDescription = edMethodDesc.Text.Trim();

                if (_pid == Guid.Empty)
                    uow.ConversionProcesses.Add(convProcess);

                uow.Complete();
                FillGrid();
            }

        }

        private void frmConversions_Load(object sender, EventArgs e)
        {
            //setting grid
            grdConversionList.AutoGenerateColumns = false;
            FillGrid();
        }

        private void grdConversionList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (Guid.TryParse(grdConversionList[0, e.RowIndex].Value.ToString(), out _pid))
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
                {
                    var process = uow.ConversionProcesses.Get(_pid);

                    if (process != null)
                    {
                        rbFrom.Checked = process.CP_Direction == "F" ? true : false;
                        rbTo.Checked = process.CP_Direction == "T" ? true : false;
                        edMethodName.Text = process.CP_MethodName;
                        edMethodDesc.Text = process.CP_MethodDescription;
                    }
                }
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            _pid = Guid.Empty;
            rbFrom.Checked = true;
            rbTo.Checked = false;
            edMethodName.Text = string.Empty;
            edMethodDesc.Text = string.Empty;
        }

        private void grdConversionList_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {

                if (Guid.TryParse(grdConversionList[0, e.Row.Index].Value.ToString(), out _pid))
                {
                    var process = uow.ConversionProcesses.Get(_pid);
                    if (process != null)
                    {
                        var delConfirm = MessageBox.Show("Are you sure you want to delete - " + process.CP_MethodName, "Delete ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (delConfirm == DialogResult.Yes)
                        {
                            uow.ConversionProcesses.Remove(process);
                            uow.Complete();
                        }
                    }


                }
            }
        }
    }
}
