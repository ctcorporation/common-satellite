﻿namespace Common_Satellite.Views
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diagnosticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBaseTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fTPTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eAdapterTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchArchivesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purgeLogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mappingOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setMappingFieldsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codeMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conversionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToAPIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpacer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslCmbMode = new System.Windows.Forms.ToolStripDropDownButton();
            this.testingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fd = new System.Windows.Forms.OpenFileDialog();
            this.gbLogging = new System.Windows.Forms.GroupBox();
            this.rbFullLog = new System.Windows.Forms.RadioButton();
            this.rbMinimal = new System.Windows.Forms.RadioButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.ilMain = new System.Windows.Forms.ImageList(this.components);
            this.btnTimer = new System.Windows.Forms.Button();
            this.bbClose = new System.Windows.Forms.Button();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblProcessed = new System.Windows.Forms.Label();
            this.lblProcCount = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.cmsLog.SuspendLayout();
            this.gbLogging.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.systemToolStripMenuItem,
            this.dataToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(378, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.profilesToolStripMenuItem,
            this.diagnosticsToolStripMenuItem,
            this.searchArchivesToolStripMenuItem,
            this.purgeLogsToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.systemToolStripMenuItem.Text = "&System";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.settingsToolStripMenuItem.Text = "S&ettings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // profilesToolStripMenuItem
            // 
            this.profilesToolStripMenuItem.Name = "profilesToolStripMenuItem";
            this.profilesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.profilesToolStripMenuItem.Text = "Profiles";
            this.profilesToolStripMenuItem.Click += new System.EventHandler(this.profilesToolStripMenuItem_Click);
            // 
            // diagnosticsToolStripMenuItem
            // 
            this.diagnosticsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataBaseTestToolStripMenuItem,
            this.emailTestToolStripMenuItem,
            this.fTPTestToolStripMenuItem,
            this.eAdapterTestToolStripMenuItem});
            this.diagnosticsToolStripMenuItem.Name = "diagnosticsToolStripMenuItem";
            this.diagnosticsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.diagnosticsToolStripMenuItem.Text = "&Diagnostics";
            // 
            // dataBaseTestToolStripMenuItem
            // 
            this.dataBaseTestToolStripMenuItem.Name = "dataBaseTestToolStripMenuItem";
            this.dataBaseTestToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.dataBaseTestToolStripMenuItem.Text = "DataBase Test";
            // 
            // emailTestToolStripMenuItem
            // 
            this.emailTestToolStripMenuItem.Name = "emailTestToolStripMenuItem";
            this.emailTestToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.emailTestToolStripMenuItem.Text = "Email Test";
            // 
            // fTPTestToolStripMenuItem
            // 
            this.fTPTestToolStripMenuItem.Name = "fTPTestToolStripMenuItem";
            this.fTPTestToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.fTPTestToolStripMenuItem.Text = "FTP Test";
            // 
            // eAdapterTestToolStripMenuItem
            // 
            this.eAdapterTestToolStripMenuItem.Name = "eAdapterTestToolStripMenuItem";
            this.eAdapterTestToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.eAdapterTestToolStripMenuItem.Text = "eAdapter Test";
            // 
            // searchArchivesToolStripMenuItem
            // 
            this.searchArchivesToolStripMenuItem.Name = "searchArchivesToolStripMenuItem";
            this.searchArchivesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.searchArchivesToolStripMenuItem.Text = "Search Archives";
            this.searchArchivesToolStripMenuItem.Click += new System.EventHandler(this.searchArchivesToolStripMenuItem_Click);
            // 
            // purgeLogsToolStripMenuItem
            // 
            this.purgeLogsToolStripMenuItem.Name = "purgeLogsToolStripMenuItem";
            this.purgeLogsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.purgeLogsToolStripMenuItem.Text = "Purge Logs";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mappingToolStripMenuItem,
            this.conversionsToolStripMenuItem,
            this.importDataToolStripMenuItem,
            this.sendToAPIToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataToolStripMenuItem.Text = "&Data";
            // 
            // mappingToolStripMenuItem
            // 
            this.mappingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mappingOperationsToolStripMenuItem,
            this.setMappingFieldsToolStripMenuItem,
            this.codeMappingToolStripMenuItem});
            this.mappingToolStripMenuItem.Name = "mappingToolStripMenuItem";
            this.mappingToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.mappingToolStripMenuItem.Text = "Mapping ";
            // 
            // mappingOperationsToolStripMenuItem
            // 
            this.mappingOperationsToolStripMenuItem.Name = "mappingOperationsToolStripMenuItem";
            this.mappingOperationsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.mappingOperationsToolStripMenuItem.Text = "Mapping Operations";
            this.mappingOperationsToolStripMenuItem.Click += new System.EventHandler(this.mappingOperationsToolStripMenuItem_Click);
            // 
            // setMappingFieldsToolStripMenuItem
            // 
            this.setMappingFieldsToolStripMenuItem.Name = "setMappingFieldsToolStripMenuItem";
            this.setMappingFieldsToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.setMappingFieldsToolStripMenuItem.Text = "Set Mapping Fields";
            this.setMappingFieldsToolStripMenuItem.Click += new System.EventHandler(this.setMappingFieldsToolStripMenuItem_Click);
            // 
            // codeMappingToolStripMenuItem
            // 
            this.codeMappingToolStripMenuItem.Name = "codeMappingToolStripMenuItem";
            this.codeMappingToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.codeMappingToolStripMenuItem.Text = "Code Mapping";
            this.codeMappingToolStripMenuItem.Click += new System.EventHandler(this.codeMappingToolStripMenuItem_Click);
            // 
            // conversionsToolStripMenuItem
            // 
            this.conversionsToolStripMenuItem.Name = "conversionsToolStripMenuItem";
            this.conversionsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.conversionsToolStripMenuItem.Text = "Conversions";
            this.conversionsToolStripMenuItem.Click += new System.EventHandler(this.conversionsToolStripMenuItem_Click);
            // 
            // importDataToolStripMenuItem
            // 
            this.importDataToolStripMenuItem.Name = "importDataToolStripMenuItem";
            this.importDataToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.importDataToolStripMenuItem.Text = "Import Data";
            this.importDataToolStripMenuItem.Click += new System.EventHandler(this.importDataToolStripMenuItem_Click);
            // 
            // sendToAPIToolStripMenuItem
            // 
            this.sendToAPIToolStripMenuItem.Name = "sendToAPIToolStripMenuItem";
            this.sendToAPIToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.sendToAPIToolStripMenuItem.Text = "Send To API";
            this.sendToAPIToolStripMenuItem.Click += new System.EventHandler(this.sendToAPIToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslMain,
            this.tslSpacer,
            this.tslMode,
            this.tslCmbMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 150);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(378, 24);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslMain
            // 
            this.tslMain.Name = "tslMain";
            this.tslMain.Size = new System.Drawing.Size(39, 19);
            this.tslMain.Text = "Status";
            // 
            // tslSpacer
            // 
            this.tslSpacer.Name = "tslSpacer";
            this.tslSpacer.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.tslSpacer.Size = new System.Drawing.Size(50, 19);
            // 
            // tslMode
            // 
            this.tslMode.Name = "tslMode";
            this.tslMode.Size = new System.Drawing.Size(50, 19);
            this.tslMode.Text = "tslMode";
            // 
            // tslCmbMode
            // 
            this.tslCmbMode.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tslCmbMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslCmbMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testingToolStripMenuItem,
            this.productionToolStripMenuItem});
            this.tslCmbMode.Image = ((System.Drawing.Image)(resources.GetObject("tslCmbMode.Image")));
            this.tslCmbMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tslCmbMode.Margin = new System.Windows.Forms.Padding(0, 2, 10, 0);
            this.tslCmbMode.Name = "tslCmbMode";
            this.tslCmbMode.Size = new System.Drawing.Size(31, 22);
            this.tslCmbMode.Text = "toolStripDropDownButton1";
            // 
            // testingToolStripMenuItem
            // 
            this.testingToolStripMenuItem.CheckOnClick = true;
            this.testingToolStripMenuItem.Name = "testingToolStripMenuItem";
            this.testingToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.testingToolStripMenuItem.Text = "Testing";
            this.testingToolStripMenuItem.Click += new System.EventHandler(this.testingToolStripMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.CheckOnClick = true;
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.productionToolStripMenuItem.Text = "Production";
            this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
            // 
            // cmsLog
            // 
            this.cmsLog.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cmsLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearLogToolStripMenuItem,
            this.exportLogToolStripMenuItem});
            this.cmsLog.Name = "cmsLog";
            this.cmsLog.Size = new System.Drawing.Size(132, 48);
            // 
            // clearLogToolStripMenuItem
            // 
            this.clearLogToolStripMenuItem.Name = "clearLogToolStripMenuItem";
            this.clearLogToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            // 
            // exportLogToolStripMenuItem
            // 
            this.exportLogToolStripMenuItem.Name = "exportLogToolStripMenuItem";
            this.exportLogToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.exportLogToolStripMenuItem.Text = "Export Log";
            // 
            // fd
            // 
            this.fd.FileName = "fd";
            // 
            // gbLogging
            // 
            this.gbLogging.Controls.Add(this.rbFullLog);
            this.gbLogging.Controls.Add(this.rbMinimal);
            this.gbLogging.Controls.Add(this.rbNone);
            this.gbLogging.Location = new System.Drawing.Point(12, 27);
            this.gbLogging.Name = "gbLogging";
            this.gbLogging.Size = new System.Drawing.Size(246, 53);
            this.gbLogging.TabIndex = 10;
            this.gbLogging.TabStop = false;
            this.gbLogging.Text = "Logging Level";
            // 
            // rbFullLog
            // 
            this.rbFullLog.AutoSize = true;
            this.rbFullLog.Location = new System.Drawing.Point(194, 24);
            this.rbFullLog.Name = "rbFullLog";
            this.rbFullLog.Size = new System.Drawing.Size(41, 17);
            this.rbFullLog.TabIndex = 2;
            this.rbFullLog.Tag = "2";
            this.rbFullLog.Text = "Full";
            this.rbFullLog.UseVisualStyleBackColor = true;
            // 
            // rbMinimal
            // 
            this.rbMinimal.AutoSize = true;
            this.rbMinimal.Checked = true;
            this.rbMinimal.Location = new System.Drawing.Point(70, 24);
            this.rbMinimal.Name = "rbMinimal";
            this.rbMinimal.Size = new System.Drawing.Size(118, 17);
            this.rbMinimal.TabIndex = 1;
            this.rbMinimal.TabStop = true;
            this.rbMinimal.Tag = "1";
            this.rbMinimal.Text = "Minimal - Errors only";
            this.rbMinimal.UseVisualStyleBackColor = true;
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(13, 24);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 0;
            this.rbNone.Tag = "0";
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // ilMain
            // 
            this.ilMain.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMain.ImageStream")));
            this.ilMain.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMain.Images.SetKeyName(0, "Error");
            this.ilMain.Images.SetKeyName(1, "Information");
            this.ilMain.Images.SetKeyName(2, "Warning");
            // 
            // btnTimer
            // 
            this.btnTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTimer.BackColor = System.Drawing.Color.LightGreen;
            this.btnTimer.Image = ((System.Drawing.Image)(resources.GetObject("btnTimer.Image")));
            this.btnTimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTimer.Location = new System.Drawing.Point(209, 116);
            this.btnTimer.Name = "btnTimer";
            this.btnTimer.Size = new System.Drawing.Size(75, 23);
            this.btnTimer.TabIndex = 6;
            this.btnTimer.Text = "&Start";
            this.btnTimer.UseVisualStyleBackColor = false;
            this.btnTimer.Click += new System.EventHandler(this.btnTimer_Click);
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Image = ((System.Drawing.Image)(resources.GetObject("bbClose.Image")));
            this.bbClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bbClose.Location = new System.Drawing.Point(291, 116);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 5;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(25, 128);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(41, 13);
            this.lblVersion.TabIndex = 11;
            this.lblVersion.Text = "version";
            // 
            // lblProcessed
            // 
            this.lblProcessed.AutoSize = true;
            this.lblProcessed.Location = new System.Drawing.Point(25, 110);
            this.lblProcessed.Name = "lblProcessed";
            this.lblProcessed.Size = new System.Drawing.Size(66, 13);
            this.lblProcessed.TabIndex = 12;
            this.lblProcessed.Text = "Processed : ";
            // 
            // lblProcCount
            // 
            this.lblProcCount.AutoSize = true;
            this.lblProcCount.Location = new System.Drawing.Point(96, 109);
            this.lblProcCount.Name = "lblProcCount";
            this.lblProcCount.Size = new System.Drawing.Size(13, 13);
            this.lblProcCount.TabIndex = 13;
            this.lblProcCount.Text = "0";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 174);
            this.Controls.Add(this.lblProcCount);
            this.Controls.Add(this.lblProcessed);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.gbLogging);
            this.Controls.Add(this.btnTimer);
            this.Controls.Add(this.bbClose);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Common Conversion Satellite";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.cmsLog.ResumeLayout(false);
            this.gbLogging.ResumeLayout(false);
            this.gbLogging.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diagnosticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataBaseTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fTPTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eAdapterTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslMain;
        private System.Windows.Forms.ContextMenuStrip cmsLog;
        private System.Windows.Forms.ToolStripMenuItem clearLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profilesToolStripMenuItem;
        private System.Windows.Forms.Button btnTimer;
        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.ToolStripDropDownButton tslCmbMode;
        private System.Windows.Forms.ToolStripMenuItem testingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tslSpacer;
        private System.Windows.Forms.ToolStripStatusLabel tslMode;
        private System.Windows.Forms.OpenFileDialog fd;
        private System.Windows.Forms.ToolStripMenuItem searchArchivesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mappingOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setMappingFieldsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codeMappingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDataToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbLogging;
        private System.Windows.Forms.RadioButton rbFullLog;
        private System.Windows.Forms.RadioButton rbMinimal;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.ToolStripMenuItem purgeLogsToolStripMenuItem;
        private System.Windows.Forms.ImageList ilMain;
        private System.Windows.Forms.ToolStripMenuItem sendToAPIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conversionsToolStripMenuItem;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblProcessed;
        private System.Windows.Forms.Label lblProcCount;
    }
}

