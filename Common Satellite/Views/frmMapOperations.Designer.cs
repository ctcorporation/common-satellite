﻿namespace Common_Satellite.Views
{
    partial class frmMapOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMapOperations));
            this.bbClose = new System.Windows.Forms.Button();
            this.grdMappingOperations = new System.Windows.Forms.DataGridView();
            this.MapOperation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MapDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MapID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMapOperation = new System.Windows.Forms.TextBox();
            this.txtOperationDescription = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsMapOperation = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grdMappingOperations)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Image = ((System.Drawing.Image)(resources.GetObject("bbClose.Image")));
            this.bbClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bbClose.Location = new System.Drawing.Point(396, 254);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(58, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // grdMappingOperations
            // 
            this.grdMappingOperations.AllowUserToAddRows = false;
            this.grdMappingOperations.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMappingOperations.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MapOperation,
            this.MapDescription,
            this.MapID});
            this.grdMappingOperations.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.grdMappingOperations.Location = new System.Drawing.Point(12, 12);
            this.grdMappingOperations.MultiSelect = false;
            this.grdMappingOperations.Name = "grdMappingOperations";
            this.grdMappingOperations.ReadOnly = true;
            this.grdMappingOperations.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMappingOperations.Size = new System.Drawing.Size(442, 163);
            this.grdMappingOperations.TabIndex = 1;
            this.grdMappingOperations.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMappingOperations_CellContentDoubleClick);
            this.grdMappingOperations.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMappingOperations_CellContentDoubleClick);
            this.grdMappingOperations.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.grdMappingOperations_UserDeletingRow);
            // 
            // MapOperation
            // 
            this.MapOperation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MapOperation.DataPropertyName = "MO_OperationName";
            this.MapOperation.HeaderText = "Map Operation";
            this.MapOperation.Name = "MapOperation";
            // 
            // MapDescription
            // 
            this.MapDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MapDescription.DataPropertyName = "MO_Description";
            this.MapDescription.HeaderText = "Description";
            this.MapDescription.Name = "MapDescription";
            // 
            // MapID
            // 
            this.MapID.DataPropertyName = "MO_ID";
            this.MapID.HeaderText = "MO_ID";
            this.MapID.Name = "MapID";
            this.MapID.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 183);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name of Map Operation";
            // 
            // txtMapOperation
            // 
            this.txtMapOperation.Location = new System.Drawing.Point(12, 199);
            this.txtMapOperation.Name = "txtMapOperation";
            this.txtMapOperation.Size = new System.Drawing.Size(121, 20);
            this.txtMapOperation.TabIndex = 4;
            // 
            // txtOperationDescription
            // 
            this.txtOperationDescription.Location = new System.Drawing.Point(149, 199);
            this.txtOperationDescription.Name = "txtOperationDescription";
            this.txtOperationDescription.Size = new System.Drawing.Size(305, 20);
            this.txtOperationDescription.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Description of Operation";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(396, 225);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(58, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMapOperation});
            this.statusStrip1.Location = new System.Drawing.Point(0, 289);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(465, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsMapOperation
            // 
            this.tsMapOperation.Name = "tsMapOperation";
            this.tsMapOperation.Size = new System.Drawing.Size(118, 17);
            this.tsMapOperation.Text = "toolStripStatusLabel1";
            // 
            // frmMapOperations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 311);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtOperationDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMapOperation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.grdMappingOperations);
            this.Controls.Add(this.bbClose);
            this.Name = "frmMapOperations";
            this.Text = "Mapping Operations";
            this.Load += new System.EventHandler(this.frmMapOperations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdMappingOperations)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.DataGridView grdMappingOperations;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMapOperation;
        private System.Windows.Forms.TextBox txtOperationDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsMapOperation;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapOperation;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapID;
    }
}