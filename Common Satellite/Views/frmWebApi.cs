﻿using RestSharp;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmWebApi : Form
    {
        public frmWebApi()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtFile_DoubleClick(object sender, EventArgs e)
        {
            using (OpenFileDialog fd = new OpenFileDialog())
            {
                if (fd.ShowDialog() == DialogResult.OK)
                {
                    txtFile.Text = fd.FileName;
                }

            }

        }

        private async void button2_Click(object sender, EventArgs e)
        {
            RestResponse restResponse = await SendFile(txtFile.Text, txtApi.Text);
            if (restResponse.get_StatusCode() == System.Net.HttpStatusCode.OK)
                MessageBox.Show("You have successfully uploaded the file", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private async Task<RestResponse> SendFile(string filename, string url)
        {
            using (var fStream = File.Open(txtFile.Text, FileMode.Open))
            {
                var client = new RestClient(txtApi.Text);
                var req = new RestRequest(Method.Post.ToString());
                using (MemoryStream ms = new MemoryStream())
                {
                    await fStream.CopyToAsync(ms);
                    req.AddFile("file", ms.ToArray(), txtFile.Text);
                    req.AlwaysMultipartFormData = true;
                    var response = await client.ExecuteAsync(req);
                    return response;
                }


            }
        }
    }
}
    
   


