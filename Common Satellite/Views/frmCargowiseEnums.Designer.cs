﻿namespace Common_Satellite.Views
{
    partial class frmCargowiseEnums
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCargowiseEnums));
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edMapType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edCargowiseValue = new System.Windows.Forms.TextBox();
            this.edMapVaule = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bbAdd = new System.Windows.Forms.Button();
            this.grdEnums = new System.Windows.Forms.DataGridView();
            this.MapTypes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MapField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustMapValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdEnums)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(486, 336);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mapping Type";
            // 
            // edMapType
            // 
            this.edMapType.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edMapType.Location = new System.Drawing.Point(114, 26);
            this.edMapType.Name = "edMapType";
            this.edMapType.Size = new System.Drawing.Size(174, 20);
            this.edMapType.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cargowise Value";
            // 
            // edCargowiseValue
            // 
            this.edCargowiseValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCargowiseValue.Location = new System.Drawing.Point(114, 52);
            this.edCargowiseValue.Name = "edCargowiseValue";
            this.edCargowiseValue.Size = new System.Drawing.Size(174, 20);
            this.edCargowiseValue.TabIndex = 4;
            // 
            // edMapVaule
            // 
            this.edMapVaule.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edMapVaule.Location = new System.Drawing.Point(114, 78);
            this.edMapVaule.Name = "edMapVaule";
            this.edMapVaule.Size = new System.Drawing.Size(174, 20);
            this.edMapVaule.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mapped Value";
            // 
            // bbAdd
            // 
            this.bbAdd.Image = ((System.Drawing.Image)(resources.GetObject("bbAdd.Image")));
            this.bbAdd.Location = new System.Drawing.Point(315, 50);
            this.bbAdd.Name = "bbAdd";
            this.bbAdd.Size = new System.Drawing.Size(45, 48);
            this.bbAdd.TabIndex = 7;
            this.bbAdd.UseVisualStyleBackColor = true;
            this.bbAdd.Click += new System.EventHandler(this.bbAdd_Click);
            // 
            // grdEnums
            // 
            this.grdEnums.AllowUserToAddRows = false;
            this.grdEnums.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEnums.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MapTypes,
            this.MapField,
            this.CustMapValue,
            this.ID});
            this.grdEnums.Location = new System.Drawing.Point(16, 114);
            this.grdEnums.MultiSelect = false;
            this.grdEnums.Name = "grdEnums";
            this.grdEnums.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdEnums.Size = new System.Drawing.Size(545, 211);
            this.grdEnums.TabIndex = 8;
            this.grdEnums.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdEnums_CellMouseDoubleClick);
            // 
            // MapTypes
            // 
            this.MapTypes.DataPropertyName = "CW_ENUMTYPE";
            this.MapTypes.HeaderText = "Mapping Type";
            this.MapTypes.Name = "MapTypes";
            this.MapTypes.Width = 200;
            // 
            // MapField
            // 
            this.MapField.DataPropertyName = "CW_ENUM";
            this.MapField.HeaderText = "Map Field";
            this.MapField.Name = "MapField";
            this.MapField.Width = 150;
            // 
            // CustMapValue
            // 
            this.CustMapValue.DataPropertyName = "CW_MAPVALUE";
            this.CustMapValue.HeaderText = "Customer Map Value";
            this.CustMapValue.Name = "CustMapValue";
            this.CustMapValue.Width = 150;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "CW_ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // frmCargowiseEnums
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 371);
            this.Controls.Add(this.grdEnums);
            this.Controls.Add(this.bbAdd);
            this.Controls.Add(this.edMapVaule);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edCargowiseValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edMapType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "frmCargowiseEnums";
            this.Text = "Cargowise Enums/ Mappings";
            this.Load += new System.EventHandler(this.frmCargowiseEnums_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdEnums)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edMapType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edCargowiseValue;
        private System.Windows.Forms.TextBox edMapVaule;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bbAdd;
        private System.Windows.Forms.DataGridView grdEnums;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapTypes;
        private System.Windows.Forms.DataGridViewTextBoxColumn MapField;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustMapValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
    }
}