﻿namespace Common_Satellite.Views
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.tcSettings = new System.Windows.Forms.TabControl();
            this.tabSystem = new System.Windows.Forms.TabPage();
            this.btnDirXml = new System.Windows.Forms.Button();
            this.txtCNodeLocation = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLibraryName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRefresh = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.txtAlertsTo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPollTime = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tabCTCNode = new System.Windows.Forms.TabPage();
            this.txtConnectionResults = new System.Windows.Forms.RichTextBox();
            this.btnCTCTest = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDatabaseName = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDatabaseServer = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tabComms = new System.Windows.Forms.TabPage();
            this.cbSMTPSecure = new System.Windows.Forms.CheckBox();
            this.txtSmtpEmailAddress = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSmtpPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSmtpUserName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSmtpPort = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSmtpServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabFilelocations = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bbTestOutputLocation = new System.Windows.Forms.Button();
            this.edTestOutputLocation = new System.Windows.Forms.TextBox();
            this.bbTestTempLocation = new System.Windows.Forms.Button();
            this.edTestTempLocation = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bbProdOutputLocation = new System.Windows.Forms.Button();
            this.edProdOutputLocation = new System.Windows.Forms.TextBox();
            this.bbProdTempLocation = new System.Windows.Forms.Button();
            this.edProdTempLocation = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.edWinSCPLocation = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tcSettings.SuspendLayout();
            this.tabSystem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPollTime)).BeginInit();
            this.tabCTCNode.SuspendLayout();
            this.tabComms.SuspendLayout();
            this.tabFilelocations.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcSettings
            // 
            this.tcSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcSettings.Controls.Add(this.tabSystem);
            this.tcSettings.Controls.Add(this.tabCTCNode);
            this.tcSettings.Controls.Add(this.tabComms);
            this.tcSettings.Controls.Add(this.tabFilelocations);
            this.tcSettings.Location = new System.Drawing.Point(12, 12);
            this.tcSettings.Name = "tcSettings";
            this.tcSettings.SelectedIndex = 0;
            this.tcSettings.Size = new System.Drawing.Size(574, 273);
            this.tcSettings.TabIndex = 0;
            // 
            // tabSystem
            // 
            this.tabSystem.Controls.Add(this.button1);
            this.tabSystem.Controls.Add(this.edWinSCPLocation);
            this.tabSystem.Controls.Add(this.label16);
            this.tabSystem.Controls.Add(this.btnDirXml);
            this.tabSystem.Controls.Add(this.txtCNodeLocation);
            this.tabSystem.Controls.Add(this.label10);
            this.tabSystem.Controls.Add(this.label4);
            this.tabSystem.Controls.Add(this.txtLibraryName);
            this.tabSystem.Controls.Add(this.label1);
            this.tabSystem.Controls.Add(this.txtRefresh);
            this.tabSystem.Controls.Add(this.label24);
            this.tabSystem.Controls.Add(this.txtAlertsTo);
            this.tabSystem.Controls.Add(this.label14);
            this.tabSystem.Controls.Add(this.label2);
            this.tabSystem.Controls.Add(this.txtPollTime);
            this.tabSystem.Controls.Add(this.label3);
            this.tabSystem.Location = new System.Drawing.Point(4, 22);
            this.tabSystem.Name = "tabSystem";
            this.tabSystem.Padding = new System.Windows.Forms.Padding(3);
            this.tabSystem.Size = new System.Drawing.Size(566, 247);
            this.tabSystem.TabIndex = 3;
            this.tabSystem.Text = "System Settings";
            this.tabSystem.UseVisualStyleBackColor = true;
            // 
            // btnDirXml
            // 
            this.btnDirXml.Location = new System.Drawing.Point(471, 98);
            this.btnDirXml.Margin = new System.Windows.Forms.Padding(2);
            this.btnDirXml.Name = "btnDirXml";
            this.btnDirXml.Size = new System.Drawing.Size(25, 20);
            this.btnDirXml.TabIndex = 30;
            this.btnDirXml.Text = "...";
            this.btnDirXml.UseVisualStyleBackColor = true;
            this.btnDirXml.Click += new System.EventHandler(this.btnDirXml_Click);
            // 
            // txtCNodeLocation
            // 
            this.txtCNodeLocation.Location = new System.Drawing.Point(141, 98);
            this.txtCNodeLocation.Margin = new System.Windows.Forms.Padding(2);
            this.txtCNodeLocation.Name = "txtCNodeLocation";
            this.txtCNodeLocation.Size = new System.Drawing.Size(329, 20);
            this.txtCNodeLocation.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 100);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "CTC Adapter Output files";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Satellite Library";
            // 
            // txtLibraryName
            // 
            this.txtLibraryName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLibraryName.Location = new System.Drawing.Point(133, 71);
            this.txtLibraryName.Name = "txtLibraryName";
            this.txtLibraryName.Size = new System.Drawing.Size(133, 20);
            this.txtLibraryName.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(468, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Minutes";
            // 
            // txtRefresh
            // 
            this.txtRefresh.Location = new System.Drawing.Point(416, 17);
            this.txtRefresh.Name = "txtRefresh";
            this.txtRefresh.Size = new System.Drawing.Size(45, 20);
            this.txtRefresh.TabIndex = 24;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(266, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(144, 13);
            this.label24.TabIndex = 23;
            this.label24.Text = "Default Screen Refresh Time";
            // 
            // txtAlertsTo
            // 
            this.txtAlertsTo.Location = new System.Drawing.Point(133, 44);
            this.txtAlertsTo.Name = "txtAlertsTo";
            this.txtAlertsTo.Size = new System.Drawing.Size(362, 20);
            this.txtAlertsTo.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 46);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(93, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Global Alerts email";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(184, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Minutes";
            // 
            // txtPollTime
            // 
            this.txtPollTime.Location = new System.Drawing.Point(133, 17);
            this.txtPollTime.Name = "txtPollTime";
            this.txtPollTime.Size = new System.Drawing.Size(45, 20);
            this.txtPollTime.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Default Polling Time";
            // 
            // tabCTCNode
            // 
            this.tabCTCNode.Controls.Add(this.txtConnectionResults);
            this.tabCTCNode.Controls.Add(this.btnCTCTest);
            this.tabCTCNode.Controls.Add(this.txtPassword);
            this.tabCTCNode.Controls.Add(this.label20);
            this.tabCTCNode.Controls.Add(this.txtUserName);
            this.tabCTCNode.Controls.Add(this.label21);
            this.tabCTCNode.Controls.Add(this.txtDatabaseName);
            this.tabCTCNode.Controls.Add(this.label22);
            this.tabCTCNode.Controls.Add(this.txtDatabaseServer);
            this.tabCTCNode.Controls.Add(this.label23);
            this.tabCTCNode.Location = new System.Drawing.Point(4, 22);
            this.tabCTCNode.Name = "tabCTCNode";
            this.tabCTCNode.Padding = new System.Windows.Forms.Padding(3);
            this.tabCTCNode.Size = new System.Drawing.Size(566, 247);
            this.tabCTCNode.TabIndex = 4;
            this.tabCTCNode.Text = "Database Settings (CTC Node)";
            this.tabCTCNode.UseVisualStyleBackColor = true;
            // 
            // txtConnectionResults
            // 
            this.txtConnectionResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConnectionResults.Location = new System.Drawing.Point(16, 163);
            this.txtConnectionResults.Name = "txtConnectionResults";
            this.txtConnectionResults.Size = new System.Drawing.Size(534, 88);
            this.txtConnectionResults.TabIndex = 23;
            this.txtConnectionResults.Text = "";
            // 
            // btnCTCTest
            // 
            this.btnCTCTest.Location = new System.Drawing.Point(16, 123);
            this.btnCTCTest.Name = "btnCTCTest";
            this.btnCTCTest.Size = new System.Drawing.Size(105, 23);
            this.btnCTCTest.TabIndex = 22;
            this.btnCTCTest.Text = "Test Connection";
            this.btnCTCTest.UseVisualStyleBackColor = true;
            this.btnCTCTest.Click += new System.EventHandler(this.btnCTCTest_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(135, 97);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(266, 20);
            this.txtPassword.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(23, 100);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Password";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(135, 71);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(266, 20);
            this.txtUserName.TabIndex = 19;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(23, 74);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "User Name";
            // 
            // txtDatabaseName
            // 
            this.txtDatabaseName.Location = new System.Drawing.Point(135, 43);
            this.txtDatabaseName.Name = "txtDatabaseName";
            this.txtDatabaseName.Size = new System.Drawing.Size(266, 20);
            this.txtDatabaseName.TabIndex = 17;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(23, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 13);
            this.label22.TabIndex = 16;
            this.label22.Text = "Database";
            // 
            // txtDatabaseServer
            // 
            this.txtDatabaseServer.Location = new System.Drawing.Point(135, 17);
            this.txtDatabaseServer.Name = "txtDatabaseServer";
            this.txtDatabaseServer.Size = new System.Drawing.Size(266, 20);
            this.txtDatabaseServer.TabIndex = 15;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(23, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(106, 13);
            this.label23.TabIndex = 14;
            this.label23.Text = "CTC Node Database";
            // 
            // tabComms
            // 
            this.tabComms.Controls.Add(this.cbSMTPSecure);
            this.tabComms.Controls.Add(this.txtSmtpEmailAddress);
            this.tabComms.Controls.Add(this.label9);
            this.tabComms.Controls.Add(this.txtSmtpPassword);
            this.tabComms.Controls.Add(this.label8);
            this.tabComms.Controls.Add(this.txtSmtpUserName);
            this.tabComms.Controls.Add(this.label7);
            this.tabComms.Controls.Add(this.txtSmtpPort);
            this.tabComms.Controls.Add(this.label6);
            this.tabComms.Controls.Add(this.txtSmtpServer);
            this.tabComms.Controls.Add(this.label5);
            this.tabComms.Location = new System.Drawing.Point(4, 22);
            this.tabComms.Name = "tabComms";
            this.tabComms.Padding = new System.Windows.Forms.Padding(3);
            this.tabComms.Size = new System.Drawing.Size(566, 247);
            this.tabComms.TabIndex = 1;
            this.tabComms.Text = "Communications ";
            this.tabComms.UseVisualStyleBackColor = true;
            // 
            // cbSMTPSecure
            // 
            this.cbSMTPSecure.AutoSize = true;
            this.cbSMTPSecure.Location = new System.Drawing.Point(399, 57);
            this.cbSMTPSecure.Name = "cbSMTPSecure";
            this.cbSMTPSecure.Size = new System.Drawing.Size(93, 17);
            this.cbSMTPSecure.TabIndex = 14;
            this.cbSMTPSecure.Text = "Secure SMTP";
            this.cbSMTPSecure.UseVisualStyleBackColor = true;
            // 
            // txtSmtpEmailAddress
            // 
            this.txtSmtpEmailAddress.Location = new System.Drawing.Point(109, 107);
            this.txtSmtpEmailAddress.Name = "txtSmtpEmailAddress";
            this.txtSmtpEmailAddress.Size = new System.Drawing.Size(266, 20);
            this.txtSmtpEmailAddress.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Email Address";
            // 
            // txtSmtpPassword
            // 
            this.txtSmtpPassword.Location = new System.Drawing.Point(109, 81);
            this.txtSmtpPassword.Name = "txtSmtpPassword";
            this.txtSmtpPassword.Size = new System.Drawing.Size(266, 20);
            this.txtSmtpPassword.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Password";
            // 
            // txtSmtpUserName
            // 
            this.txtSmtpUserName.Location = new System.Drawing.Point(109, 55);
            this.txtSmtpUserName.Name = "txtSmtpUserName";
            this.txtSmtpUserName.Size = new System.Drawing.Size(266, 20);
            this.txtSmtpUserName.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Username";
            // 
            // txtSmtpPort
            // 
            this.txtSmtpPort.Location = new System.Drawing.Point(431, 23);
            this.txtSmtpPort.Name = "txtSmtpPort";
            this.txtSmtpPort.Size = new System.Drawing.Size(38, 20);
            this.txtSmtpPort.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(396, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Port:";
            // 
            // txtSmtpServer
            // 
            this.txtSmtpServer.Location = new System.Drawing.Point(109, 23);
            this.txtSmtpServer.Name = "txtSmtpServer";
            this.txtSmtpServer.Size = new System.Drawing.Size(266, 20);
            this.txtSmtpServer.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mail Server:";
            // 
            // tabFilelocations
            // 
            this.tabFilelocations.Controls.Add(this.groupBox2);
            this.tabFilelocations.Controls.Add(this.groupBox1);
            this.tabFilelocations.Location = new System.Drawing.Point(4, 22);
            this.tabFilelocations.Name = "tabFilelocations";
            this.tabFilelocations.Padding = new System.Windows.Forms.Padding(3);
            this.tabFilelocations.Size = new System.Drawing.Size(566, 247);
            this.tabFilelocations.TabIndex = 2;
            this.tabFilelocations.Text = "File Locations";
            this.tabFilelocations.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bbTestOutputLocation);
            this.groupBox2.Controls.Add(this.edTestOutputLocation);
            this.groupBox2.Controls.Add(this.bbTestTempLocation);
            this.groupBox2.Controls.Add(this.edTestTempLocation);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(15, 132);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(534, 96);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Test";
            // 
            // bbTestOutputLocation
            // 
            this.bbTestOutputLocation.Location = new System.Drawing.Point(473, 47);
            this.bbTestOutputLocation.Name = "bbTestOutputLocation";
            this.bbTestOutputLocation.Size = new System.Drawing.Size(27, 23);
            this.bbTestOutputLocation.TabIndex = 11;
            this.bbTestOutputLocation.Text = "...";
            this.bbTestOutputLocation.UseVisualStyleBackColor = true;
            this.bbTestOutputLocation.Click += new System.EventHandler(this.bbTestOutputLocation_Click);
            // 
            // edTestOutputLocation
            // 
            this.edTestOutputLocation.Location = new System.Drawing.Point(112, 49);
            this.edTestOutputLocation.Name = "edTestOutputLocation";
            this.edTestOutputLocation.Size = new System.Drawing.Size(362, 20);
            this.edTestOutputLocation.TabIndex = 10;
            // 
            // bbTestTempLocation
            // 
            this.bbTestTempLocation.Location = new System.Drawing.Point(473, 17);
            this.bbTestTempLocation.Name = "bbTestTempLocation";
            this.bbTestTempLocation.Size = new System.Drawing.Size(27, 23);
            this.bbTestTempLocation.TabIndex = 9;
            this.bbTestTempLocation.Text = "...";
            this.bbTestTempLocation.UseVisualStyleBackColor = true;
            this.bbTestTempLocation.Click += new System.EventHandler(this.bbTestTempLocation_Click);
            // 
            // edTestTempLocation
            // 
            this.edTestTempLocation.Location = new System.Drawing.Point(112, 19);
            this.edTestTempLocation.Name = "edTestTempLocation";
            this.edTestTempLocation.Size = new System.Drawing.Size(362, 20);
            this.edTestTempLocation.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Output Location";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Temp Location";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bbProdOutputLocation);
            this.groupBox1.Controls.Add(this.edProdOutputLocation);
            this.groupBox1.Controls.Add(this.bbProdTempLocation);
            this.groupBox1.Controls.Add(this.edProdTempLocation);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(15, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(534, 98);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Production";
            // 
            // bbProdOutputLocation
            // 
            this.bbProdOutputLocation.Location = new System.Drawing.Point(473, 55);
            this.bbProdOutputLocation.Name = "bbProdOutputLocation";
            this.bbProdOutputLocation.Size = new System.Drawing.Size(27, 23);
            this.bbProdOutputLocation.TabIndex = 5;
            this.bbProdOutputLocation.Text = "...";
            this.bbProdOutputLocation.UseVisualStyleBackColor = true;
            this.bbProdOutputLocation.Click += new System.EventHandler(this.bbProdOutputLocation_Click);
            // 
            // edProdOutputLocation
            // 
            this.edProdOutputLocation.Location = new System.Drawing.Point(112, 57);
            this.edProdOutputLocation.Name = "edProdOutputLocation";
            this.edProdOutputLocation.Size = new System.Drawing.Size(362, 20);
            this.edProdOutputLocation.TabIndex = 4;
            // 
            // bbProdTempLocation
            // 
            this.bbProdTempLocation.Location = new System.Drawing.Point(473, 25);
            this.bbProdTempLocation.Name = "bbProdTempLocation";
            this.bbProdTempLocation.Size = new System.Drawing.Size(27, 23);
            this.bbProdTempLocation.TabIndex = 3;
            this.bbProdTempLocation.Text = "...";
            this.bbProdTempLocation.UseVisualStyleBackColor = true;
            this.bbProdTempLocation.Click += new System.EventHandler(this.bbProdTempLocation_Click);
            // 
            // edProdTempLocation
            // 
            this.edProdTempLocation.Location = new System.Drawing.Point(112, 27);
            this.edProdTempLocation.Name = "edProdTempLocation";
            this.edProdTempLocation.Size = new System.Drawing.Size(362, 20);
            this.edProdTempLocation.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Output Location";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Temp Location";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(511, 291);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(430, 291);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(471, 128);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 20);
            this.button1.TabIndex = 33;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // edWinSCPLocation
            // 
            this.edWinSCPLocation.Location = new System.Drawing.Point(141, 128);
            this.edWinSCPLocation.Margin = new System.Windows.Forms.Padding(2);
            this.edWinSCPLocation.Name = "edWinSCPLocation";
            this.edWinSCPLocation.Size = new System.Drawing.Size(329, 20);
            this.edWinSCPLocation.TabIndex = 32;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 130);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "WinSCP.exe Location";
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 326);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.tcSettings);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.tcSettings.ResumeLayout(false);
            this.tabSystem.ResumeLayout(false);
            this.tabSystem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPollTime)).EndInit();
            this.tabCTCNode.ResumeLayout(false);
            this.tabCTCNode.PerformLayout();
            this.tabComms.ResumeLayout(false);
            this.tabComms.PerformLayout();
            this.tabFilelocations.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcSettings;
        private System.Windows.Forms.TabPage tabComms;
        private System.Windows.Forms.TextBox txtSmtpEmailAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSmtpPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSmtpUserName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSmtpPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSmtpServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabFilelocations;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabPage tabSystem;
        private System.Windows.Forms.TabPage tabCTCNode;
        private System.Windows.Forms.RichTextBox txtConnectionResults;
        private System.Windows.Forms.Button btnCTCTest;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDatabaseName;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDatabaseServer;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtRefresh;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtAlertsTo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txtPollTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbSMTPSecure;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLibraryName;
        private System.Windows.Forms.Button btnDirXml;
        private System.Windows.Forms.TextBox txtCNodeLocation;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bbTestOutputLocation;
        private System.Windows.Forms.TextBox edTestOutputLocation;
        private System.Windows.Forms.Button bbTestTempLocation;
        private System.Windows.Forms.TextBox edTestTempLocation;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bbProdOutputLocation;
        private System.Windows.Forms.TextBox edProdOutputLocation;
        private System.Windows.Forms.Button bbProdTempLocation;
        private System.Windows.Forms.TextBox edProdTempLocation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edWinSCPLocation;
        private System.Windows.Forms.Label label16;
    }
}
