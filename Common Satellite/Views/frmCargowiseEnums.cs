﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmCargowiseEnums : Form
    {
        public NodeDataContext Context { get; set; }

        public frmCargowiseEnums(string connString)
        {
            InitializeComponent();
            Context = new NodeDataContext(connString);

        }
        public frmCargowiseEnums()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bbAdd_Click(object sender, EventArgs e)
        {

            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                Cargowise_Enums cwEnums = new Cargowise_Enums
                {
                    CW_ENUMTYPE = edMapType.Text,
                    CW_ENUM = edCargowiseValue.Text,
                    CW_MAPVALUE = edMapVaule.Text
                };
                var enumVal = uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == edMapType.Text && x.CW_MAPVALUE == edMapVaule.Text).FirstOrDefault();
                if (enumVal != null)
                {
                    enumVal.CW_ENUMTYPE = cwEnums.CW_ENUMTYPE;
                    enumVal.CW_ENUM = cwEnums.CW_ENUM;
                    enumVal.CW_MAPVALUE = cwEnums.CW_MAPVALUE;
                }
                else
                {
                    uow.CargowiseEnums.Add(cwEnums);
                }
                if (uow.Complete() > 0)
                {
                    DialogResult dr = MessageBox.Show("Mapping Value Added. Would you like to add another?", "Mapping values maintenance", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dr == DialogResult.Yes)
                    {
                        edCargowiseValue.Text = string.Empty;
                        edMapVaule.Text = string.Empty;
                        edCargowiseValue.Focus();
                    }
                }
                FillGrid();
            }
        }

        private void frmCargowiseEnums_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void FillGrid()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                var mapList = uow.CargowiseEnums.GetAll().OrderBy(x => x.CW_ENUMTYPE).ToList();
                if (mapList != null)
                {
                    grdEnums.DataSource = mapList;

                }
            }
        }

        private void grdEnums_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                edMapType.Text = (string)grdEnums["MapTypes", e.RowIndex].Value;
                edCargowiseValue.Text = (string)grdEnums["MapField", e.RowIndex].Value;
                edMapVaule.Text = (string)grdEnums["CustMapValue", e.RowIndex].Value;
            }
        }
    }
}
