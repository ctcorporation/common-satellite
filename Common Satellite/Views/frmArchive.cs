﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Windows.Forms;

namespace Common_Satellite.Views
{
    public partial class frmArchive : Form
    {
        public frmArchive()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void frmArchive_Load(object sender, EventArgs e)
        {
            FillCustomerList();

        }

        private void FillCustomerList()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                var customers = uow.Customers.GetAll().OrderBy(x => x.C_NAME).ToList();
                customers.Insert(0, new Customer { C_ID = Guid.Empty, C_NAME = "(none selected)" });
                cmbCustomer.DataSource = customers;
                cmbCustomer.DisplayMember = "C_NAME";
                cmbCustomer.ValueMember = "C_ID";
            }


        }

        private void cmbCustomer_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex != 0)
            {
                FillProfileList();
            }
        }

        private void FillProfileList()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                var customer = (Customer)cmbCustomer.SelectedItem;
                var profiles = uow.Profiles.Find(x => x.P_C == customer.C_ID).OrderBy(x => x.P_DESCRIPTION).ToList();
                profiles.Insert(0, new NodeData.Models.Profile { P_ID = Guid.Empty, P_DESCRIPTION = "(none selected)" });
                cmbSelectProfile.DataSource = profiles;
                cmbSelectProfile.DisplayMember = "P_DESCRIPTION";
                cmbSelectProfile.ValueMember = "P_ID";
            }
        }

        private void bbFind_Click(object sender, EventArgs e)
        {
            var customer = (Customer)cmbCustomer.SelectedItem;
            var profile = (NodeData.Models.Profile)cmbSelectProfile.SelectedItem;

            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.ConnString.ConnString)))
            {
                string filter = string.Empty;
                var procDate = DateTime.Parse(dtReceived.Text);
                List<object> paramslist = new List<object>();
                paramslist.Add(new { dtReceived.Text });
                paramslist.Add(new { edSenderID.Text });
                paramslist.Add(new { edRecipientId.Text });
                paramslist.Add(new { dtReceived.Text });
                paramslist.Add(new { edOriginalFileName.Text });
                paramslist.Add(new { edProcessedFileName.Text });
                paramslist.Add(new { customer.C_ID });
                if (profile!=null)
                {
                    paramslist.Add(new { profile.P_ID });
                }

                if (procDate != null)
                {
                   
                    if (string.IsNullOrEmpty(filter))
                    {
                        filter = "TL_RECEIVEDATE>=@0";


                    }
                    else
                    {
                        filter += " && TL_RECEIVEDATE>=@0";
                    }

                }
                if (!string.IsNullOrEmpty(edSenderID.Text))
                {
                    
                    if (string.IsNullOrEmpty(filter))
                    {
                        filter = "TL_SENDERID==@1";
                    }
                    else
                    {
                        filter += " && TL_SENDERID==@1";
                    }
                }
                if (!string.IsNullOrEmpty(edRecipientId.Text))
                {
                    if (string.IsNullOrEmpty(filter))
                    {
                        filter = "TL_RECIPIENTID==@2";
                    }
                    else
                    {
                        filter += " && TL_RECIPIENTID==@2";
                    }
                }
                if (!string.IsNullOrEmpty(edOriginalFileName.Text))
                {
                    if (string.IsNullOrEmpty(filter))
                    {
                        filter += "TL_ORIGINALFILENAME.Contains (@3)";
                    }
                    else
                    {
                        filter += "&& TL_ORIGINALFILENAME.Contains (@3)";
                    }
                }
                if (!string.IsNullOrEmpty(edProcessedFileName.Text))
                {
                    if (string.IsNullOrEmpty(filter))
                    {
                        filter += "TL_FILENAME.Contains (@4)";
                    }
                    else
                    {
                        filter += "&& TL_FILENAME.Contains (@4)";
                    }
                }
                if (customer.C_ID != Guid.Empty)
                {
                    if (string.IsNullOrEmpty(filter))
                    {
                        filter += "TL_C==@5";
                    }
                    else
                    {
                        filter += "&& TL_C==#5";
                    }
                }
                if (profile != null)
                {
                    if (profile.P_ID != Guid.Empty)
                    {
                        if (string.IsNullOrEmpty(filter))
                        {
                            filter += "TL_P==@6";
                        }
                        else
                        {
                            filter += "&& TL_P==@6";
                        }
                    }
                }

                uow.TaskLists.Find(filter,paramslist.ToArray());

            }
        }
    }
}
