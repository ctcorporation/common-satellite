﻿namespace Common_Satellite.Views
{
    partial class frmConvertData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConvertData));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFileToConvert = new System.Windows.Forms.TextBox();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnConvert = new System.Windows.Forms.Button();
            this.lvRequiredFields = new System.Windows.Forms.ListBox();
            this.dgMapping = new System.Windows.Forms.DataGridView();
            this.Col2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAddRequiredField = new System.Windows.Forms.Button();
            this.btnAddField = new System.Windows.Forms.Button();
            this.btnAddMapping = new System.Windows.Forms.Button();
            this.lvFieldList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbOperationList = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbMapList = new System.Windows.Forms.ComboBox();
            this.btnLoadMapping = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbOverride = new System.Windows.Forms.CheckBox();
            this.txtStaticValue = new System.Windows.Forms.TextBox();
            this.txtMapDescription = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMapName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSaveMapping = new System.Windows.Forms.Button();
            this.cmbDataTypes = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.txtOwner = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.jobType = new System.Windows.Forms.ComboBox();
            this.legNo = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.poNo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(896, 851);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "&Close";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select File to Convert";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select Output Directory";
            // 
            // txtFileToConvert
            // 
            this.txtFileToConvert.Location = new System.Drawing.Point(181, 22);
            this.txtFileToConvert.Margin = new System.Windows.Forms.Padding(4);
            this.txtFileToConvert.Name = "txtFileToConvert";
            this.txtFileToConvert.Size = new System.Drawing.Size(517, 22);
            this.txtFileToConvert.TabIndex = 5;
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Location = new System.Drawing.Point(181, 57);
            this.txtOutputFolder.Margin = new System.Windows.Forms.Padding(4);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(517, 22);
            this.txtOutputFolder.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(700, 20);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 28);
            this.button2.TabIndex = 9;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(700, 54);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 28);
            this.button3.TabIndex = 10;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnConvert
            // 
            this.btnConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConvert.Image = ((System.Drawing.Image)(resources.GetObject("btnConvert.Image")));
            this.btnConvert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConvert.Location = new System.Drawing.Point(896, 815);
            this.btnConvert.Margin = new System.Windows.Forms.Padding(4);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(100, 28);
            this.btnConvert.TabIndex = 11;
            this.btnConvert.Text = "C&onvert";
            this.btnConvert.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // lvRequiredFields
            // 
            this.lvRequiredFields.FormattingEnabled = true;
            this.lvRequiredFields.ItemHeight = 16;
            this.lvRequiredFields.Location = new System.Drawing.Point(724, 121);
            this.lvRequiredFields.Margin = new System.Windows.Forms.Padding(4);
            this.lvRequiredFields.Name = "lvRequiredFields";
            this.lvRequiredFields.Size = new System.Drawing.Size(252, 276);
            this.lvRequiredFields.TabIndex = 15;
            this.lvRequiredFields.DoubleClick += new System.EventHandler(this.lvRequiredFields_DoubleClick);
            // 
            // dgMapping
            // 
            this.dgMapping.AllowUserToAddRows = false;
            this.dgMapping.ColumnHeadersHeight = 29;
            this.dgMapping.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col2,
            this.Col3,
            this.DataType});
            this.dgMapping.Location = new System.Drawing.Point(269, 144);
            this.dgMapping.Margin = new System.Windows.Forms.Padding(4);
            this.dgMapping.Name = "dgMapping";
            this.dgMapping.ReadOnly = true;
            this.dgMapping.RowHeadersVisible = false;
            this.dgMapping.RowHeadersWidth = 51;
            this.dgMapping.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMapping.Size = new System.Drawing.Size(439, 183);
            this.dgMapping.TabIndex = 17;
            this.dgMapping.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgMapping_CellMouseDoubleClick);
            this.dgMapping.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgMapping_UserDeletedRow);
            this.dgMapping.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgMapping_UserDeletingRow);
            // 
            // Col2
            // 
            this.Col2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col2.DataPropertyName = "MapFrom";
            this.Col2.HeaderText = "Map From";
            this.Col2.MinimumWidth = 6;
            this.Col2.Name = "Col2";
            this.Col2.ReadOnly = true;
            // 
            // Col3
            // 
            this.Col3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Col3.DataPropertyName = "MapTo";
            this.Col3.HeaderText = "Map To";
            this.Col3.MinimumWidth = 6;
            this.Col3.Name = "Col3";
            this.Col3.ReadOnly = true;
            // 
            // DataType
            // 
            this.DataType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DataType.HeaderText = "Type";
            this.DataType.MinimumWidth = 6;
            this.DataType.Name = "DataType";
            this.DataType.ReadOnly = true;
            // 
            // btnAddRequiredField
            // 
            this.btnAddRequiredField.Location = new System.Drawing.Point(659, 335);
            this.btnAddRequiredField.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddRequiredField.Name = "btnAddRequiredField";
            this.btnAddRequiredField.Size = new System.Drawing.Size(51, 28);
            this.btnAddRequiredField.TabIndex = 18;
            this.btnAddRequiredField.Text = "<";
            this.btnAddRequiredField.UseVisualStyleBackColor = true;
            this.btnAddRequiredField.Click += new System.EventHandler(this.btnAddRequiredField_Click);
            // 
            // btnAddField
            // 
            this.btnAddField.Location = new System.Drawing.Point(271, 335);
            this.btnAddField.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.Size = new System.Drawing.Size(56, 28);
            this.btnAddField.TabIndex = 19;
            this.btnAddField.Text = ">";
            this.btnAddField.UseVisualStyleBackColor = true;
            this.btnAddField.Click += new System.EventHandler(this.btnAddField_Click);
            // 
            // btnAddMapping
            // 
            this.btnAddMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMapping.Image")));
            this.btnAddMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddMapping.Location = new System.Drawing.Point(335, 335);
            this.btnAddMapping.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddMapping.Name = "btnAddMapping";
            this.btnAddMapping.Size = new System.Drawing.Size(316, 28);
            this.btnAddMapping.TabIndex = 20;
            this.btnAddMapping.Text = "MapField";
            this.btnAddMapping.UseVisualStyleBackColor = true;
            this.btnAddMapping.Click += new System.EventHandler(this.btnAddMapping_Click);
            // 
            // lvFieldList
            // 
            this.lvFieldList.FormattingEnabled = true;
            this.lvFieldList.ItemHeight = 16;
            this.lvFieldList.Location = new System.Drawing.Point(17, 121);
            this.lvFieldList.Margin = new System.Windows.Forms.Padding(4);
            this.lvFieldList.Name = "lvFieldList";
            this.lvFieldList.Size = new System.Drawing.Size(241, 212);
            this.lvFieldList.TabIndex = 21;
            this.lvFieldList.DoubleClick += new System.EventHandler(this.lvFieldList_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txtOutputFolder);
            this.groupBox1.Controls.Add(this.txtFileToConvert);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(16, 108);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(985, 103);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Convert File Details";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbOperationList);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cmbMapList);
            this.groupBox2.Controls.Add(this.btnLoadMapping);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(16, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(985, 92);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Conversion Information";
            // 
            // cmbOperationList
            // 
            this.cmbOperationList.FormattingEnabled = true;
            this.cmbOperationList.Items.AddRange(new object[] {
            "(none selected) ",
            "Native Product (Cargowise)",
            "Organisation (Cargowise)"});
            this.cmbOperationList.Location = new System.Drawing.Point(215, 23);
            this.cmbOperationList.Margin = new System.Windows.Forms.Padding(4);
            this.cmbOperationList.Name = "cmbOperationList";
            this.cmbOperationList.Size = new System.Drawing.Size(360, 24);
            this.cmbOperationList.TabIndex = 14;
            this.cmbOperationList.DropDown += new System.EventHandler(this.cmbOperationList_DropDown);
            this.cmbOperationList.SelectionChangeCommitted += new System.EventHandler(this.cmbOperationList_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Select Conversion Type. ";
            // 
            // cmbMapList
            // 
            this.cmbMapList.FormattingEnabled = true;
            this.cmbMapList.Location = new System.Drawing.Point(159, 57);
            this.cmbMapList.Margin = new System.Windows.Forms.Padding(4);
            this.cmbMapList.Name = "cmbMapList";
            this.cmbMapList.Size = new System.Drawing.Size(603, 24);
            this.cmbMapList.TabIndex = 30;
            // 
            // btnLoadMapping
            // 
            this.btnLoadMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadMapping.Image")));
            this.btnLoadMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLoadMapping.Location = new System.Drawing.Point(771, 54);
            this.btnLoadMapping.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadMapping.Name = "btnLoadMapping";
            this.btnLoadMapping.Size = new System.Drawing.Size(151, 28);
            this.btnLoadMapping.TabIndex = 25;
            this.btnLoadMapping.Text = "Load Mapping";
            this.btnLoadMapping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLoadMapping.UseVisualStyleBackColor = true;
            this.btnLoadMapping.Click += new System.EventHandler(this.btnLoadMapping_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 58);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 17);
            this.label10.TabIndex = 29;
            this.label10.Text = "Select Saved Map";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbOverride);
            this.groupBox3.Controls.Add(this.txtStaticValue);
            this.groupBox3.Controls.Add(this.txtMapDescription);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtMapName);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnSaveMapping);
            this.groupBox3.Controls.Add(this.cmbDataTypes);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.lvFieldList);
            this.groupBox3.Controls.Add(this.btnAddMapping);
            this.groupBox3.Controls.Add(this.btnAddField);
            this.groupBox3.Controls.Add(this.btnAddRequiredField);
            this.groupBox3.Controls.Add(this.dgMapping);
            this.groupBox3.Controls.Add(this.lvRequiredFields);
            this.groupBox3.Location = new System.Drawing.Point(16, 293);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(985, 414);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Field Mapping ";
            // 
            // cbOverride
            // 
            this.cbOverride.AutoSize = true;
            this.cbOverride.Location = new System.Drawing.Point(21, 342);
            this.cbOverride.Margin = new System.Windows.Forms.Padding(4);
            this.cbOverride.Name = "cbOverride";
            this.cbOverride.Size = new System.Drawing.Size(206, 21);
            this.cbOverride.TabIndex = 42;
            this.cbOverride.Text = "Override Value (only String)";
            this.cbOverride.UseVisualStyleBackColor = true;
            this.cbOverride.CheckedChanged += new System.EventHandler(this.cbOverride_CheckedChanged);
            // 
            // txtStaticValue
            // 
            this.txtStaticValue.Location = new System.Drawing.Point(17, 367);
            this.txtStaticValue.Margin = new System.Windows.Forms.Padding(4);
            this.txtStaticValue.Name = "txtStaticValue";
            this.txtStaticValue.Size = new System.Drawing.Size(241, 22);
            this.txtStaticValue.TabIndex = 41;
            // 
            // txtMapDescription
            // 
            this.txtMapDescription.Location = new System.Drawing.Point(161, 63);
            this.txtMapDescription.Margin = new System.Windows.Forms.Padding(4);
            this.txtMapDescription.Name = "txtMapDescription";
            this.txtMapDescription.Size = new System.Drawing.Size(603, 22);
            this.txtMapDescription.TabIndex = 39;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 64);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 17);
            this.label12.TabIndex = 38;
            this.label12.Text = "Map Description ";
            // 
            // txtMapName
            // 
            this.txtMapName.Location = new System.Drawing.Point(161, 31);
            this.txtMapName.Margin = new System.Windows.Forms.Padding(4);
            this.txtMapName.Name = "txtMapName";
            this.txtMapName.Size = new System.Drawing.Size(444, 22);
            this.txtMapName.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 32);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 17);
            this.label9.TabIndex = 36;
            this.label9.Text = "Map Name";
            // 
            // btnSaveMapping
            // 
            this.btnSaveMapping.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveMapping.Image")));
            this.btnSaveMapping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveMapping.Location = new System.Drawing.Point(615, 27);
            this.btnSaveMapping.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveMapping.Name = "btnSaveMapping";
            this.btnSaveMapping.Size = new System.Drawing.Size(132, 28);
            this.btnSaveMapping.TabIndex = 35;
            this.btnSaveMapping.Text = "&Save Mapping";
            this.btnSaveMapping.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveMapping.UseVisualStyleBackColor = true;
            this.btnSaveMapping.Click += new System.EventHandler(this.btnSaveMapping_Click);
            // 
            // cmbDataTypes
            // 
            this.cmbDataTypes.FormattingEnabled = true;
            this.cmbDataTypes.Location = new System.Drawing.Point(361, 372);
            this.cmbDataTypes.Margin = new System.Windows.Forms.Padding(4);
            this.cmbDataTypes.Name = "cmbDataTypes";
            this.cmbDataTypes.Size = new System.Drawing.Size(240, 24);
            this.cmbDataTypes.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(271, 375);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 17);
            this.label11.TabIndex = 31;
            this.label11.Text = "Data Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(720, 98);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(251, 16);
            this.label8.TabIndex = 24;
            this.label8.Text = "Fields (From Conversion Selection)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(269, 121);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 16);
            this.label7.TabIndex = 23;
            this.label7.Text = "Mapping Definition";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 96);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 16);
            this.label5.TabIndex = 22;
            this.label5.Text = "Available Fields (From File)";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtCustomer);
            this.groupBox4.Controls.Add(this.txtOwner);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(16, 219);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(985, 66);
            this.groupBox4.TabIndex = 26;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Organisation Details";
            // 
            // txtCustomer
            // 
            this.txtCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustomer.Location = new System.Drawing.Point(696, 27);
            this.txtCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(132, 22);
            this.txtCustomer.TabIndex = 44;
            // 
            // txtOwner
            // 
            this.txtOwner.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtOwner.Location = new System.Drawing.Point(313, 27);
            this.txtOwner.Margin = new System.Windows.Forms.Padding(4);
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Size = new System.Drawing.Size(132, 22);
            this.txtOwner.TabIndex = 43;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(468, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 17);
            this.label4.TabIndex = 42;
            this.label4.Text = "(To) Cargowise System Code";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(241, 17);
            this.label3.TabIndex = 41;
            this.label3.Text = "(From) Cargowise Organization Code";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.jobType);
            this.groupBox5.Controls.Add(this.legNo);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.poNo);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Location = new System.Drawing.Point(16, 713);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(984, 95);
            this.groupBox5.TabIndex = 27;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Job Details";
            this.groupBox5.Visible = false;
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // jobType
            // 
            this.jobType.FormattingEnabled = true;
            this.jobType.Location = new System.Drawing.Point(383, 39);
            this.jobType.Name = "jobType";
            this.jobType.Size = new System.Drawing.Size(315, 24);
            this.jobType.TabIndex = 8;
            // 
            // legNo
            // 
            this.legNo.Location = new System.Drawing.Point(850, 39);
            this.legNo.Name = "legNo";
            this.legNo.Size = new System.Drawing.Size(121, 22);
            this.legNo.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(720, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(120, 17);
            this.label15.TabIndex = 4;
            this.label15.Text = "Start Leg Number";
            // 
            // poNo
            // 
            this.poNo.Location = new System.Drawing.Point(107, 39);
            this.poNo.Name = "poNo";
            this.poNo.Size = new System.Drawing.Size(180, 22);
            this.poNo.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(310, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 17);
            this.label14.TabIndex = 1;
            this.label14.Text = "Job Type";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "P.O. Number";
            // 
            // frmConvertData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 894);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmConvertData";
            this.Text = "Convert Data File. ";
            this.Load += new System.EventHandler(this.frmConvertProductData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgMapping)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFileToConvert;
        private System.Windows.Forms.TextBox txtOutputFolder;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.ListBox lvRequiredFields;
        private System.Windows.Forms.DataGridView dgMapping;
        private System.Windows.Forms.Button btnAddRequiredField;
        private System.Windows.Forms.Button btnAddField;
        private System.Windows.Forms.Button btnAddMapping;
        private System.Windows.Forms.ListBox lvFieldList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbOperationList;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnLoadMapping;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbMapList;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbDataTypes;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.TextBox txtOwner;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMapDescription;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMapName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSaveMapping;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col3;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataType;
        private System.Windows.Forms.TextBox txtStaticValue;
        private System.Windows.Forms.CheckBox cbOverride;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox legNo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox poNo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox jobType;
    }
}