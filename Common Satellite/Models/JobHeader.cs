namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobHeader")]
    public partial class JobHeader
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobHeader()
        {
            TransportLegs = new HashSet<TransportLeg>();
        }

        [Key]
        public Guid J_JOBID { get; set; }

        [StringLength(128)]
        public string J_U { get; set; }

        public Guid? J_O_Customer { get; set; }

        public int? J_Status { get; set; }

        public int? J_ItemsTotal { get; set; }

        public int? J_ItemsDelivered { get; set; }

        public bool? J_UploadSuccessful { get; set; }

        [StringLength(20)]
        public string J_TransportJobNo { get; set; }

        [StringLength(10)]
        public string J_JobType { get; set; }

        [StringLength(3)]
        public string J_ContainerMode { get; set; }

        [StringLength(3)]
        public string J_DropMode { get; set; }

        public DateTime? J_PlannedDeliveryTime { get; set; }

        public DateTime? J_DeliveryTime { get; set; }

        public DateTime? J_StartTime { get; set; }

        public int? J_TotalPieces { get; set; }

        public decimal? J_TotalWeight { get; set; }

        public decimal? J_TotalVolume { get; set; }

        [StringLength(3)]
        public string J_UOM { get; set; }

        public DateTime? J_FinishTime { get; set; }

        public DateTime? J_LastUpdated { get; set; }

        public int? J_Priority { get; set; }

        [StringLength(20)]
        public string J_Reference { get; set; }

        [StringLength(200)]
        public string J_Description { get; set; }

        [StringLength(1)]
        public string J_Hazardous { get; set; }

        public byte[] J_Signature { get; set; }

        [StringLength(200)]
        public string J_SignedBy { get; set; }

        [StringLength(200)]
        public string J_EmailCode { get; set; }

        public DateTime? J_CreateDate { get; set; }

        public bool? J_CWJobType { get; set; }

        [StringLength(50)]
        public string J_ORDERNO { get; set; }

        public virtual Enums Enum { get; set; }

        public virtual Organisation Organisation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransportLeg> TransportLegs { get; set; }
    }
}
