namespace Common_Satellite.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TransModContext : DbContext
    {
        public TransModContext()
            : base("name=TransModContext")
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Enums> Enums { get; set; }
        public virtual DbSet<EnumType> EnumTypes { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<JobHeader> JobHeaders { get; set; }
        public virtual DbSet<JobNote> JobNotes { get; set; }
        public virtual DbSet<Organisation> Organisations { get; set; }
        public virtual DbSet<RunLink> RunLinks { get; set; }
        public virtual DbSet<TransportLeg> TransportLegs { get; set; }
        public virtual DbSet<RunSheet> RunSheets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>()
                .Property(e => e.A_AddressType)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.A_Suburb)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.A_PostCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.A_Ph)
                .IsFixedLength();

            modelBuilder.Entity<Address>()
                .Property(e => e.A_MainAddress)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.A_State)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.A_CODE)
                .IsFixedLength();

            modelBuilder.Entity<Address>()
                .Property(e => e.A_SHORTCODE)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .Property(e => e.A_Email)
                .IsUnicode(false);

            modelBuilder.Entity<Address>()
                .HasMany(e => e.TransportLegs)
                .WithOptional(e => e.Address)
                .HasForeignKey(e => e.T_A);

            modelBuilder.Entity<Enums>()
                .Property(e => e.E_Name)
                .IsUnicode(false);

            modelBuilder.Entity<Enums>()
                .HasMany(e => e.JobHeaders)
                .WithOptional(e => e.Enum)
                .HasForeignKey(e => e.J_Status);

            modelBuilder.Entity<Enums>()
                .HasMany(e => e.JobNotes)
                .WithRequired(e => e.Enum)
                .HasForeignKey(e => e.N_JobNoteType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Enums>()
                .HasMany(e => e.TransportLegs)
                .WithOptional(e => e.Enum)
                .HasForeignKey(e => e.T_Status);

            modelBuilder.Entity<EnumType>()
                .Property(e => e.Z_Name)
                .IsUnicode(false);

            modelBuilder.Entity<EnumType>()
                .HasMany(e => e.Enums)
                .WithRequired(e => e.EnumType)
                .HasForeignKey(e => e.E_EnumType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Image>()
                .Property(e => e.I_ReftTable)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_TransportJobNo)
                .IsFixedLength();

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_JobType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_ContainerMode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_DropMode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_TotalWeight)
                .HasPrecision(10, 2);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_TotalVolume)
                .HasPrecision(8, 2);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_UOM)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_Reference)
                .IsFixedLength();

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_Hazardous)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_SignedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .Property(e => e.J_EmailCode)
                .IsUnicode(false);

            modelBuilder.Entity<JobHeader>()
                .HasMany(e => e.TransportLegs)
                .WithOptional(e => e.JobHeader)
                .HasForeignKey(e => e.T_J);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.N_Reference)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .HasMany(e => e.Images)
                .WithOptional(e => e.JobNote)
                .HasForeignKey(e => e.I_REF);

            modelBuilder.Entity<Organisation>()
                .Property(e => e.O_Code)
                .IsFixedLength();

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Organisation)
                .HasForeignKey(e => e.A_O);

            modelBuilder.Entity<Organisation>()
                .HasMany(e => e.JobHeaders)
                .WithOptional(e => e.Organisation)
                .HasForeignKey(e => e.J_O_Customer);

            modelBuilder.Entity<RunLink>()
                .Property(e => e.RL_STATUS)
                .IsFixedLength();

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_ContainerNo)
                .IsFixedLength();

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_Weight)
                .HasPrecision(10, 2);

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_Volume)
                .HasPrecision(8, 2);

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_DropMode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_Containermode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_JobType)
                .IsFixedLength();

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_IsLoaded)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_UOM)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TransportLeg>()
                .Property(e => e.T_SignedBy)
                .IsUnicode(false);

            modelBuilder.Entity<TransportLeg>()
                .HasMany(e => e.JobNotes)
                .WithOptional(e => e.TransportLeg)
                .HasForeignKey(e => e.N_T);

            modelBuilder.Entity<RunSheet>()
                .Property(e => e.RS_STATUS)
                .IsFixedLength();

            modelBuilder.Entity<RunSheet>()
                .Property(e => e.RS_VEHICLE)
                .IsFixedLength();
        }
    }
}
