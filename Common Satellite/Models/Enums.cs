namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Enum")]
    public partial class Enums
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Enums()
        {
            JobHeaders = new HashSet<JobHeader>();
            JobNotes = new HashSet<JobNote>();
            TransportLegs = new HashSet<TransportLeg>();
        }

        [Key]
        public int E_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string E_Name { get; set; }

        public int E_EnumType { get; set; }

        public virtual EnumType EnumType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobHeader> JobHeaders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobNote> JobNotes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransportLeg> TransportLegs { get; set; }
    }
}
