namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Organisation")]
    public partial class Organisation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organisation()
        {
            Addresses = new HashSet<Address>();
            JobHeaders = new HashSet<JobHeader>();
        }

        [Key]
        public Guid O_ID { get; set; }

        [StringLength(100)]
        public string O_Name { get; set; }

        [StringLength(15)]
        public string O_Code { get; set; }

        public bool? O_Isactive { get; set; }

        public bool? O_OnHold { get; set; }

        [StringLength(100)]
        public string O_Email { get; set; }

        public DateTime? O_TrialStart { get; set; }

        public DateTime? O_TrialEnd { get; set; }

        public bool? O_IsConsignee { get; set; }

        public Guid? O_O_PARENT { get; set; }

        public int O_LicenseCount { get; set; }

        public byte[] O_Logo { get; set; }

        [StringLength(100)]
        public string O_EDIEmail { get; set; }

        public byte[] O_Letterhead { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Address> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobHeader> JobHeaders { get; set; }
    }
}
