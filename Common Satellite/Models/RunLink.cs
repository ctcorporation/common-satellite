namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RunLink")]
    public partial class RunLink
    {
        [Key]
        public Guid RL_ID { get; set; }

        public Guid? RL_TP { get; set; }

        public Guid? RL_TD { get; set; }

        [StringLength(10)]
        public string RL_STATUS { get; set; }

        [StringLength(255)]
        public string RL_NOTES { get; set; }

        public Guid? RL_RS { get; set; }

        public int? RL_SEQUENCE { get; set; }
    }
}
