namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Address")]
    public partial class Address
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Address()
        {
            TransportLegs = new HashSet<TransportLeg>();
        }

        [Key]
        public Guid A_ID { get; set; }

        public Guid? A_O { get; set; }

        [StringLength(50)]
        public string A_AddressType { get; set; }

        [StringLength(100)]
        public string A_AddressName { get; set; }

        [StringLength(50)]
        public string A_Address1 { get; set; }

        [StringLength(50)]
        public string A_Address2 { get; set; }

        [StringLength(50)]
        public string A_Address3 { get; set; }

        [StringLength(50)]
        public string A_Suburb { get; set; }

        [StringLength(4)]
        public string A_PostCode { get; set; }

        [StringLength(15)]
        public string A_Ph { get; set; }

        public string A_AddressNotes { get; set; }

        [StringLength(1)]
        public string A_MainAddress { get; set; }

        [StringLength(25)]
        public string A_State { get; set; }

        [StringLength(15)]
        public string A_CODE { get; set; }

        [StringLength(50)]
        public string A_SHORTCODE { get; set; }

        [StringLength(50)]
        public string A_Email { get; set; }

        public virtual Organisation Organisation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransportLeg> TransportLegs { get; set; }
    }
}
