namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Image")]
    public partial class Image
    {
        [Key]
        public Guid I_ID { get; set; }

        public Guid? I_REF { get; set; }

        [StringLength(1)]
        public string I_ReftTable { get; set; }

        public byte[] I_Image { get; set; }

        public virtual JobNote JobNote { get; set; }
    }
}
