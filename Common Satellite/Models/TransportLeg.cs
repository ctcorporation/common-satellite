namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransportLeg")]
    public partial class TransportLeg
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TransportLeg()
        {
            JobNotes = new HashSet<JobNote>();
        }

        [Key]
        public Guid T_ID { get; set; }

        public Guid? T_J { get; set; }

        [StringLength(13)]
        public string T_ContainerNo { get; set; }

        public int? T_Pieces { get; set; }

        public decimal? T_Weight { get; set; }

        public decimal? T_Volume { get; set; }

        public Guid? T_A { get; set; }

        public DateTime? T_TimeStart { get; set; }

        public DateTime? T_TimeFin { get; set; }

        [StringLength(3)]
        public string T_DropMode { get; set; }

        [StringLength(3)]
        public string T_Containermode { get; set; }

        [StringLength(10)]
        public string T_JobType { get; set; }

        public int? T_PiecesDelivered { get; set; }

        public int? T_Status { get; set; }

        public int? T_LegNo { get; set; }

        public int? T_Priority { get; set; }

        [StringLength(1)]
        public string T_IsLoaded { get; set; }

        public DateTime? T_RequiredDate { get; set; }

        [StringLength(128)]
        public string T_DriverId { get; set; }

        [StringLength(3)]
        public string T_UOM { get; set; }

        public int? T_Sequence { get; set; }

        public byte[] T_Signature { get; set; }

        [StringLength(200)]
        public string T_SignedBy { get; set; }

        public virtual Address Address { get; set; }

        public virtual Enums Enum { get; set; }

        public virtual JobHeader JobHeader { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobNote> JobNotes { get; set; }
    }
}
