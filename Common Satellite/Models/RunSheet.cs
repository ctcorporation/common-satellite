namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RunSheet")]
    public partial class RunSheet
    {
        [Key]
        public Guid RS_ID { get; set; }

        public DateTime? RS_STARTDATE { get; set; }

        public DateTime? RS_ENDDATE { get; set; }

        public Guid? RS_DriverID { get; set; }

        [StringLength(10)]
        public string RS_STATUS { get; set; }

        [StringLength(10)]
        public string RS_VEHICLE { get; set; }
    }
}
