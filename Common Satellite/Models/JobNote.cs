namespace Common_Satellite.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobNote")]
    public partial class JobNote
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JobNote()
        {
            Images = new HashSet<Image>();
        }

        [Key]
        public Guid N_ID { get; set; }

        public Guid? N_T { get; set; }

        public string N_Note { get; set; }

        public DateTime? N_Date { get; set; }

        public int N_JobNoteType { get; set; }

        [StringLength(100)]
        public string N_Reference { get; set; }

        public virtual Enums Enum { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Image> Images { get; set; }

        public virtual TransportLeg TransportLeg { get; set; }
    }
}
