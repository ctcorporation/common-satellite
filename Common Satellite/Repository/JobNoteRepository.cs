﻿using Common_Satellite.Interfaces;
using Common_Satellite.Models;

namespace Common_Satellite.Repository
{
    public class JobNoteRepository : GenericRepository<JobNote>, IJobNoteRepository
    {
        public JobNoteRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return Context as TransModContext;
            }
        }

    }
}
