﻿using Common_Satellite.Interfaces;
using Common_Satellite.Models;

namespace Common_Satellite.Repository
{
    public class TransportLegRepository : GenericRepository<TransportLeg>, ITransportLegRepository

    {
        public TransportLegRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            { return Context as TransModContext; }
        }
    }
}
