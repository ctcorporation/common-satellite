﻿using Common_Satellite.Interfaces;
using Common_Satellite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace Common_Satellite.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        #region members
        protected readonly TransModContext Context;
        #endregion

        #region properties

        #endregion

        #region constructors
        public GenericRepository(TransModContext context)
        {
            Context = context;
        }
        #endregion

        #region methods
        public void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);

        }

        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);

        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }

        public TEntity Get(Guid id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return Context.Set<TEntity>();
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            var query = Context.Set<TEntity>().Where(predicate);
            return query;
        }

        public IQueryable<TEntity> Find(string filter, object[] paramList)
        {
            return Context.Set<TEntity>().Where(filter, paramList);
        }
        #endregion

        #region helpers

        #endregion
    }
}
