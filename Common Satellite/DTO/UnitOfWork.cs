﻿using Common_Satellite.Interfaces;
using Common_Satellite.Models;
using Common_Satellite.Repository;
using System;
using System.Data.Entity.Validation;

namespace Common_Satellite.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        #region members
        private readonly TransModContext _context;
        private bool disposed = false;
        private string _errorMessage = string.Empty;

        #endregion

        #region properties
        public string ErrorMessage
        {
            get
            { return _errorMessage; }
            set
            {
                _errorMessage = value;
            }
        }

        public IAddressRepository Addresses
        {
            get; private set;
        }

        public IEnumRepository TransEnums
        {
            get;
            private set;
        }

        public IEnumTypesRepository TransEnumTypes
        {
            get; private set;
        }

        public IImageRepository Images
        {
            get;
            private set;
        }

        public IJobHeaderRepository JobHeaders
        {
            get;
            private set;
        }

        public IJobNoteRepository JobNotes
        {
            get;
            private set;
        }

        public IOrganisationRepository Organisations
        {
            get;
            private set;
        }

        public IRunLinkRepository RunLinks
        {
            get;
            private set;

        }

        public IRunSheetRepository RunSheets
        {
            get;
            private set;
        }
        public ITransportLegRepository TransportLegs
        {
            get;
            private set;
        }
        #endregion

        #region constructors
        public UnitOfWork(TransModContext context)
        {
            _context = context;
            AddRepos();
        }


        #endregion

        #region methods
        private void AddRepos()
        {
            Addresses = new AddressRepository(_context);
            TransEnums = new EnumRepository(_context);
            TransEnumTypes = new EnumTypesRepository(_context);
            Images = new ImageRepository(_context);
            JobHeaders = new JobHeaderRepository(_context);
            JobNotes = new JobNoteRepository(_context);
            Organisations = new OrganisationRepository(_context);
            RunLinks = new RunLinkRepository(_context);
            RunSheets = new RunSheetRepository(_context);
            TransportLegs = new TransportLegRepository(_context);
        }

        public bool DbExists()
        {
            return _context.Database.Exists();
        }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                _errorMessage += ex.Message.GetType() + " Found: Error was: " + ex.Message + ": " + ex.InnerException.Message + Environment.NewLine;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        _errorMessage += entityName + ": " + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;
                    }
                }
            }
            return -1;

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UnitOfWork()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region helpers

        #endregion

    }
}
