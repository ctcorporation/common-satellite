﻿

using NodeData;
using NodeResources;

namespace Common_Satellite
{
    public static class Globals
    {
        private static IMailServerSettings _mailServerSettings
        { get; set; }
        private static string _appPath { get; set; }
        private static string _appLogPath { get; set; }
        private static string _tempLocation { get; set; }
        private static string _archiveLocation { get; set; }
        private static string _primaryLocation { get; set; }
        private static string _testTempLocation { get; set; }
        private static string _testArchiveLocation { get; set; }
        private static string _testPrimaryLocation { get; set; }
        private static string _testPickupLocation { get; set; }
        private static string _testOutputLocation { get; set; }
        private static string _prodOutputLocation { get; set; }
        private static string _prodTempLocation { get; set; }
        private static string _winSCPLocation { get; set; }
        private static string _pickupLocation { get; set; }
        private static int _pollTime { get; set; }
        private static int _refeshTime { get; set; }
        private static string _libraryName { get; set; }
        private static string _cNodePath { get; set; }
        private static NodeData.IConnectionManager _connString { get; set; }
        private static string _appConfig
        {
            get; set;
        }

        public static IMailServerSettings MailServerSettings
        {
            get { return _mailServerSettings; }
            set { _mailServerSettings = value; }
        }

        public static string TempLocation
        {
            get { return _tempLocation; }
            set { _tempLocation = value; }
        }

        public static string WinSCPLocation
        {
            get { return _winSCPLocation; }
            set { _winSCPLocation = value; }
        }
        public static string ArchiveLocation
        {
            get { return _archiveLocation; }
            set { _archiveLocation = value; }
        }
        public static string PrimaryLocation
        {
            get { return _primaryLocation; }
            set { _primaryLocation = value; }
        }
        public static string TestPickupLocation
        {
            get { return _testPickupLocation; }
            set { _testPickupLocation = value; }
        }
        public static string PickupLocation
        {
            get { return _pickupLocation; }
            set { _pickupLocation = value; }
        }
        public static string TestTempLocation
        {
            get { return _testTempLocation; }
            set { _testTempLocation = value; }
        }
        public static string TestOutputLocation
        {
            get { return _testOutputLocation; }
            set { _testOutputLocation = value; }
        }
        public static string ProdTempLocation
        {
            get { return _prodTempLocation; }
            set { _prodTempLocation = value; }
        }
        public static string ProdOutputLocation
        {
            get { return _prodOutputLocation; }
            set { _prodOutputLocation = value; }
        }
        public static string TestArchiveLocation
        {
            get { return _testArchiveLocation; }
            set { _testArchiveLocation = value; }
        }
        public static string TestPrimaryLocation
        {
            get { return _testPrimaryLocation; }
            set { _testPrimaryLocation = value; }
        }
        public static string AppPath
        {
            get { return _appPath; }
            set { _appPath = value; }
        }

        public static string AppLogPath
        {
            get { return _appLogPath; }
            set { _appLogPath = value; }

        }

        public static string LibraryName
        {
            get { return _libraryName; }
            set { _libraryName = value; }
        }

        public static int PollTime
        {
            get { return _pollTime; }
            set { _pollTime = value; }
        }

        public static int RefreshTime
        {
            get { return _refeshTime; }
            set { _refeshTime = value; }
        }

        public static string AppConfig
        {
            get { return _appConfig; }
            set { _appConfig = value; }
        }
        public static NodeData.IConnectionManager ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        public static string AlertsTo { get; internal set; }
        public static string CNodePath
        {
            get
            { return _cNodePath; }
            set
            { _cNodePath = value; }
        }


    }
}
