﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IRunLinkRepository : IGenericRepository<RunLink>
    {
        TransModContext TransModContext { get; }
    }
}
