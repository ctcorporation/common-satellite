﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IEnumTypesRepository : IGenericRepository<EnumType>
    {
        TransModContext TransModContext { get; }
    }
}
