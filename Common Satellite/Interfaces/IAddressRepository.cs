﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IAddressRepository : IGenericRepository<Address>
    {
        TransModContext TransModContext { get; }
    }
}