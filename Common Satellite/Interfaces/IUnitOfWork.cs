﻿using System;

namespace Common_Satellite.Interfaces

{
    public interface IUnitOfWork : IDisposable
    {
        string ErrorMessage { get; set; }

        IAddressRepository Addresses { get; }
        IEnumRepository TransEnums { get; }
        IEnumTypesRepository TransEnumTypes { get; }
        IImageRepository Images { get; }
        IJobHeaderRepository JobHeaders { get; }
        IJobNoteRepository JobNotes { get; }
        IOrganisationRepository Organisations { get; }
        IRunLinkRepository RunLinks { get; }
        IRunSheetRepository RunSheets { get; }
        ITransportLegRepository TransportLegs { get; }
        int Complete();
        bool DbExists();
    }
}