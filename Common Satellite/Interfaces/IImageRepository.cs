﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IImageRepository : IGenericRepository<Image>
    {
        TransModContext TransModContext { get; }
    }
}
