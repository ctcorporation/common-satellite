﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IJobNoteRepository : IGenericRepository<JobNote>
    {
        TransModContext TransModContext { get; }
    }
}
