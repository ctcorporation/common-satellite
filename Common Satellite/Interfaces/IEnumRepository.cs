﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IEnumRepository : IGenericRepository<Enums>
    {
        TransModContext TransModContext { get; }
    }
}