﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IOrganisationRepository : IGenericRepository<Organisation>
    {
        TransModContext TransModContext { get; }
    }
}
