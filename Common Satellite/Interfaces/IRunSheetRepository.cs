﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IRunSheetRepository : IGenericRepository<RunSheet>
    {
        TransModContext TransModContext { get; }
    }
}
