﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface ITransportLegRepository : IGenericRepository<TransportLeg>
    {
        TransModContext TransModContext { get; }
    }
}
