﻿using Common_Satellite.Models;

namespace Common_Satellite.Interfaces
{
    public interface IJobHeaderRepository : IGenericRepository<JobHeader>
    {
        TransModContext TransModContext
        {
            get;
        }
    }
}
