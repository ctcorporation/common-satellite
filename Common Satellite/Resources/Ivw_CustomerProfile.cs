﻿using System;

namespace Common_Satellite.Resources
{
    public interface Ivw_CustomerProfile
    {
        string C_CODE { get; set; }
        string C_FTP_CLIENT { get; set; }
        Guid C_ID { get; set; }
        string C_IS_ACTIVE { get; set; }
        string C_NAME { get; set; }
        string C_ON_HOLD { get; set; }
        string C_PATH { get; set; }
        string C_SHORTNAME { get; set; }
        string P_ACTIVE { get; set; }
        string P_BILLTO { get; set; }
        Guid? P_C { get; set; }
        string P_CHARGEABLE { get; set; }
        string P_CUSTOMERCOMPANYNAME { get; set; }
        string P_CWAPPCODE { get; set; }
        string P_CWFILENAME { get; set; }
        string P_CWSUBJECT { get; set; }
        string P_DELIVERY { get; set; }
        string P_DESCRIPTION { get; set; }
        string P_DIRECTION { get; set; }
        string P_DTS { get; set; }
        string P_EMAILADDRESS { get; set; }
        string P_EVENTCODE { get; set; }
        string P_FILETYPE { get; set; }
        string P_FORWARDWITHFLAGS { get; set; }
        Guid P_ID { get; set; }
        string P_LIBNAME { get; set; }
        string P_MESSAGEDESCR { get; set; }
        string P_MESSAGETYPE { get; set; }
        string P_METHOD { get; set; }
        string P_MSGTYPE { get; set; }
        string P_NOTIFY { get; set; }
        string P_PARAMLIST { get; set; }
        string P_PASSWORD { get; set; }
        string P_PATH { get; set; }
        string P_PORT { get; set; }
        string P_REASONCODE { get; set; }
        string P_RECIPIENTID { get; set; }
        string P_SENDEREMAIL { get; set; }
        string P_SENDERID { get; set; }
        string P_SERVER { get; set; }
        string P_SSL { get; set; }
        string P_SUBJECT { get; set; }
        string P_USERNAME { get; set; }
        string P_XSD { get; set; }
    }
}
