﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Common_Satellite.Classes
{
    class ConvertTransport
    {
        #region members
        string _errorLog;
        string _fileName;
        string _connString;
        DataTable _table;
        List<Mapping> mapping;
        #endregion

        #region constructors
        public ConvertTransport(DataTable tb, string connString)
        {
            ConnString = connString;
            _table = tb;
        }
        #endregion

        #region properties

        public string ConnString
        {
            get
            { return _connString; }
            set
            { _connString = value; }
        }
        public List<MapOperation> Map { get; set; }
        public string FileName
        {
            get
            { return _fileName; }
            set
            { _fileName = value; }
        }
        public string ErrorLog
        {
            get
            { return _errorLog; }
        }
        #endregion

        #region methods

        private void GetFieldNames(List<MapOperation> map, Guid mapHeaderId)
        {
            mapping = new List<Mapping>();
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BoxNo"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "SONo"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "JobNo"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BarCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ClientSort"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "FactorySort"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "TrackingNo"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Logo"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Remarks"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "CompanyName"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "DelCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ContactName"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "PhoneNo"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Port"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "State"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "City"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "PostCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Address1"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Email"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "UserID"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Code"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "SW"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Description"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Weight"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Color"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderQty"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "PackingSize"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "TotalCubage"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "PackageUnit"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderDate"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ConfirmDate"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Width"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Height"));

        }



        public NodeFile CreateCommon(List<MapOperation> map, Guid mapHeaderId)
        {
            GetFieldNames(map, mapHeaderId);


            NodeFile common = new NodeFile();
            string currentOrderNo = string.Empty;
            string prevOrderNo = string.Empty;
            List<NodeFileTransportJob> orders = new List<NodeFileTransportJob>();
            int iLineNo = 0;
            NodeFileTransportJob order = new NodeFileTransportJob();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            for (int i = 1; i < _table.Rows.Count; i++)
            {
                if (_table.Rows[i].ItemArray[1].ToString() == "")
                {
                    continue;
                }

                order = CreateTransportJobFromRow(_table.Rows[i]);

                OrderLineElement line = CreateOrderLineFromRow(_table.Rows[i]);
                orderLines.Add(line);
                order.OrderLine = line;

                orders.Add(order);


                CompanyElement company = CreateCompanyFromRow(_table.Rows[i]);

                order.Company = company;

                List<DateElement> dates = GetDatesFromRow(_table.Rows[i]);

                order.Dates = dates.ToArray();


            }

            common.TransportJob = orders.ToArray();
            return common;
        }

        private OrderLineElement CreateOrderLineFromRow(DataRow dataRow)
        {
            OrderLineElement line = new OrderLineElement();

            var ordQty = FromMapping("OrderQty", dataRow);
            if (!string.IsNullOrEmpty(ordQty))
            {
                line.OrderQty = decimal.Parse(ordQty);
            }


            var gross = FromMapping("Weight", dataRow);
            if (!string.IsNullOrEmpty(gross))
            {
                line.Weight = decimal.Parse(gross);
            }

            var volume = FromMapping("Volume", dataRow);
            if (!string.IsNullOrEmpty(volume))
            {
                line.Volume = decimal.Parse(volume);
            }


            return line;

        }

        private CompanyElement CreateCompanyFromRow(DataRow dataRow)
        {
            CompanyElement comp = new CompanyElement();
            var name = FromMapping("CompanyName", dataRow);
            if (!string.IsNullOrEmpty(name))
            {
                comp.CompanyName = name;
            }
            var del = FromMapping("DelCode", dataRow);
            if (!string.IsNullOrEmpty(del))
            {
                comp.DelCode = del;

            }
            var contact = FromMapping("ContactName", dataRow);
            if (!string.IsNullOrEmpty(contact))
            {
                comp.ContactName = contact;

            }
            var phone = FromMapping("PhoneNo", dataRow);
            if (!string.IsNullOrEmpty(phone))
            {
                comp.PhoneNo = phone;
            }

            var port = FromMapping("Port", dataRow);
            if (!string.IsNullOrEmpty(port))
            {
                comp.Port = port;
            }
            var state = FromMapping("State", dataRow);
            if (!string.IsNullOrEmpty(state))
            {
                comp.State = state;
            }
            var city = FromMapping("City", dataRow);
            if (!string.IsNullOrEmpty(city))
            {
                comp.City = city;
            }
            var zip = FromMapping("PostCode", dataRow);
            if (!string.IsNullOrEmpty(zip))
            {
                comp.PostCode = zip;
            }
            var shipto = FromMapping("Address1", dataRow);
            if (!string.IsNullOrEmpty(shipto))
            {
                comp.Address1 = shipto;
            }



            return comp;

        }

        private NodeFileTransportJob CreateTransportJobFromRow(DataRow dataRow)
        {
            NodeFileTransportJob job = new NodeFileTransportJob();
            var box = FromMapping("BoxNo", dataRow);
            if (!string.IsNullOrEmpty(box))
            {
                job.BoxNo = box;
            }
            var so = FromMapping("SONo", dataRow);
            if (!string.IsNullOrEmpty(so))
            {
                job.SONo = so;

            }
            var jobno = FromMapping("JobNo", dataRow);
            if (!string.IsNullOrEmpty(jobno))
            {
                job.JobNo = jobno;

            }
            var barcode = FromMapping("BarCode", dataRow);
            if (!string.IsNullOrEmpty(barcode))
            {
                job.BarCode = barcode;
            }

            var client = FromMapping("ClientSort", dataRow);
            if (!string.IsNullOrEmpty(client))
            {
                job.ClientSort = client;
            }
            var factory = FromMapping("FactorySort", dataRow);
            if (!string.IsNullOrEmpty(factory))
            {
                job.FactorySort = factory;
            }
            var tracking = FromMapping("TrackingNo", dataRow);
            if (!string.IsNullOrEmpty(tracking))
            {
                job.TrackingNo = tracking;
            }
            var logo = FromMapping("Logo", dataRow);
            if (!string.IsNullOrEmpty(logo))
            {
                job.Logo = logo;
            }
            var remarks = FromMapping("Remarks", dataRow);
            if (!string.IsNullOrEmpty(remarks))
            {
                job.Remarks = remarks;
            }



            return job;

        }


        private NodeFileTrackingOrder CreateOrderHeaderFromRow(DataRow dataRow)
        {
            NodeFileTrackingOrder order = new NodeFileTrackingOrder();
            var incoterms = FromMapping("IncoTerms", dataRow);

            if (!string.IsNullOrEmpty(incoterms))
            {
                order.IncoTerms = incoterms;
            }

            order.Dates = GetDatesFromRow(dataRow).ToArray();
            List<CompanyElement> cmp = GetCompaniesFromRow(dataRow);
            if (cmp == null)
            {
                return null;
            }
            order.Companies = cmp.ToArray();
            var contmode = FromMapping("ContainerMode", dataRow);
            if (!string.IsNullOrEmpty(contmode))
            {
                order.ContainerMode = contmode;
            }
            var trnmode = FromMapping("TransportMode", dataRow);
            if (!string.IsNullOrEmpty(trnmode))
            {
                order.TransportMode = trnmode;
            }
            var ordno = FromMapping("OrderNumber", dataRow);
            order.OrderNo = !string.IsNullOrEmpty(ordno) ? ordno : string.Empty;




            return order;
        }

        private string GetContainerModeFromRow(string v)
        {
            throw new NotImplementedException();
        }

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow)
        {
            List<CompanyElement> companies = new List<CompanyElement>();
            var custField = FromMapping("Importer", dataRow);
            if (!string.IsNullOrEmpty(custField))
            {
                var comp = new CompanyElement
                {
                    CompanyOrgCode = custField,
                    CompanyType = CompanyElementCompanyType.Consignee
                };
                companies.Add(comp);
            }
            var suppField = FromMapping("Supplier", dataRow);
            if (!string.IsNullOrEmpty(suppField))
            {
                var comp = new CompanyElement
                {
                    CompanyOrgCode = suppField,
                    CompanyType = CompanyElementCompanyType.Consignor
                };
                companies.Add(comp);
            }




            return companies;
        }
        #endregion

        #region helpers
        private List<DateElement> GetDatesFromRow(DataRow dataRow)
        {
            List<DateElement> Dates = new List<DateElement>();
            DateElement date;
            int i = 0;
            string convertedDate = string.Empty;
            date = GetDateElement("OrderDate", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }

            date = new DateElement();
            date = GetDateElement("ConfirmDate", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }


            return Dates;


        }

        private DateElement GetDateElement(string fieldName, DataRow dataRow)
        {
            string dateFormat = "yyyy-MM-ddTHH:mm:ss";
            string convertedDate = FromMapping(fieldName, dataRow);
            DateTime dt;

            if (!string.IsNullOrEmpty(convertedDate))
            {

                if (DateTime.TryParse(convertedDate, out dt))
                {
                    convertedDate = dt.ToString(dateFormat);
                }
                else
                {
                    int i;
                    i = int.Parse(convertedDate);
                    convertedDate = DateTime.FromOADate(i).ToString(dateFormat);
                }
                var date = new DateElement();
                switch (fieldName)

                {
                    case "OrderDate":
                        date.DateType = DateElementDateType.OrderDate;
                        break;
                    case "ConfirmDate":
                        date.DateType = DateElementDateType.ConfirmDate;
                        break;

                };
                date.ActualDate = convertedDate;
                return date;
            }
            return null;


        }

        private string FromMapping(string fieldName, DataRow row)
        {
            var field = (from x in mapping
                         where x.MM_FieldName.ToString() == fieldName
                         select new { Data = x.MapFrom, DataType = x.MM_FieldType }).FirstOrDefault();

            if (field != null)
            {
                var data = field.DataType != "OVR" ? row[field.Data].ToString() : field.Data;
                return data;
            }
            return string.Empty;

        }

        private string FieldTypeFromMapping(string fieldName)
        {
            var field = (from x in mapping
                         where x.MM_FieldName.ToString() == fieldName
                         select x.MM_FieldType).FirstOrDefault();
            return field;
        }
        private Mapping ReturnStringFromList(List<MapOperation> map, Guid operation, string stringToFind)
        {

            var value = (from x in map
                         where x.MD_MH == operation
                         && x.MD_FromField == stringToFind
                         select new Mapping
                         {
                             MapFrom = x.MD_ToField,
                             MM_FieldName = x.MD_FromField,
                             MM_FieldType = x.MD_DataType
                         }).FirstOrDefault();
            if (value == null)
            {
                return new Mapping
                {
                    MapFrom = string.Empty,
                    MM_FieldName = string.Empty
                };
            }
            return value;
        }

        #endregion
    }


}
