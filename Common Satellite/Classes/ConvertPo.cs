﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Common_Satellite.Classes
{
    public class ConvertPo
    {
        #region members
        string _errorLog;
        string _fileName;
        string _connString;
        DataTable _table;
        List<Mapping> mapping;
        #endregion

        #region constructors
        public ConvertPo(DataTable tb, string connString)
        {
            ConnString = connString;
            _table = tb;
        }
        #endregion

        #region properties

        public string ConnString
        {
            get
            { return _connString; }
            set
            { _connString = value; }
        }
        public List<MapOperation> Map { get; set; }
        public string FileName
        {
            get
            { return _fileName; }
            set
            { _fileName = value; }
        }
        public string ErrorLog
        {
            get
            { return _errorLog; }
        }
        #endregion

        #region methods

        private void GetFieldNames(List<MapOperation> map, Guid mapHeaderId)
        {
            mapping = new List<Mapping>();
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderNumber"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Importer"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Supplier"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "EtaDate"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ExFactory"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "RequiredBy"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderDate"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "IncoTerms"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Currency"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Mode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ContainerMode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "LineNumber"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ProductCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Description"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrdQty"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "UnitPrice"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "UnitOfMeasure"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "LineTotal"));

        }
        public NodeFile CreateCommon(List<MapOperation> map, Guid mapHeaderId)
        {
            GetFieldNames(map, mapHeaderId);


            NodeFile common = new NodeFile();
            string currentOrderNo = string.Empty;
            string prevOrderNo = string.Empty;
            List<NodeFileTrackingOrder> orders = new List<NodeFileTrackingOrder>();
            int iLineNo = 0;
            NodeFileTrackingOrder order = new NodeFileTrackingOrder();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            for (int i = 0; i < _table.Rows.Count; i++)
            {
                var lineNo = FromMapping("LineNumber", _table.Rows[i]);
                if (!int.TryParse(lineNo, out iLineNo))
                {

                }
                if (iLineNo == 1)
                {
                    order = CreateOrderHeaderFromRow(_table.Rows[i]);
                    currentOrderNo = order.OrderNo;
                    orderLines = new List<OrderLineElement>();
                }
                OrderLineElement line = CreateOrderLineFromRow(_table.Rows[i]);
                orderLines.Add(line);
                order.OrderLines = orderLines.ToArray();
                if (i + 1 < _table.Rows.Count)
                {
                    var nextOrdNo = FromMapping("OrderNumber", _table.Rows[i + 1]);
                    if (nextOrdNo != currentOrderNo)
                    {
                        orders.Add(order);
                    }
                }
                else
                {
                    orders.Add(order);
                }


            }
            common.TrackingOrders = orders.ToArray();

            return common;
        }

        private OrderLineElement CreateOrderLineFromRow(DataRow dataRow)
        {
            OrderLineElement line = new OrderLineElement();
            var lineno = FromMapping("LineNumber", dataRow);
            if (!string.IsNullOrEmpty(lineno))
            {
                line.LineNo = int.Parse(lineno);
            }
            var ordQty = FromMapping("OrdQty", dataRow);
            if (!string.IsNullOrEmpty(ordQty))
            {
                line.OrderQty = decimal.Parse(ordQty);
                line.OrderQtySpecified = true;
            }
            var unitprice = FromMapping("UnitPrice", dataRow);
            if (!string.IsNullOrEmpty(unitprice))
            {
                line.UnitPrice = decimal.Parse(unitprice);
                line.UnitPriceSpecified = true;
            }
            var uom = FromMapping("UnitOfMeasure", dataRow);
            if (!string.IsNullOrEmpty(uom))
            {
                line.UnitOfMeasure = uom;
            }
            var linetot = FromMapping("LineTotal", dataRow);
            if (!string.IsNullOrEmpty(linetot))
            {
                line.LineTotal = decimal.Parse(linetot);
                line.LineTotalSpecified = true;
            }
            var pkgQty = FromMapping("PackageQty", dataRow);
            if (!string.IsNullOrEmpty(pkgQty))
            {
                line.PackageQty = decimal.Parse(pkgQty);
                line.PackageQtySpecified = true;
            }
            var pkgUnit = FromMapping("PackageUnit", dataRow);
            if (!string.IsNullOrEmpty(pkgUnit))
            {
                line.PackageUnit = pkgUnit;
            }
            var prodCode = FromMapping("ProductCode", dataRow);
            if (!string.IsNullOrEmpty(prodCode))
            {
                line.Product = new ProductElement();

                line.Product.Code = prodCode;
                var desc = FromMapping("Description", dataRow);
                if (!string.IsNullOrEmpty(desc))
                {
                    line.Product.Description = desc;
                }

            }
            return line;

        }

        private NodeFileTrackingOrder CreateOrderHeaderFromRow(DataRow dataRow)
        {
            NodeFileTrackingOrder order = new NodeFileTrackingOrder();
            var incoterms = FromMapping("IncoTerms", dataRow);

            if (!string.IsNullOrEmpty(incoterms))
            {
                order.IncoTerms = incoterms;
            }

            order.Dates = GetDatesFromRow(dataRow).ToArray();
            List<CompanyElement> cmp = GetCompaniesFromRow(dataRow);
            if (cmp == null)
            {
                return null;
            }
            order.Companies = cmp.ToArray();
            var contmode = FromMapping("ContainerMode", dataRow);
            if (!string.IsNullOrEmpty(contmode))
            {
                order.ContainerMode = contmode;
            }
            var trnmode = FromMapping("TransportMode", dataRow);
            if (!string.IsNullOrEmpty(trnmode))
            {
                order.TransportMode = trnmode;
            }
            var ordno = FromMapping("OrderNumber", dataRow);
            order.OrderNo = !string.IsNullOrEmpty(ordno) ? ordno : string.Empty;




            return order;
        }

        private string GetContainerModeFromRow(string v)
        {
            throw new NotImplementedException();
        }

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow)
        {
            List<CompanyElement> companies = new List<CompanyElement>();
            var custField = FromMapping("Importer", dataRow);
            if (!string.IsNullOrEmpty(custField))
            {
                var comp = new CompanyElement
                {
                    CompanyOrgCode = custField,
                    CompanyType = CompanyElementCompanyType.Consignee
                };
                companies.Add(comp);
            }
            var suppField = FromMapping("Supplier", dataRow);
            if (!string.IsNullOrEmpty(suppField))
            {
                var comp = new CompanyElement
                {
                    CompanyOrgCode = suppField,
                    CompanyType = CompanyElementCompanyType.Consignor
                };
                companies.Add(comp);
            }




            return companies;
        }
        #endregion

        #region helpers
        private List<DateElement> GetDatesFromRow(DataRow dataRow)
        {
            List<DateElement> Dates = new List<DateElement>();
            DateElement date;
            int i = 0;
            string convertedDate = string.Empty;
            date = GetDateElement("OrderDate", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }
            date = GetDateElement("EtaDate", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }
            date = GetDateElement("ExFactory", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }
            date = GetDateElement("RequiredBy", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }

            return Dates;


        }

        private DateElement GetDateElement(string fieldName, DataRow dataRow)
        {
            string dateFormat = "yyyy-MM-ddTHH:mm:ss";
            string convertedDate = FromMapping(fieldName, dataRow);
            DateTime dt;

            if (!string.IsNullOrEmpty(convertedDate))
            {

                if (DateTime.TryParse(convertedDate, out dt))
                {
                    convertedDate = dt.ToString(dateFormat);
                }
                else
                {
                    int i;
                    i = int.Parse(convertedDate);
                    convertedDate = DateTime.FromOADate(i).ToString(dateFormat);
                }
                var date = new DateElement();
                switch (fieldName)

                {
                    case "OrderDate":
                        date.DateType = DateElementDateType.Ordered;
                        break;
                    case "EtaDate":
                        date.DateType = DateElementDateType.Arrival;
                        break;
                    case "ExFactory":
                        date.DateType = DateElementDateType.ExFactory;
                        break;
                    case "RequiredBy":
                        date.DateType = DateElementDateType.Required;
                        break;

                };
                date.ActualDate = convertedDate;
                return date;
            }
            return null;


        }

        private string FromMapping(string fieldName, DataRow row)
        {
            var field = (from x in mapping
                         where x.MM_FieldName.ToString() == fieldName
                         select new { Data = x.MapFrom, DataType = x.MM_FieldType }).FirstOrDefault();

            if (field != null)
            {
                var data = field.DataType != "OVR" ? row[field.Data].ToString() : field.Data;
                return data;
            }
            return string.Empty;

        }

        private string FieldTypeFromMapping(string fieldName)
        {
            var field = (from x in mapping
                         where x.MM_FieldName.ToString() == fieldName
                         select x.MM_FieldType).FirstOrDefault();
            return field;
        }
        private Mapping ReturnStringFromList(List<MapOperation> map, Guid operation, string stringToFind)
        {

            var value = (from x in map
                         where x.MD_MH == operation
                         && x.MD_FromField == stringToFind
                         select new Mapping
                         {
                             MapFrom = x.MD_ToField,
                             MM_FieldName = x.MD_FromField,
                             MM_FieldType = x.MD_DataType
                         }).FirstOrDefault();
            if (value == null)
            {
                return new Mapping
                {
                    MapFrom = string.Empty,
                    MM_FieldName = string.Empty
                };
            }
            return value;
        }

        #endregion
    }

    class Mapping : MappingMasterField
    {
        public string MapFrom { get; set; }
    }
}
