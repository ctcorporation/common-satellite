﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Common_Satellite.Classes
{
    public class ConvertData
    {
        #region members
        string _connString;
        #endregion

        #region parameters
        public string ConnString
        {
            get { return _connString; }
            set { this._connString = value; }
        }
        #endregion

        #region constructors
        public ConvertData()
        {

        }
        public ConvertData(string connString)
        {
            ConnString = connString;

        }
        #endregion

        #region methods
        public  NodeFile CreateCommonOrgs(DataTable tb, string from, string to)
        {
            NodeFile common = new NodeFile();
            CompanyElement company = new CompanyElement();
            List<CompanyElement> addressColl = new List<CompanyElement>();
            foreach (DataRow rec in tb.Rows)
            {
                CompanyElement address = new CompanyElement();
                address.CompanyName = rec["CompanyName"].ToString();
                address.Address1 = rec["Address1"].ToString();
                address.Address2 = rec["Address2"].ToString();
                address.City = rec["City"].ToString();
                address.State = rec["State"].ToString();
                address.PostCode = rec["PostCode"].ToString();
                address.CompanyType = (CompanyElementCompanyType)Enum.Parse(typeof(CompanyElementCompanyType), rec["CompanyType"].ToString());
                addressColl.Add(address);
            }
            company.CompanyCode = to;
            company.AddressCollection = addressColl.ToArray();
            common.IdendtityMatrix = new NodeFileIdendtityMatrix
            {
                CustomerId = to,
                DocumentType = "ConvertCompany",
                FileDateTime = DateTime.Now.ToString("yyyy-MM-dd"),
                SenderId = from
            };
            List<CompanyElement> companyImports = new List<CompanyElement>();
            companyImports.Add(company);
            common.CompanyImport = companyImports.ToArray();
            return common;


        }
        #endregion

        #region helpers
        #endregion
    }
}
