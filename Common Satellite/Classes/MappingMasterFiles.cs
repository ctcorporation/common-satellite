﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common_Satellite.Classes
{
    public class MappingMasterFiles
    {
        #region members
        private string _connString;
        private string _errorLog;
        #endregion

        #region parameters
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        public string ErrorLog
        {
            get { return _errorLog; }
        }
        #endregion

        #region constructors
        public MappingMasterFiles(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region methods

        public int SaveOperation(MappingMasterOperation operation)
        {
            MappingMasterOperation op = new MappingMasterOperation();
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
                {

                    if (operation.MO_ID == Guid.Empty)
                    {

                        op.MO_OperationName = operation.MO_OperationName;
                        op.MO_Description = operation.MO_Description;
                        uow.MappingMasterOperations.Add(op);
                    }
                    else
                    {
                        op = uow.MappingMasterOperations.Get(operation.MO_ID);
                        if (op != null)
                        {
                            op.MO_OperationName = operation.MO_OperationName;
                            op.MO_Description = operation.MO_Description;
                        }
                    }

                    return uow.Complete();

                }
            }
            catch (Exception ex)
            {

            }
            return -1;

        }

        internal string GetOperationMethod(Guid selectedValue)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                return uow.MappingMasterOperations.Get(selectedValue).MD_MethodName;
            }
        }
        public int SaveFields(MappingMasterField mapfield)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                MappingMasterField newField;
                newField = uow.MappingMasterFields.Find(x => x.MM_FieldName.ToUpper() == mapfield.MM_FieldName.ToUpper()
                 && x.MM_MO == mapfield.MM_MO).FirstOrDefault();
                if (newField == null)
                {
                    newField = new MappingMasterField
                    {
                        MM_FieldName = mapfield.MM_FieldName,
                        MM_MO = mapfield.MM_MO
                    };
                    uow.MappingMasterFields.Add(newField);
                    var complete = uow.Complete();
                    if (complete != 0)
                    {
                        _errorLog = uow.ErrorMessage;

                    }

                    return complete;

                }
                else
                    return 1;

            }
            return -1;
        }

        internal void RemoveField(Guid fieldId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                uow.MappingMasterFields.Remove(uow.MappingMasterFields.Get(fieldId));
                uow.Complete();
            }
        }

        internal void RemoveOperation(Guid opid)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                uow.MapOperations.Remove(uow.MapOperations.Get(opid));
                uow.Complete();
            }


        }


        #endregion

        #region helpers
        public List<MappingMasterField> GetFields(Guid fieldId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                return uow.MappingMasterFields.Find(x => x.MM_MO == fieldId)
                    .OrderBy(o => o.MM_FieldName)
                    .ToList();
            }
        }
        public List<KeyValuePair<Guid, String>> GetOperations()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                return uow.MappingMasterOperations.GetAll().Distinct().OrderBy(x => x.MO_OperationName)
                    .ToDictionary(o => o.MO_ID, o => o.MO_OperationName).ToList();
            }
        }

        public List<KeyValuePair<String, String>> GetJobTypes()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                return uow.CargowiseEnums.Find(x => x.CW_ENUMTYPE == "JOBTYPE").Distinct().OrderBy(x => x.CW_MAPVALUE)
                    .ToDictionary(o => o.CW_ENUM, o => o.CW_MAPVALUE).ToList();
            }
        }

        public List<KeyValuePair<Guid, string>> GetMapHeaders(Guid opID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                return uow.MapHeaders.Find(x => x.MH_MO == opID)
                       .ToDictionary(d => d.MH_ID, d => d.MH_Name)
                       .ToList();
            }

        }

        public MapHeader AddMapHeader(MapHeader mapheader)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                uow.MapHeaders.Add(mapheader);
                uow.Complete();
                return mapheader;
            }
        }
        public void AddMapOperation(MapHeader mapHeader, List<MapOperation> map)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                var newMapHeader = uow.MapHeaders.Get(mapHeader.MH_ID);
                newMapHeader.MH_Customer = mapHeader.MH_Customer;
                newMapHeader.MH_Description = mapHeader.MH_Description;
                newMapHeader.MH_Name = mapHeader.MH_Name;
                newMapHeader.MH_Recipient = mapHeader.MH_Recipient;
                uow.MapOperations.RemoveRange(uow.MapOperations.Find(x => x.MD_MH == mapHeader.MH_ID));
                foreach (var operation in map)
                {
                    operation.MD_MH = mapHeader.MH_ID;
                    uow.MapOperations.Add(operation);

                }
                uow.Complete();
            }
        }
        public List<MapOperation> GetMap(Guid mapID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                return uow.MapOperations.Find(x => x.MD_MH == mapID).ToList();
            }

        }

        internal MapHeader GetMapHeader(Guid selectedValue)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                return uow.MapHeaders.Get(selectedValue);
            }
        }
        internal MapHeader GetMapHeaderByName(string name)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                return (uow.MapHeaders.Find(x => x.MH_Name.ToLower() == name.ToLower())).FirstOrDefault();
            }
        }

        internal void RemoveMaps(List<MapOperation> map)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                uow.MapOperations.RemoveRange(map);
                uow.Complete();
            }
        }
        #endregion

    }
}
