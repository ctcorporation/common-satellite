﻿namespace Common_Satellite.Classes
{
    public class TransModOperations
    {
        #region members
        string _connString;
        string _errMsg;
        #endregion

        #region properties
        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }
        public string ErrorMessage
        {
            get
            {
                return _errMsg;
            }
            set
            {
                _errMsg = value;
            }
        }
        #endregion

        #region constructors
        public TransModOperations(string connString)
        {
            ConnString = connString;
        }
        #endregion



    }
}
