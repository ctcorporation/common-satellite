﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Common_Satellite.Classes
{
    public class ConvertOrder
    {

        #region members
        string _errorLog;
        string _fileName;
        string _connString;
        DataTable _table;
        List<Mapping> mapping;
        #endregion

        #region constructors
        public ConvertOrder(DataTable tb, string connString)
        {
            ConnString = connString;
            _table = tb;
        }
        #endregion

        #region properties

        public string ConnString
        {
            get
            { return _connString; }
            set
            { _connString = value; }
        }
        public List<MapOperation> Map { get; set; }
        public string FileName
        {
            get
            { return _fileName; }
            set
            { _fileName = value; }
        }
        public string ErrorLog
        {
            get
            { return _errorLog; }
        }
        #endregion

        #region methods

        private void GetFieldNames(List<MapOperation> map, Guid mapHeaderId)
        {
            mapping = new List<Mapping>();
            var mapOperationMasterList = GetMasterFieldList(mapHeaderId);
            foreach (var mapfield in mapOperationMasterList)
            {
                mapping.Add(ReturnStringFromList(map, mapHeaderId, mapfield.MM_FieldName.Trim()));
            }
        }

        private void GetFields(List<MapOperation> map, Guid mapHeaderId)
        {
            mapping = new List<Mapping>();
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderNumber"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Customer"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "DeliverAddress1"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "DeliverAddress2"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "DeliverSuburb"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "DeliverState"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "DeliverPostCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ProductCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ProductDescription"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "SpecialInstructions"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderQty"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "PackageQty"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Warehouse"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "Batch"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "OrderDate"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "EtaDate"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BillName"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ShipName"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "ShipVia"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "UnitPrice"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "UOM"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BillAddress1"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BillAddress2"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BillState"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BillSuburb"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "BillPostCode"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "TotalAmount"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "CustomerReference"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "FreightInstructions"));
            mapping.Add(ReturnStringFromList(map, mapHeaderId, "FreightType"));

        }

        private List<MappingMasterField> GetMasterFieldList(Guid mapHeaderId)
        {
            MappingMasterFiles mm = new MappingMasterFiles(_connString);
            return mm.GetFields(mapHeaderId);
        }

        public NodeFile CreateCommon(List<MapOperation> map, Guid mapHeaderId)
        {
            GetFieldNames(map, mapHeaderId);
            NodeFile common = new NodeFile();
            string currentOrderNo = string.Empty;
            string prevOrderNo = string.Empty;
            List<NodeFileWarehouseOrder> orders = new List<NodeFileWarehouseOrder>();
            int iLineNo = 0;
            NodeFileWarehouseOrder order = new NodeFileWarehouseOrder();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            for (int i = 0; i < _table.Rows.Count; i++)
            {
                var lineNo = FromMapping("LineNumber", _table.Rows[i]);
                if (!int.TryParse(lineNo, out iLineNo))
                {

                }
                if (iLineNo == 1)
                {
                    order = CreateOrderHeaderFromRow(_table.Rows[i]);
                    currentOrderNo = order.OrderReference;
                    orderLines = new List<OrderLineElement>();
                }
                OrderLineElement line = CreateOrderLineFromRow(_table.Rows[i]);
                orderLines.Add(line);
                order.OrderLines = orderLines.ToArray();
                if (i + 1 < _table.Rows.Count)
                {
                    var nextOrdNo = FromMapping("OrderNumber", _table.Rows[i + 1]);
                    if (nextOrdNo != currentOrderNo)
                    {
                        orders.Add(order);
                    }
                }
                else
                {
                    orders.Add(order);
                }
            }
            common.WarehouseOrders = orders.ToArray();
            return common;
        }


        public NodeFile CreateCommonOrder(List<MapOperation> map, Guid mapHeaderId)
        {
            GetFields(map, mapHeaderId);
            NodeFile common = new NodeFile();
            string currentOrderNo = string.Empty;
            string prevOrderNo = string.Empty;
            List<NodeFileWarehouseOrder> orders = new List<NodeFileWarehouseOrder>();
            int iLineNo = 0;
            NodeFileWarehouseOrder order = new NodeFileWarehouseOrder();
            CompanyElement company = new CompanyElement();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            List<CompanyElement> companies = new List<CompanyElement>();
            List<CompanyElement> companies2 = new List<CompanyElement>();
            for (int i = 0; i < _table.Rows.Count; i++)
            {
                iLineNo += 1;

                order.OrderNumber = FromMapping("OrderNumber", _table.Rows[i]);
                currentOrderNo = FromMapping("OrderNumber", _table.Rows[i]);

                if (iLineNo == 1)
                {
                    order = CreateOrderHeaderFromRow(_table.Rows[i]);
                }

                List<DateElement> dates = GetDatesFromRow(_table.Rows[i]);

                order.Dates = dates.ToArray();

                OrderLineElement line = CreateOrderLineFromRow(_table.Rows[i]);
                line.LineNo = iLineNo;
                line.OrderQtyUnit = "Bag";
                line.OrderQtyUnitCode = "BAG";
                orderLines.Add(line);
                order.OrderLines = orderLines.ToArray();

                //var specialInstructions = FromMapping("SpecialInstructions", _table.Rows[i]);
                //order.SpecialInstructions = !string.IsNullOrEmpty(specialInstructions) ? specialInstructions : string.Empty;

                company = CreateCompanyFromRow(_table.Rows[i]);
                companies.Add(company);
                if (i + 1 < _table.Rows.Count)
                {
                    var nextOrdNo = FromMapping("OrderNumber", _table.Rows[i + 1]);
                    if (nextOrdNo != currentOrderNo)
                    {
                        companies2 = CreateCompanyOrderFromRow(_table.Rows[i]);
                        companies2.AddRange(companies);
                        order.Companies = companies2.ToArray();
                        orders.Add(order);

                        order = new NodeFileWarehouseOrder();
                    }
                }
                else
                {
                    companies2 = CreateCompanyOrderFromRow(_table.Rows[i]);
                    companies2.AddRange(companies);
                    order.Companies = companies2.ToArray();
                    orders.Add(order);
                }
            }
            common.WarehouseOrders = orders.ToArray();
            return common;
        }

        public NodeFile CreateWarehouseOrders(List<MapOperation> map, Guid mapHeaderId)
        {
            GetFields(map, mapHeaderId);
            NodeFile common = new NodeFile();
            string currentOrderNo = string.Empty;
            string prevOrderNo = string.Empty;
            List<NodeFileWarehouseOrder> orders = new List<NodeFileWarehouseOrder>();
            int iLineNo = 0;
            NodeFileWarehouseOrder order = new NodeFileWarehouseOrder();
            CompanyElement company = new CompanyElement();
            NoteElement note = new NoteElement();
            OrderLineElement orderLine = new OrderLineElement();
            DateElement date = new DateElement();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            List<CompanyElement> companies = new List<CompanyElement>();
            List<NoteElement> notes = new List<NoteElement>();
            List<DateElement> dates = new List<DateElement>();
            string internalId = FromMapping("Warehouse", _table.Rows[0]);


            for (int i = 0; i < _table.Rows.Count; i++)
            {
                if (i == 0 || FromMapping("OrderNumber", _table.Rows[i]) != FromMapping("OrderNumber", _table.Rows[i - 1]))
                {
                    iLineNo = 1;

                    order = new NodeFileWarehouseOrder();

                    order.OrderNumber = FromMapping("OrderNumber", _table.Rows[i]);
                    order.ServiceLevel = FromMapping("FreightType", _table.Rows[i]);
                    order.OrderReference = FromMapping("CustomerReference", _table.Rows[i]);
                    order.WarehouseCode = "SYD";

                    notes = new List<NoteElement>();

                    note = new NoteElement()
                    {
                        InstructionType = "Picking Instructions",
                        InstructionText = FromMapping("SpecialInstructions", _table.Rows[i])
                    };

                    notes.Add(note);

                    note = new NoteElement()
                    {
                        InstructionType = "Goods Handling Instructions",
                        InstructionText = FromMapping("FreightInstructions", _table.Rows[i])
                    };

                    notes.Add(note);

                    order.Instructions = notes.ToArray();
                    order.TransportProvider = FromMapping("ShipVia", _table.Rows[i]);

                    companies = new List<CompanyElement>();
                    company = new CompanyElement();

                    company.CompanyType = CompanyElementCompanyType.DeliveryAddress;
                    company.AddressOverride = "false";
                    company.CompanyName = FromMapping("ShipName", _table.Rows[i]);
                    company.Address1 = FromMapping("DeliverAddress1", _table.Rows[i]);
                    company.Address2 = FromMapping("DeliverAddress2", _table.Rows[i]);
                    company.State = FromMapping("DeliverState", _table.Rows[i]);
                    company.City = FromMapping("DeliverSuburb", _table.Rows[i]);
                    company.PostCode = FromMapping("DeliverPostCode", _table.Rows[i]);
                    company.Country = "AU";
                    companies.Add(company);

                    company = new CompanyElement();

                    company.CompanyType = CompanyElementCompanyType.Consignor;
                    //company.CompanyName = FromMapping("BillName", _table.Rows[i]);

                    //company.Address1 = FromMapping("BillAddress1", _table.Rows[i]);
                    //company.Address2 = FromMapping("BillAddress2", _table.Rows[i]);
                    //company.State = FromMapping("BillState", _table.Rows[i]);
                    //company.City = FromMapping("BillSuburb", _table.Rows[i]);
                    //company.PostCode = FromMapping("BillPostCode", _table.Rows[i]);
                    //company.Country = "AU";
                    companies.Add(company);

                    order.Companies = companies.ToArray();

                    orderLines = new List<OrderLineElement>();
                    orderLine = new OrderLineElement();

                    orderLine.LineNo = iLineNo;
                    orderLine.Product = new ProductElement()
                    {
                        Code = FromMapping("ProductCode", _table.Rows[i])
                    };

                    orderLine.OrderQty = Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    order.TotalUnits += Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    //orderLine.UnitPrice = Convert.ToDecimal(FromMapping("UnitPrice", _table.Rows[i]));
                    orderLine.OrderQtyUnitCode = FromMapping("UOM", _table.Rows[i]);
                    orderLine.PackageUnit = FromMapping("UOM", _table.Rows[i]);
                    orderLine.LineTotal = Convert.ToDecimal(FromMapping("TotalAmount", _table.Rows[i]));


                    orderLines.Add(orderLine);
                    order.OrderLines = orderLines.ToArray();


                    dates = new List<DateElement>();
                    //date = new DateElement();

                    //date.DateType = DateElementDateType.Ordered;
                    //date.ActualDate = FromMapping("OrderDate", _table.Rows[i]);

                    //dates.Add(date);

                    date = new DateElement();

                    date.DateType = DateElementDateType.Required;
                    date.ActualDate = FromMapping("EtaDate", _table.Rows[i]);

                    dates.Add(date);

                    order.Dates = dates.ToArray();
                }
                else
                {
                    orderLine = new OrderLineElement();
                    orderLine.LineNo = iLineNo += 1;
                    orderLine.Product = new ProductElement()
                    {
                        Code = FromMapping("ProductCode", _table.Rows[i])
                    };

                    orderLine.OrderQty = Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    order.TotalUnits += Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    //orderLine.UnitPrice = Convert.ToDecimal(FromMapping("UnitPrice", _table.Rows[i]));
                    orderLine.OrderQtyUnitCode = FromMapping("UOM", _table.Rows[i]);
                    orderLine.PackageUnit = FromMapping("UOM", _table.Rows[i]);
                    orderLine.LineTotal = Convert.ToDecimal(FromMapping("TotalAmount", _table.Rows[i]));

                    orderLines.Add(orderLine);

                    if (i < _table.Rows.Count - 1)
                    {
                        if (FromMapping("OrderNumber", _table.Rows[i]) != FromMapping("OrderNumber", _table.Rows[i + 1]))
                        {
                            order.OrderLines = orderLines.ToArray();
                        }
                    }
                    else
                    {
                        order.OrderLines = orderLines.ToArray();
                    }

                    continue;
                }

                order.PickOption = "AUT";
                orders.Add(order);

            }
            common.WarehouseOrders = orders.ToArray();
            return common;
        }

        public NodeFile CreateWarehouseOrders(List<MapOperation> map, Guid mapHeaderId, string senderId)
        {
            GetFields(map, mapHeaderId);
            NodeFile common = new NodeFile();
            string currentOrderNo = string.Empty;
            string prevOrderNo = string.Empty;
            List<NodeFileWarehouseOrder> orders = new List<NodeFileWarehouseOrder>();
            int iLineNo = 0;
            NodeFileWarehouseOrder order = new NodeFileWarehouseOrder();
            CompanyElement company = new CompanyElement();
            NoteElement note = new NoteElement();
            OrderLineElement orderLine = new OrderLineElement();
            DateElement date = new DateElement();
            List<OrderLineElement> orderLines = new List<OrderLineElement>();
            List<CompanyElement> companies = new List<CompanyElement>();
            List<NoteElement> notes = new List<NoteElement>();
            List<DateElement> dates = new List<DateElement>();
            string internalId = FromMapping("Warehouse", _table.Rows[0]);


            for (int i = 0; i < _table.Rows.Count; i++)
            {
                if (i == 0 || FromMapping("OrderNumber", _table.Rows[i]) != FromMapping("OrderNumber", _table.Rows[i - 1]))
                {
                    iLineNo = 1;

                    order = new NodeFileWarehouseOrder();

                    order.OrderNumber = FromMapping("OrderNumber", _table.Rows[i]);
                    order.ServiceLevel = FromMapping("FreightType", _table.Rows[i]);
                    order.OrderReference = FromMapping("CustomerReference", _table.Rows[i]);
                    order.WarehouseCode = "SYD";

                    notes = new List<NoteElement>();

                    if(!string.IsNullOrEmpty(FromMapping("SpecialInstructions", _table.Rows[i])))
                    {
                        note = new NoteElement()
                        {
                            InstructionType = "Picking Instructions",
                            InstructionText = FromMapping("SpecialInstructions", _table.Rows[i])
                        };

                        notes.Add(note);
                    }
                    
                    if(!string.IsNullOrEmpty(FromMapping("FreightInstructions", _table.Rows[i])))
                    {
                        note = new NoteElement()
                        {
                            InstructionType = "Goods Handling Instructions",
                            InstructionText = FromMapping("FreightInstructions", _table.Rows[i])
                        };

                        notes.Add(note);
                    }

                    

                    order.Instructions = notes.ToArray();
                    order.TransportProvider = FromMapping("ShipVia", _table.Rows[i]);

                    companies = new List<CompanyElement>();
                    company = new CompanyElement();

                    company.CompanyType = CompanyElementCompanyType.DeliveryAddress;
                    company.AddressOverride = "false";
                    company.CompanyName = FromMapping("ShipName", _table.Rows[i]);
                    company.Address1 = FromMapping("DeliverAddress1", _table.Rows[i]);
                    company.Address2 = FromMapping("DeliverAddress2", _table.Rows[i]);
                    company.State = FromMapping("DeliverState", _table.Rows[i]);
                    company.City = FromMapping("DeliverSuburb", _table.Rows[i]);
                    company.PostCode = FromMapping("DeliverPostCode", _table.Rows[i]);
                    company.Country = "AU";
                    companies.Add(company);

                    company = new CompanyElement();

                    company.CompanyOrgCode = senderId;
                    company.CompanyType = CompanyElementCompanyType.Consignor;

                    companies.Add(company);

                    order.Companies = companies.ToArray();

                    orderLines = new List<OrderLineElement>();
                    orderLine = new OrderLineElement();

                    orderLine.LineNo = iLineNo;
                    orderLine.Product = new ProductElement()
                    {
                        Code = FromMapping("ProductCode", _table.Rows[i])
                    };

                    orderLine.OrderQty = Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    order.TotalUnits += Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    //orderLine.UnitPrice = Convert.ToDecimal(FromMapping("UnitPrice", _table.Rows[i]));
                    orderLine.OrderQtyUnitCode = FromMapping("UOM", _table.Rows[i]);
                    orderLine.PackageUnit = FromMapping("UOM", _table.Rows[i]);
                    orderLine.LineTotal = Convert.ToDecimal(FromMapping("TotalAmount", _table.Rows[i]));


                    orderLines.Add(orderLine);
                    order.OrderLines = orderLines.ToArray();


                    dates = new List<DateElement>();
                    //date = new DateElement();

                    //date.DateType = DateElementDateType.Ordered;
                    //date.ActualDate = FromMapping("OrderDate", _table.Rows[i]);

                    //dates.Add(date);

                    date = new DateElement();

                    date.DateType = DateElementDateType.Required;
                    date.ActualDate = DateTime.Now.ToString();

                    dates.Add(date);

                    order.Dates = dates.ToArray();
                }
                else
                {
                    orderLine = new OrderLineElement();
                    orderLine.LineNo = iLineNo += 1;
                    orderLine.Product = new ProductElement()
                    {
                        Code = FromMapping("ProductCode", _table.Rows[i])
                    };

                    orderLine.OrderQty = Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    order.TotalUnits += Convert.ToDecimal(FromMapping("OrderQty", _table.Rows[i]));
                    //orderLine.UnitPrice = Convert.ToDecimal(FromMapping("UnitPrice", _table.Rows[i]));
                    orderLine.OrderQtyUnitCode = FromMapping("UOM", _table.Rows[i]);
                    orderLine.PackageUnit = FromMapping("UOM", _table.Rows[i]);
                    orderLine.LineTotal = Convert.ToDecimal(FromMapping("TotalAmount", _table.Rows[i]));

                    orderLines.Add(orderLine);

                    if (i < _table.Rows.Count - 1)
                    {
                        if (FromMapping("OrderNumber", _table.Rows[i]) != FromMapping("OrderNumber", _table.Rows[i + 1]))
                        {
                            order.OrderLines = orderLines.ToArray();
                        }
                    }
                    else
                    {
                        order.OrderLines = orderLines.ToArray();
                    }

                    continue;
                }

                order.PickOption = "AUT";
                orders.Add(order);

            }
            common.WarehouseOrders = orders.ToArray();
            return common;
        }

        private string TruncateStr(string desc, int maxVal)
        {
            string truncStr = "";

            if(desc.Length <= maxVal)
            {
                truncStr = desc;
            }
            else
            {
                truncStr = desc.Substring(0, maxVal);
            }

            return truncStr;
        }


        private List<CompanyElement> CreateCompanyOrderFromRow(DataRow dataRow)
        {
            List<CompanyElement> comps = new List<CompanyElement>();

            CompanyElement comp = new CompanyElement();

            comp.CompanyType = CompanyElementCompanyType.Consignor;
            comp.Address1 = "PO BOX 5018";
            comp.City = "ELANORA HEIGHTS";
            comp.CompanyName = "PHYTO THERAPY";
            comp.CompanyOrgCode = "PHYTHESYD";
            comp.Country = "Australia";
            comp.CountryCode = "AU";
            comp.PhoneNo = "+61299132100";
            comp.Port = "Sydney";
            comp.PostCode = "2101";
            comp.State = "NSW";

            comps.Add(comp);

            comp = new CompanyElement();

            comp.CompanyType = CompanyElementCompanyType.PickupAddress;
            comp.Address1 = "UNIT 10 1A HALE STREET";
            comp.City = "BOTANY";
            comp.CompanyName = "Paul's Customs &amp; Forwarding Solutions P/L";
            comp.Country = "Australia";
            comp.CountryCode = "AU";
            comp.CompanyOrgCode = "PAUCUSSYD";
            comp.PostCode = "2019";
            comp.State = "NSW";

            comps.Add(comp);
            return comps;

        }


        private CompanyElement CreateCompanyFromRow(DataRow dataRow)
        {


            CompanyElement comp = new CompanyElement();
            comp.CompanyType = CompanyElementCompanyType.DeliveryAddress;
            var name = FromMapping("Customer", dataRow);
            if (!string.IsNullOrEmpty(name))
            {
                comp.CompanyName = name;
            }
            var state = FromMapping("DeliverState", dataRow);
            if (!string.IsNullOrEmpty(state))
            {
                comp.State = state;
            }
            var city = FromMapping("DeliverSuburb", dataRow);
            if (!string.IsNullOrEmpty(city))
            {
                comp.City = city;
            }
            var zip = FromMapping("DeliverPostCode", dataRow);
            if (!string.IsNullOrEmpty(zip))
            {
                comp.PostCode = zip;
            }
            var shipto = FromMapping("DeliverAddress1", dataRow);
            if (!string.IsNullOrEmpty(shipto))
            {
                comp.Address1 = shipto;
            }



            return comp;

        }

        private OrderLineElement CreateOrderLineFromRow(DataRow dataRow)
        {
            OrderLineElement line = new OrderLineElement();
            ProductElement product = new ProductElement();
            var lineno = FromMapping("LineNumber", dataRow);
            if (!string.IsNullOrEmpty(lineno))
            {
                int iL;
                if (int.TryParse(lineno, out iL))
                {
                    line.LineNo = iL;
                }

            }
            var ordQty = FromMapping("OrderQty", dataRow);
            if (!string.IsNullOrEmpty(ordQty))
            {
                decimal d;
                if (decimal.TryParse(ordQty, out d))
                {
                    line.OrderQty = d;
                    line.OrderQtySpecified = true;
                }
            }
            var unitprice = FromMapping("UnitPrice", dataRow);
            if (!string.IsNullOrEmpty(unitprice))
            {
                decimal p;
                if (decimal.TryParse(unitprice, out p))
                {
                    line.UnitPrice = p;
                    line.UnitPriceSpecified = true;
                }
            }
            var uom = FromMapping("UnitOfMeasure", dataRow);
            if (!string.IsNullOrEmpty(uom))
            {
                line.UnitOfMeasure = uom;
            }
            var linetot = FromMapping("LineTotal", dataRow);
            if (!string.IsNullOrEmpty(linetot))
            {
                line.LineTotal = decimal.Parse(linetot);
                line.LineTotalSpecified = true;
            }
            var pkgQty = FromMapping("PackageQty", dataRow);
            if (!string.IsNullOrEmpty(pkgQty))
            {
                line.PackageQty = decimal.Parse(pkgQty);
                line.PackageQtySpecified = true;
            }
            var pkgUnit = FromMapping("PackageUnit", dataRow);
            if (!string.IsNullOrEmpty(pkgUnit))
            {
                line.PackageUnit = pkgUnit;
            }

            var batch = FromMapping("Batch", dataRow);
            if (!string.IsNullOrEmpty(batch))
            {
                line.Batch = batch;
            }

            var prodCode = FromMapping("ProductCode", dataRow);
            if (!string.IsNullOrEmpty(prodCode))
            {
                line.Product = new ProductElement();
                var barcode = FromMapping("ProductBarcode", dataRow);
                line.Product.Barcode = !string.IsNullOrEmpty(barcode) ? barcode : string.Empty;
                var prodcode = FromMapping("ProducCode", dataRow);
                line.Product.Code = prodCode;
                var desc = FromMapping("Description", dataRow);
                line.Product.Description = !string.IsNullOrEmpty(desc) ? desc : string.Empty;

            }
            return line;

        }

        private NodeFileWarehouseOrder CreateOrderHeaderFromRow(DataRow dataRow)
        {
            NodeFileWarehouseOrder order = new NodeFileWarehouseOrder();
            var whouse = FromMapping("Warehouse", dataRow);
            order.WarehouseCode = !string.IsNullOrEmpty(whouse) ? whouse : string.Empty;
            order.Dates = GetDatesFromRow(dataRow).ToArray();
            List<CompanyElement> cmp = GetCompaniesFromRow(dataRow);
            if (cmp == null)
            {
                return null;
            }
            order.Companies = cmp.ToArray();
            var specialInstructions = FromMapping("SpecialInstructions", dataRow);
            order.SpecialInstructions = !string.IsNullOrEmpty(specialInstructions) ? specialInstructions : string.Empty;
            var ordno = FromMapping("OrderNumber", dataRow);
            order.OrderNumber = !string.IsNullOrEmpty(ordno) ? ordno : string.Empty;
            var orderRef = FromMapping("CustomerReference", dataRow);
            order.OrderReference = !string.IsNullOrEmpty(orderRef) ? orderRef : string.Empty;

            return order;
        }

        private string GetContainerModeFromRow(string v)
        {
            throw new NotImplementedException();
        }

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow)
        {
            List<CompanyElement> companies = new List<CompanyElement>();
            var custField = FromMapping("Customer", dataRow);
            if (!string.IsNullOrEmpty(custField))
            {
                var comp = new CompanyElement
                {
                    CompanyOrgCode = custField,
                    CompanyType = CompanyElementCompanyType.Customer
                };
                companies.Add(comp);
            }
            var suppField = FromMapping("DeliverTo", dataRow);
            if (!string.IsNullOrEmpty(suppField))
            {
                var comp = new CompanyElement
                {
                    CompanyOrgCode = suppField,
                    CompanyType = CompanyElementCompanyType.DeliveryAddress
                };
                companies.Add(comp);
            }




            return companies;
        }
        #endregion

        #region helpers
        private List<DateElement> GetDatesFromRow(DataRow dataRow)
        {
            List<DateElement> Dates = new List<DateElement>();
            DateElement date;
            int i = 0;
            string convertedDate = string.Empty;
            date = GetDateElement("OrderDate", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }
            date = GetDateElement("EtaDate", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }
            date = GetDateElement("ExFactory", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }
            date = GetDateElement("RequiredBy", dataRow);
            if (date != null)
            {
                Dates.Add(date);
            }

            return Dates;


        }

        private DateElement GetDateElement(string fieldName, DataRow dataRow)
        {
            string dateFormat = "yyyy-MM-ddTHH:mm:ss";
            string convertedDate = FromMapping(fieldName, dataRow);
            DateTime dt;

            if (!string.IsNullOrEmpty(convertedDate))
            {

                if (DateTime.TryParse(convertedDate, out dt))
                {
                    convertedDate = dt.ToString(dateFormat);
                }
                else
                {
                    int i;
                    i = int.Parse(convertedDate);
                    convertedDate = DateTime.FromOADate(i).ToString(dateFormat);
                }
                var date = new DateElement();
                switch (fieldName)

                {
                    case "OrderDate":
                        date.DateType = DateElementDateType.Ordered;
                        break;
                    case "EtaDate":
                        date.DateType = DateElementDateType.Arrival;
                        break;
                    case "ExFactory":
                        date.DateType = DateElementDateType.ExFactory;
                        break;
                    case "RequiredBy":
                        date.DateType = DateElementDateType.Required;
                        break;

                };
                date.ActualDate = convertedDate;
                return date;
            }
            return null;


        }

        private string FromMapping(string fieldName, DataRow row)
        {
            var field = (from x in mapping
                         where x.MM_FieldName.ToString() == fieldName
                         select new { Data = x.MapFrom, DataType = x.MM_FieldType }).FirstOrDefault();

            if (field != null)
            {
                var data = field.DataType != "OVR" ? row[field.Data].ToString() : field.Data;
                return data;
            }
            return string.Empty;

        }

        private string FieldTypeFromMapping(string fieldName)
        {
            var field = (from x in mapping
                         where x.MM_FieldName.ToString() == fieldName
                         select x.MM_FieldType).FirstOrDefault();
            return field;
        }
        private Mapping ReturnStringFromList(List<MapOperation> map, Guid operation, string stringToFind)
        {

            var value = (from x in map
                         where x.MD_MH == operation
                         && x.MD_FromField == stringToFind
                         select new Mapping
                         {
                             MapFrom = x.MD_ToField,
                             MM_FieldName = x.MD_FromField,
                             MM_FieldType = x.MD_DataType
                         }).FirstOrDefault();
            if (value == null)
            {
                return new Mapping
                {
                    MapFrom = string.Empty,
                    MM_FieldName = string.Empty
                };
            }
            return value;
        }

        #endregion
    }


}


