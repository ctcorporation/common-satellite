﻿using NodeData.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using XMLLocker.Cargowise.XUS;
using XMLLocker.CTC;

namespace Common_Satellite.Classes
{
    public class OrderReceipt
    {
        #region Members
        string _senderID;
        string _receipientID;
        string _outputPath;
        DataTable _dt;
        const string err = "CRITICAL.Error";
        #endregion
        private List<MapOperation> _map;
        private string _errSummary = string.Empty;
        private string _connString;
        private MailServerSettings _mailSettings;

        #region Properties
        public string SenderId
        {
            get
            { return _senderID; }
            set
            {
                _senderID = value;
            }
        }

        public DataTable CustData
        {
            get
            { return _dt; }
            set
            {
                _dt = value;
            }

        }

        public List<MapOperation> Map
        {
            get
            {
                return _map;
            }
            set
            {
                _map = value;
            }
        }
        public MailServerSettings MailSettings
        {
            get
            {
                return _mailSettings;
            }
            set
            {
                _mailSettings = value;
            }
        }

        public string ErrorMsg
        {
            get
            {
                return _errSummary;
            }
            set
            {
                _errSummary = value;
            }
        }

        public string OutputPath

        {
            get
            {
                return _outputPath;
            }
            set
            {
                _outputPath = value;
            }
        }


        public string ConnnString
        {
            get
            { return _connString; }
            set
            {
                _connString = value;
            }
        }
        public string LogPath
        {
            get;
        }

        public string RecipientId
        {
            get
            { return _receipientID; }
            set
            {
                _receipientID = value;
            }

        }
        #endregion

        #region Constructors
        public OrderReceipt(string connString, string senderID, string recipientid, string logPath)
        {
            _connString = connString;
            SenderId = senderID;
            RecipientId = recipientid;
            LogPath = logPath;

        }

        #endregion
        public OrderReceipt(string connString, DataTable tb, List<MapOperation> map)
        {
            _map = map;
            CustData = tb;
            _connString = connString;
        }

        #region Methods

        public NodeFile AddReceiptOrder(DataTable tb)
        {
            MethodBase m = MethodBase.GetCurrentMethod();
            string lo = LogPath;
            string cp = string.Empty;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            string conversionResult = string.Empty;
            NodeFile nodeFile = new NodeFile();
            NodeFileIdendtityMatrix id = new NodeFileIdendtityMatrix
            {
                CustomerId = RecipientId,
                SenderId = SenderId,
                DocumentType = "WarehouseReceipt"
            };
            nodeFile.IdendtityMatrix = id;
            NodeFileWarehouseReceipt receipt = new NodeFileWarehouseReceipt();

            return nodeFile;
        }

        public void ImportPO(DataTable tb, string refPO, string senderCode, string recipCode)
        {
            //SqlDataAdapter daPo = new SqlDataAdapter("SELECT PO_ID, PO_POID, PO_ETA, PO_POCODE, PL_Code, PL_QTY, PL_UOM, PL_Article, PW_PARTNUM, PW_PRODUCTNAME, PW_CWUOM " +
            //                                    "FROM[PCFS-WSF-PurchaseOrder] AS po INNER JOIN " +
            //                                    "[PCFS-WSF-PurchaseLine] AS pol ON po.PO_ID = pol.PL_PO INNER JOIN " +
            //                                    "[PCFS-WSF-Products] AS prod ON pol.PL_Article = prod.PW_ARTICLEID " +
            //                                    "WHERE PO_ID = '" + poid + "'", sqlConn);
            //DataSet dsPo = new DataSet();
            //daPo.Fill(dsPo, "PO");
            //if (dsPo.Tables["PO"].Rows.Count > 0)
            {

                XMLLocker.Cargowise.XUS.UniversalInterchange uInt = new UniversalInterchange();
                UniversalInterchangeHeader uHeader = new UniversalInterchangeHeader();
                uHeader.RecipientID = recipCode;
                uHeader.SenderID = senderCode;
                uInt.Header = uHeader;
                UniversalInterchangeBody uBody = new UniversalInterchangeBody();
                UniversalShipmentData uShipData = new UniversalShipmentData();
                Shipment shipment = new Shipment();
                DataContext dc = new DataContext();
                List<DataTarget> dataTargetColl = new List<DataTarget>();
                DataTarget dataTarget = new DataTarget();
                dataTarget.Type = "WarehouseReceive";
                dataTargetColl.Add(dataTarget);
                dc.DataTargetCollection = dataTargetColl.ToArray();
                ShipmentOrder pOrder = new ShipmentOrder();
                pOrder.OrderNumber = refPO;
                pOrder.ClientReference = refPO;
                shipment.DataContext = dc;
                //TODO 
                //Create Variable to allow for Return Authority
                CodeDescriptionPair codePair = new CodeDescriptionPair();
                codePair.Code = "REC";
                codePair.Description = "GOODS RECEIPT";
                pOrder.Type = codePair;
                ShipmentOrderWarehouse warehouse = new ShipmentOrderWarehouse();
                warehouse.Code = "SYD";
                warehouse.Name = "PCFS SYDNEY";
                pOrder.Warehouse = warehouse;
                List<Date> dateColl = new List<Date>();
                Date orderDate = new Date();
                orderDate.Type = DateType.BookingConfirmed;
                orderDate.Value = DateTime.Now.ToString("s");
                orderDate.IsEstimate = false;
                orderDate.IsEstimateSpecified = true;
                dateColl.Add(orderDate);
                pOrder.DateCollection = dateColl.ToArray();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                int i = 0;
                orderLine.Content = CollectionContent.Complete;
                List<ShipmentOrderOrderLineCollectionOrderLine> olColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                orderLine.ContentSpecified = true;
                foreach (DataRow dr in tb.Rows)
                {
                    i++;
                    ShipmentOrderOrderLineCollectionOrderLine oLine = new ShipmentOrderOrderLineCollectionOrderLine();
                    oLine.ExpectedQuantity = decimal.Parse(dr["QTY"].ToString());
                    oLine.ExpectedQuantitySpecified = true;

                    oLine.LineNumber = i;
                    oLine.LineNumberSpecified = true;
                    CodeDescriptionPair cdUnit = new CodeDescriptionPair();
                    cdUnit.Code = "UNT";
                    oLine.OrderedQtyUnit = cdUnit;
                    Product product = new Product();
                    product.Code = dr["CODE"].ToString(); ;
                    product.Description = dr["DESC"].ToString();
                    oLine.Product = product;
                    olColl.Add(oLine);
                }
                orderLine.OrderLine = olColl.ToArray();
                pOrder.OrderLineCollection = orderLine;
                List<OrganizationAddress> OrgAddColl = new List<OrganizationAddress>();
                OrganizationAddress oa = new OrganizationAddress();
                oa.AddressType = "ConsignorDocumentaryAddress";
                oa.OrganizationCode = senderCode;
                OrgAddColl.Add(oa);
                shipment.OrganizationAddressCollection = OrgAddColl.ToArray();
                shipment.Order = pOrder;
                uShipData.Shipment = shipment;
                uBody.BodyField = uShipData;
                uShipData.version = "1.1";
                uShipData.Shipment = shipment;
                String cwXML = Path.Combine(OutputPath, senderCode + "PO" + pOrder.OrderNumber.Trim() + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(OutputPath, senderCode + "PO" + pOrder.OrderNumber.Trim() + iFileCount + ".xml");
                }
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                StringWriter writer = new StringWriter();
                uInt.Body = uBody;
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                xSer.Serialize(outputCW, uInt, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
            }
        }

        #endregion
        public bool ConvertToCommonReceipt(string alertEmail, Guid selectedMap)
        {
            bool errorFound = false;
            string orderNoField = ReturnStringFromList(Map, selectedMap, "ReceiveReference");
            if (string.IsNullOrEmpty(orderNoField))
            {
                _errSummary += "Critical: Unable to process Receipt. Must have an Order Number Field";
                return false;
            }
            var custRefField = ReturnStringFromList(_map, selectedMap, "CustomerReferences");
            if (string.IsNullOrEmpty(custRefField))
            {
                _errSummary += "Warning: No Customer Reference field. Not compulsory" + Environment.NewLine;

            }
            var prodCodeField = ReturnStringFromList(_map, selectedMap, "ProductCode");
            if (string.IsNullOrEmpty(prodCodeField))
            {
                _errSummary += "Critical:Unable to process Receipt. Must have an Product Code Field";
                return false;
            }
            var whouseField = ReturnStringFromList(_map, selectedMap, "WarehouseCode");
            if (string.IsNullOrEmpty(whouseField))
            {
                _errSummary += "Critical:Unable to process Receipt. Must have a Warehouse Code Field";
                return false;
            }
            var qtyField = ReturnStringFromList(_map, selectedMap, "ExpectedQty");
            if (string.IsNullOrEmpty(qtyField))
            {
                _errSummary += "Critical:Unable to process Receipt. Must have an Expected Qty Field";
                return false;
            }
            var etaField = ReturnStringFromList(_map, selectedMap, "ETA");
            if (string.IsNullOrEmpty(etaField))
            {
                _errSummary += "Warning: There was not an ETA Field found. ETA will be defaulted to Today's date." + Environment.NewLine;

            }
            var uomField = ReturnStringFromList(_map, selectedMap, "ReceiveUOM");
            if (string.IsNullOrEmpty(uomField))
            {
                _errSummary += "Warning: There was not an UOM Field found. UOM will be defaulted to UNT." + Environment.NewLine;

            }
            var supplierField = ReturnStringFromList(_map, selectedMap, "Supplier");
            var containerNoField = ReturnStringFromList(_map, selectedMap, "ContainerNumber");
            if (string.IsNullOrEmpty(containerNoField))
            {
                _errSummary += "Information: No Container Number Field. Not compulsory" + Environment.NewLine;

            }
            var containerTypeField = ReturnStringFromList(_map, selectedMap, "ContainerType");
            if (string.IsNullOrEmpty(containerTypeField))
            {
                _errSummary += "Information: No Container Type Field. Not compulsory" + Environment.NewLine;

            }
            var palletQtyField = ReturnStringFromList(Map, selectedMap, "TotalPallets") + Environment.NewLine;
            if (string.IsNullOrEmpty(palletQtyField))
            {
                _errSummary += "Information: No Pallet Total Field found. Not compulsory" + Environment.NewLine;

            }
            var orderList = CustData.AsEnumerable()

                .Select(row => new
                {
                    OrderNo = row.Field<string>(orderNoField)
                }).Distinct().ToList();

            foreach (var order in orderList)
            {
                NodeFile convertedReceipt = new NodeFile();

                convertedReceipt.IdendtityMatrix = new NodeFileIdendtityMatrix
                {
                    CustomerId = _receipientID,
                    SenderId = _senderID,
                    DocumentType = "Receipt"
                };
                if (!string.IsNullOrEmpty(order.OrderNo))
                {
                    DataTable tempTable = (from l in CustData.AsEnumerable()
                                           where l.Field<string>(orderNoField) == order.OrderNo
                                           select l).CopyToDataTable();
                    var receipt = new NodeFileWarehouseReceipt();
                    receipt.Companies = GetCompaniesFromRow(tempTable.Rows[0], supplierField).ToArray();
                    receipt.Dates = GetDatesFromRow(tempTable.Rows[0], etaField, true).ToArray();
                    
                    receipt.CustomerReferences = ReturnValueFromTable(tempTable.Rows[0], custRefField).ToString();
                    var rxref = ReturnValueFromTable(tempTable.Rows[0], orderNoField).ToString();
                    if ( rxref==err)
                    {
                        return false;
                    }
                    receipt.ReceiveReference = ReturnValueFromTable(tempTable.Rows[0], orderNoField).ToString();
                    var whcode = ReturnValueFromTable(tempTable.Rows[0], whouseField);
                    if (whcode == err)
                    {
                        return false;
                    }
                    receipt.WarehouseCode = whcode;
                    List<OrderLineElement> receiptLines = new List<OrderLineElement>();
                    int lineNo = 0;
                    List<ContainerElement> containers = new List<ContainerElement>();
                    foreach (DataRow row in tempTable.Rows)
                    {
                        OrderLineElement line = new OrderLineElement();
                        lineNo++;
                        line.LineNo = lineNo;
                        line.LineNoSpecified = true;
                        int qty = 0;

                        if (int.TryParse(row[qtyField].ToString(), out qty))
                        {
                            line.ExpectedQuantity = qty;
                        }

                        line.PackageUnit = row[uomField].ToString();
                        line.Product = new ProductElement
                        {
                            Code = row[prodCodeField].ToString()
                        };
                        receiptLines.Add(line);
                        if (!string.IsNullOrEmpty(row[containerNoField].ToString()))
                        {
                            ContainerElement container = new ContainerElement
                            {
                                ContainerNo = row[containerNoField].ToString(),
                                ContainerCode = row[containerTypeField].ToString()
                            };
                            containers.Add(container);
                        }
                    }
                    receipt.Containers = containers.Count > 0 ? containers.ToArray() : null;
                    receipt.OrderLines = receiptLines.ToArray();
                    convertedReceipt.WarehouseReceipt = receipt;
                    ToCargowise commonFunc = new ToCargowise("andy.duggan@ctcorp.com.au");
                    var cwFile = commonFunc.CWFromCommon(convertedReceipt);
                    commonFunc.ToFile(cwFile, convertedReceipt.IdendtityMatrix.CustomerId + "-" +
                        convertedReceipt.IdendtityMatrix.DocumentType + "-" + order.OrderNo.Replace('/', '-'), OutputPath);
                }
            }
            return errorFound;

        }

        #region Helpers
        private string ReturnValueFromTable(DataRow dataRow, string custRefField)
        {
            try
            {
                return !string.IsNullOrEmpty(custRefField) ? dataRow[custRefField].ToString().Trim() : null;
            }
            catch (ArgumentException ex)
            {
                ErrorMsg += ex.Message + Environment.NewLine;
                return err;
            }

        }
        private List<DateElement> GetDatesFromRow(DataRow dataRow, string fieldName, bool eta)
        {

            List<DateElement> Dates = new List<DateElement>();
            DateElement receiptDate = new DateElement();
            DateTime date = new DateTime();

            if (DateTime.TryParse(dataRow[fieldName].ToString(), out date))
            {
                receiptDate.EstimateDate = date.ToString("yyyy-MM-ddTHH\\:mm\\:ss");

            }
            else
            {
                double d;
                if (double.TryParse(dataRow[fieldName].ToString(), out d))
                {
                    receiptDate.EstimateDate = DateTime.FromOADate(d).ToString("yyyy-MM-ddTHH\\:mm\\:ss");
                    receiptDate.DateType = DateElementDateType.Arrival;
                    Dates.Add(receiptDate);
                }
                else
                {
                    if (eta)
                    {
                        receiptDate.EstimateDate = DateTime.Now.ToString("yyyy-MM-ddTHH\\:mm\\:ss");
                        Dates.Add(receiptDate);
                    }
                }
            }
            return Dates;


        }
        private static string ReturnStringFromList(List<MapOperation> map, Guid selectedMap, string stringToFind)
        {
            var value = (from x in map
                         where x.MD_MH == selectedMap
                         && x.MD_FromField.Trim() == stringToFind
                         select x.MD_ToField.Trim()).FirstOrDefault();
            return value;
        }

        private List<CompanyElement> GetCompaniesFromRow(DataRow dataRow, string fieldName)
        {
            List<CompanyElement> companies = new List<CompanyElement>{
                new CompanyElement{
                     CompanyOrgCode =SenderId,
                 CompanyType = CompanyElementCompanyType.Consignor} };

            if (!string.IsNullOrEmpty(fieldName))
            {
                CompanyElement supplier = new CompanyElement();
                supplier.CompanyCode = dataRow[fieldName].ToString();
                companies.Add(supplier);
            }
            return companies;
        }

        #endregion
    }
}
