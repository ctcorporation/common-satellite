﻿using System;
using System.Collections.Generic;
using System.Linq;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common_Satellite.Classes
{
    public class References
    {
        #region members
        private string _connString;
        private string _errorLog;
        #endregion

        #region parameters
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        public string ErrorLog
        {
            get { return _errorLog; }
        }
        #endregion

        #region constructors
        public References(string connString)
        {
            ConnString = connString;
        }
        #endregion

        public int SaveReference(ReferenceNumber refs)
        {
            ReferenceNumber job = new ReferenceNumber();
            try
            {
                using (NodeDataContext _context = new NodeDataContext(Globals.ConnString.ConnString))
                {
                    

                        if (CheckReferenceNumber(refs))
                        {
                            job.REF_ID = Guid.NewGuid();
                            job.REF_HOUSEBILL = refs.REF_HOUSEBILL;
                            job.REF_MASTERBILL = refs.REF_MASTERBILL;
                            job.REF_JOBNUMBER = refs.REF_JOBNUMBER;
                            _context.ReferenceNumbers.Add(job);
                            _context.SaveChanges();

                        }
                    
                }
            }
            catch (Exception ex)
            {
                var x = ex.InnerException;
            }
            return -1;

        }

        public bool CheckReferenceNumber(ReferenceNumber refNum)
        {
            bool result = true;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                ReferenceNumber data = uow.ReferenceNumbers.Find(x => x.REF_MASTERBILL == refNum.REF_MASTERBILL).FirstOrDefault();

                if(data != null)
                {
                    result = false;
                }
            }

            return result;
        }

        public string GetJobNumber(string billNumber)
        {
            string job = null;
            ReferenceNumber data = new ReferenceNumber();
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(ConnString)))
            {
                data = uow.ReferenceNumbers.Find(x => x.REF_HOUSEBILL == billNumber || x.REF_MASTERBILL == billNumber).FirstOrDefault();

                if(data != null)
                {
                    job = data.REF_JOBNUMBER;
                }
            }

            return job;

        }
    }
}
