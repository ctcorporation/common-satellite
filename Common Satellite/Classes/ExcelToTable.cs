﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Data.OleDb;

namespace Common_Satellite.Classes
{
    public class ExcelToTable
    {

        #region members
        string _errString;
        string _fileName;
        #endregion

        #region Properties
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        #endregion 

        #region Constructor
        public ExcelToTable(string fileName)
        {
            FileName = fileName;
        }
        #endregion
        public DataTable ToTable(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {

                SpreadsheetDocument xs = SpreadsheetDocument.Open(fs, false);
                WorkbookPart workbookPart = xs.WorkbookPart;
                IEnumerable<Sheet> sheets = xs.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)xs.WorkbookPart.GetPartById(relationshipId);
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                DataTable dt = new DataTable();
                foreach (Cell cell in rows.ElementAt(0))
                {
                    DataColumnCollection colColl = dt.Columns;
                    string colName = ExcelHelper.GetCellValue(xs, cell).Trim();
                    int i = 1;
                    while (colColl.Contains(colName))
                    {
                        colName = ExcelHelper.GetCellValue(xs, cell).Trim() + "-" + i;
                        i++;
                    }
                    dt.Columns.Add(colName);
                }
                int colcount = dt.Columns.Count;
                foreach (Row row in rows)
                {
                    try
                    {

                        DataRow tempRow = dt.NewRow();
                        int columnIndex = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            int cellColumnIndex = (int)ExcelHelper.GetColumnIndexFromName(ExcelHelper.GetColumnName(cell.CellReference));
                            cellColumnIndex--;
                            if (columnIndex < cellColumnIndex)
                            {
                                do
                                {
                                    tempRow[columnIndex] = "";
                                    columnIndex++;
                                }
                                while (columnIndex < cellColumnIndex);

                            }
                            tempRow[columnIndex] = ExcelHelper.GetCellValue(xs, cell).Trim();
                            columnIndex++;
                            if (dt.Rows.Count == 82)
                            {
                                var cCount = tempRow.Table.Columns.Count;
                            }
                        }

                        dt.Rows.Add(tempRow);
                    }
                    catch (Exception ex)
                    {
                        _errString = ex.Message;
                    }
                }
                fs.Close();

                dt.Rows.RemoveAt(0);
                return dt;


            }
        }


        public DataTable ToTable(string fileName, int startIndex)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {

                SpreadsheetDocument xs = SpreadsheetDocument.Open(fs, false);
                WorkbookPart workbookPart = xs.WorkbookPart;
                IEnumerable<Sheet> sheets = xs.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)xs.WorkbookPart.GetPartById(relationshipId);
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                DataTable dt = new DataTable();
                foreach (Cell cell in rows.ElementAt(startIndex))
                {
                    DataColumnCollection colColl = dt.Columns;
                    string colName = ExcelHelper.GetCellValue(xs, cell).Trim();
                    int i = 1;
                    while (colColl.Contains(colName))
                    {
                        colName = ExcelHelper.GetCellValue(xs, cell).Trim() + "-" + i;
                        i++;
                    }
                    dt.Columns.Add(colName);
                }
                int colcount = dt.Columns.Count;
                foreach (Row row in rows)
                {
                    //if (row.RowIndex < 10)
                    //{
                    //    continue;
                    //}
                    try
                    {

                        DataRow tempRow = dt.NewRow();
                        int columnIndex = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            int cellColumnIndex = (int)ExcelHelper.GetColumnIndexFromName(ExcelHelper.GetColumnName(cell.CellReference));
                            cellColumnIndex--;
                            if (columnIndex < cellColumnIndex)
                            {
                                do
                                {
                                    tempRow[columnIndex] = "";
                                    columnIndex++;
                                }
                                while (columnIndex < cellColumnIndex);

                            }
                            tempRow[columnIndex] = ExcelHelper.GetCellValue(xs, cell).Trim();
                            columnIndex++;
                            if (dt.Rows.Count == 82)
                            {
                                var cCount = tempRow.Table.Columns.Count;
                            }
                        }

                        dt.Rows.Add(tempRow);
                    }
                    catch (Exception ex)
                    {
                        _errString = ex.Message;
                    }
                }
                fs.Close();
                //dt.Rows.RemoveAt(0);


                return dt;


            }


        }

        public DataTable ToTable(string fileName, string header1)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {

                SpreadsheetDocument xs = SpreadsheetDocument.Open(fs, false);
                WorkbookPart workbookPart = xs.WorkbookPart;
                IEnumerable<Sheet> sheets = xs.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)xs.WorkbookPart.GetPartById(relationshipId);
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                DataTable dt = new DataTable();
                bool startHeader = false;
                Row headers = rows.ElementAt(0);
                foreach (Row row in rows)
                {
                    foreach(Cell cell in row)
                    {
                        string colName = ExcelHelper.GetCellValue(xs, cell).Trim();
                        if (colName == header1)
                        {
                            startHeader = true;
                        }

                        if (startHeader == false)
                        {
                            break;
                        }

                        startHeader = true;
                        DataColumnCollection colColl = dt.Columns;
                        int i = 1;
                        while (colColl.Contains(colName))
                        {
                            colName = ExcelHelper.GetCellValue(xs, cell).Trim() + "-" + i;
                            i++;
                        }
                        dt.Columns.Add(colName);
                    }

                    if(startHeader == true)
                    {
                        break;
                    }
                    
                }
                int colcount = dt.Columns.Count;
                foreach (Row row in rows)
                {
                    //if (row.RowIndex < 10)
                    //{
                    //    continue;
                    //}
                    try
                    {

                        DataRow tempRow = dt.NewRow();
                        int columnIndex = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            int cellColumnIndex = (int)ExcelHelper.GetColumnIndexFromName(ExcelHelper.GetColumnName(cell.CellReference));
                            cellColumnIndex--;
                            if (columnIndex < cellColumnIndex)
                            {
                                do
                                {
                                    tempRow[columnIndex] = "";
                                    columnIndex++;
                                }
                                while (columnIndex < cellColumnIndex);

                            }
                            tempRow[columnIndex] = ExcelHelper.GetCellValue(xs, cell).Trim();
                            columnIndex++;
                            if (dt.Rows.Count == 82)
                            {
                                var cCount = tempRow.Table.Columns.Count;
                            }
                        }

                        dt.Rows.Add(tempRow);
                    }
                    catch (Exception ex)
                    {
                        _errString = ex.Message;
                    }
                }
                fs.Close();
                //dt.Rows.RemoveAt(0);


                return dt;


            }


        }

        public DataTable XlsToTable(string filename, int startIndex)
        {
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + "; Extended Properties='Excel 8.0;HDR=NO;IMEX=1;'";
            DataTable dttemp = new DataTable();
            DataSet ds = new DataSet();
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                dttemp = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dttemp != null)
                {
                    var tempDataTable = (from dataRow in dttemp.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    //dt = tempDataTable;
                    sheetName = dttemp.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15, F16, F17 FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                //dt = ds.Tables["excelData"];
                conn.Close();

            }

            DataTable dt = new DataTable();

            for (int i = 0; i < 17; i++)
            {
                dt.Columns.Add(ds.Tables[0].Rows[startIndex][i].ToString());
            }

            for (int i = startIndex + 1; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow tempRow = dt.NewRow();

                if (ds.Tables[0].Rows[i].ItemArray[1].ToString() != "")
                {
                    for (int c = 0; c < 17; c++)
                    {
                        tempRow[c] = ds.Tables[0].Rows[i].ItemArray[c].ToString();
                    }
                }

                dt.Rows.Add(tempRow);
            }


            return dt;
        }


        public DataTable XlsToTable(string filename, string header1)
        {
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + "; Extended Properties='Excel 8.0;HDR=NO;IMEX=1;'";
            DataTable dttemp = new DataTable();
            DataSet ds = new DataSet();
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                dttemp = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dttemp != null)
                {
                    var tempDataTable = (from dataRow in dttemp.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    //dt = tempDataTable;
                    sheetName = dttemp.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13, F14, F15, F16, F17 FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                //dt = ds.Tables["excelData"];
                conn.Close();

            }

            DataTable dt = new DataTable();

            bool startHeader = false;
            int r = 0;
            while (startHeader == false)
            {
                for (int i = 0; i < 17; i++)
                {
                    if(ds.Tables[0].Rows[r][i].ToString() == header1)
                    {
                        startHeader = true;
                    }

                    if (startHeader)
                    {
                        dt.Columns.Add(ds.Tables[0].Rows[r][i].ToString());
                    }
                    else
                    {
                        r += 1;
                        break;
                    }
                }
            }

            

            for (int i = r + 1; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow tempRow = dt.NewRow();

                if (ds.Tables[0].Rows[i].ItemArray[1].ToString() != "")
                {
                    for (int c = 0; c < 17; c++)
                    {
                        tempRow[c] = ds.Tables[0].Rows[i].ItemArray[c].ToString();
                    }
                }

                dt.Rows.Add(tempRow);
            }


            return dt;
        }

        public DataTable XlsToTable(string filename)
        {
            string connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + "; Extended Properties='Excel 8.0;HDR=NO;IMEX=1;'";
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();

                dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    var tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    //dt = tempDataTable;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dt = ds.Tables["excelData"];
                conn.Close();

            }

            return dt;
        }
    }
}
