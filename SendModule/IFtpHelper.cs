﻿using System.Collections.Generic;
using WinSCP;

namespace SendModule
{
    public interface IFtpHelper
    {
        string ErrString { get; set; }
        string FileName { get; set; }
        string ReceivePath { get; set; }
        string WinSCPLocation { get; set; }
        SessionOptions FTPsessionOptions
        {
            get; set;
        }
        string sshfingerPrint
        {
            get; set;
        }
        string PrivateKeyPath
        {
            get; set;
        }

        SessionOptions BuildSessionOptions(string host, int port, string username, string password, bool ssl);
        List<string> FTPReceive(string fileMask, string receivePath);
        string FtpSend(string sendPath);
        string FtpSend(string file, string server, string folder, string user, string password);
    }
}